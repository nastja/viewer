add_executable(ViewerUnitTests
        test.cpp
        eventtest.cpp
        model/file/colorfileparsertest.cpp)

target_link_libraries(ViewerUnitTests viewer)
include_directories(${CMAKE_SOURCE_DIR}/src)

add_test(NAME ViewerUnitTests COMMAND ViewerUnitTests)
