/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "catch.hpp"
#include "event/modelelement.h"
#include "event/modellistener.h"

class TestListener : public ModelListener {
public:
  TestListener(std::string s) : ModelListener(s) {}
  bool hasChanged = false;

  void onElementChange() {
    hasChanged = true;
  }
};

class TestModelElement : public ModelElement {
public:
  TestModelElement(std::string s) : ModelElement(s) {}
  void call_notify() {
    notify();
  }
};

TEST_CASE("Event tests", "[event]") {
  TestListener listenerA("listenerA"), listenerB("listenerB");
  TestModelElement element("testelement");

  element.call_notify();

  REQUIRE(!listenerA.hasChanged);
  REQUIRE(!listenerB.hasChanged);

  element.addListener(&listenerA);
  element.addListener(&listenerB);
  element.call_notify();

  REQUIRE(listenerA.hasChanged);
  REQUIRE(listenerB.hasChanged);

  listenerA.hasChanged = false;
  listenerB.hasChanged = false;

  element.removeListener(&listenerB);
  element.call_notify();

  REQUIRE(listenerA.hasChanged);
  REQUIRE(!listenerB.hasChanged);
}
