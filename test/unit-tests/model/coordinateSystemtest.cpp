/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "catch.hpp"
#include "model/coordinatesystem.h"

TEST_CASE("Coordinate System Test", "[model]") {
  CoordinateSystem coord;

  ivec3 testVec(1, 2, 3);

  ivec3 result = coord.transformVector(testVec);
  REQUIRE(result == ivec3(1, 2, 3));

  coord.rotate(Rotation::UP);

  result = coord.transformVector(testVec);
  REQUIRE(result[0] == 1);
  REQUIRE(result[1] == 3);
  REQUIRE(result[2] == -2);

  result = coord.transformVectorInverse(result);
  REQUIRE(result[0] == 1);
  REQUIRE(result[1] == 2);
  REQUIRE(result[2] == 3);

  coord.rotate(Rotation::RIGHT);
  result = coord.transformVector(testVec);
  REQUIRE(result[0] == 2);
  REQUIRE(result[1] == 3);
  REQUIRE(result[2] == 1);

  result = coord.transformVectorInverse(result);
  REQUIRE(result[0] == 1);
  REQUIRE(result[1] == 2);
  REQUIRE(result[2] == 3);
}
