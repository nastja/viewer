/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#define CATCH_CONFIG_RUNNER
#include "catch.hpp"
#include "presenter/presentationmanager.h"
#include <FL/Fl.H>
#include <cstring>
#include <thread>
#include "commandlineparser.h"
#include "testframe.h"

int main(int argc, char** argv) {
  const char** arg   = new const char*[2];
  std::string config = TEST_FILES;
  char* c            = strdup(config.c_str());
  arg[0]             = argv[0];
  arg[1]             = c;
  CommandLineParser parser(2, arg);
  ModelManager model{parser};
  model.setupModelElements();

  PresentationManager presentation{model};

  TestFrame gui(presentation);

  Fl::run();

  return Catch::Session().run(argc, argv);
}
