/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "testframe.h"
#include <FL/Enumerations.H>
#include <FL/Fl.H>

TestFrame::TestFrame(PresentationManager& presentationManager)
    : Fl_Window(Fl::w(), Fl::h(), "NAStJA viewer") {
  Fl::scheme("gtk+");

  end();

  auto screenwitdh  = Fl::w();
  auto screenheight = Fl::h();

  begin();
  visualizationView = new TestVisualizationView(40, 50, screenwitdh - 270, screenheight - 100, presentationManager);
  end();
  resize(0, 0, screenwitdh, screenheight);

  size_range(500, 650, 0, 0);
  resizable(visualizationView);
  uchar r;
  uchar g;
  uchar b;
  Fl::get_color(FL_BACKGROUND_COLOR, r, g, b);
  Fl::background(r, g, b);

  show();
}

int TestFrame::handle(int /*event*/) {
  if (visualizationView->isDrawn())
    hide();
  return 1;
}

void TestFrame::resize(int x, int y, int w, int h) {
  Fl_Window::resize(x, y, w, h);
}
