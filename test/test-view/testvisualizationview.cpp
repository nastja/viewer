/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "testvisualizationview.h"
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <dirent.h>
#include <iomanip>
#include <iostream>
#include <lodepng.h>
#include <regex>
#include <sstream>

TestVisualizationView::TestVisualizationView(unsigned int x, unsigned int y, unsigned int w, unsigned int h, PresentationManager& presentationManager)
    : Fl_Box(x, y, w, h), dataPresentation(presentationManager.getDataPresentation()) {
  dataPresentation.setResolution(w, h);
  dataPresentation.setView(this);
  take_focus();
}

TestVisualizationView::~TestVisualizationView() {
}

void TestVisualizationView::draw() {
  auto img = dataPresentation.draw();
  fl_copy_offscreen(x(), y(), w(), h(), img, 0, 0);
  auto* img_data = new unsigned char[w() * h() * 3];
  fl_read_image(img_data, x(), y(), w(), h());
  saveScreenshot(img_data);
  fl_delete_offscreen(img);
  drawn = true;
}

void TestVisualizationView::update() {
  redraw();
}

int TestVisualizationView::handle(int event) {
  switch (event) {
    case FL_KEYBOARD:
      handle_key(event, Fl::event_key());
      return 1;
    case FL_PUSH:
      return 1;
    case FL_RELEASE:
      return 1;
    case FL_DRAG:
      return 1;
    case FL_MOVE:
      return 1;
    case FL_FOCUS:
      return 1;
    case FL_UNFOCUS:
      return 1;
    default:
      return 0;
  }
}

int TestVisualizationView::handle_key(int /*event*/, int /*key*/) {
  return 1;
}

void TestVisualizationView::resize(int x, int y, int w, int h) {
  Fl_Box::resize(x, y, w, h);

  dataPresentation.setResolution(w, h);
}

void TestVisualizationView::saveScreenshot(unsigned char* img_data) {
  const std::string start = "Screenshot";

  std::stringstream ss;
  ss << start << ".png";

  auto fileName = ss.str();
  int error     = lodepng::encode(fileName, img_data, w(), h(), LCT_RGB);
  if (error)
    std::cerr << "Image writing failed!" << std::endl;
  else {
#ifndef NDEBUG
    std::cout << "[Image] File " + fileName + " written." << std::endl;
#endif
  }
}
