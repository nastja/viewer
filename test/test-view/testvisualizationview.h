/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef NASTJA_VIEWER_TESTVISUALIZATIONVIEW_H
#define NASTJA_VIEWER_TESTVISUALIZATIONVIEW_H

#include "presenter/presentationmanager.h"
#include "presenter/datapresentation/datapresentation.h"
#include "view/view.h"
#include <FL/Fl_Box.H>
#include <memory>

class TestVisualizationView : public Fl_Box, View {
public:
  TestVisualizationView(unsigned int x, unsigned int y, unsigned int w, unsigned int h, PresentationManager& presentationManager);
  ~TestVisualizationView() override;
  void draw() override;
  void update() override;
  void resize(int x, int y, int w, int h) override;
  int handle(int e) override;

  bool isDrawn() {
    return drawn;
  }

private:
  DataPresentation& dataPresentation;
  int handle_key(int event, int key);
  void saveScreenshot(unsigned char* img_data);
  bool drawn = false;
};

#endif  //NASTJA_VIEWER_TESTVISUALIZATIONVIEW_H
