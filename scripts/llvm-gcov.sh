#!/bin/bash
if command -v llvm-cov-3.8
then
	exec llvm-cov-3.8 gcov "$@"
else
	exec llvm-cov gcov "$@"
fi
 
