#!/bin/bash
lcov --directory src/CMakeFiles/ --base-directory . --gcov-tool ../scripts/llvm-gcov.sh  --capture -o coverage.info
lcov -r coverage.info /usr/\* *evaluator.stdout* *configfile.stdout* \*/include/\* -o coverage_local.info

