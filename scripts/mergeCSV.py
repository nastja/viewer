import glob

filenames = glob.glob('**/*.csv', recursive=True)

unique_names = {name[name.rfind('/') + 1:] for name in filenames }

for name in unique_names:
    with open(name, 'a') as singleFile:
        for file in filenames:
            if file.find(name) != -1:
                for line in open(file, 'r'):
                    singleFile.write(line)
                    