#!/usr/bin/env perl

use warnings;
use strict;

# pack binary data from stdin to a uint_8 c-array at stdout.
# array is named with first command line argzment from script

if(!defined$ARGV[0]) {
	die "add array name as first argument";
}

binmode(STDIN);

print "#include <stdint.h>\n";
printf("uint8_t %s[] = {\n", $ARGV[0]);

my $val;

if(!defined($val = getc())) { die "file has length 0"; }

printf("    0x%X", ord($val));

while(defined($val = getc())) {
    printf(", 0x%X", ord($val));
}

print "\n};\n"
