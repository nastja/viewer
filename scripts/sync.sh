#!/bin/bash

if [[ -z "$1" ]]; then
  echo "Missing folder argument."
  exit 1
fi

if [[ -z "$2" ]]; then
  echo "Missing target."
  exit 1
fi

target="kasper:nastja_remote"
echo "Syncing $1 to $target"
rsync -avz --delete --exclude '.git' --exclude 'build*' $1/ $2
