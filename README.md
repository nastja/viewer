The NAStJA Viewer
=================
A small and fast viewer for 3D image data.

## Features
- Multi-platform support
- VTI vtk image data (float32, float64, uint32, uint64)
- CSV file for mapping origin data to several columns
- Simple .dat files
- Display of data in a block-structured grid (000/000/000/name-00000.vti)
- Time series
- Different color bars
- Discrete colors
- 3D and 1D data view
- Most options can be operated via keyboard

![Screenshot](documentation/screenshot.png "Screenshot of the NAStJA Viewer")

## Build
Requirements: FLTK 1.3.5

Tested with GCC and clang on 64bit linux and MacOS.

```sh
mkdir build
cd build
cmake ..
make
```
Start with `src/nastjaviewer folder`.

## History
The first version was created in the "Praxis der Softwareentwicklung" (PSE WS 2017/18) from students of the KIT.
