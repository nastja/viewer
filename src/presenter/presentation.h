/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_PRESENTER_PRESENTATION_H_
#define SRC_PRESENTER_PRESENTATION_H_

#include "model/modelmanager.h"
#include "view/view.h"
#include <string>
#include <utility>

/**
 * Abstact base class for all Presentations. Will contain a reference to ModelManager and a reference to a matching View.
 *
 * @ingroup presenter
 */
class Presentation : public ModelListener {
public:
  /**
   * Creates new Presentation using ModelManager
   *
   * @param model the ModelManager to use
   */

  Presentation(const std::string& name, ModelManager& model)
      : ModelListener(name + " Presentation"), model(model) {}
  ~Presentation() = default;

  /**
   * sets the view for the presentation
   * @param view new view
   */
  void setView(View* view) { this->view = view; }

protected:
  ModelManager& model;

  /**
   * updates the view
   */
  void updateView() { view->update(); }

private:
  View* view;
};

#endif  // SRC_PRESENTER_PRESENTATION_H_
