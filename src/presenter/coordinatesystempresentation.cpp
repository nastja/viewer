/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "coordinatesystempresentation.h"
#include "model/coordinatesystem.h"
#include <FL/Enumerations.H>
#include <FL/fl_draw.H>

CoordinateSystemPresentation::CoordinateSystemPresentation(ModelManager& modelManager)
    : ImagePresentation("CoordinateSystem", modelManager) {
  modelManager.getCoordinateSystem().addListener(this);
  resolution = ivec2(100, 100);
}

/**
 * Draws an arrow.
 *
 * @param start  The start vector.
 * @param end    The end vector.
 */
void draw_arrow(ivec2 start, ivec2 end) {
  vec2 direction = end - start;
  direction.normalize();
  vec2 half(0.5, 0.5);  // for rounding

  ivec2 right = end + (8.0 * direction.rotate(160) + half);
  ivec2 left  = end + (8.0 * direction.rotate(-160 * 2) + half);

  fl_line(start.x(), start.y(), end.x(), end.y());
  fl_line(end.x(), end.y(), left.x(), left.y());
  fl_line(end.x(), end.y(), right.x(), right.y());
}

/**
 * Gets the axis name.
 *
 * @param buffer  The buffer.
 * @param n       The buffer size.
 * @param vec     The vector of transformed coordinates.
 * @param size    The size vector.
 */
void get_axis_name(char* buffer, int n, ivec3 vec, ivec3 size) {
  int dim = 0;
  if (vec.x() != 0) {
    dim = 0;
  } else if (vec.y() != 0) {
    dim = 1;
  } else {
    dim = 2;
  }

  if (vec[dim] < 0) {
    std::snprintf(buffer, n, "-%c [%d]", 'x' + dim, size[dim]);
  } else {
    std::snprintf(buffer, n, "%c [%d]", 'x' + dim, size[dim]);
  }
}

Fl_Offscreen CoordinateSystemPresentation::draw() {
  auto& coords = model.getCoordinateSystem();
  auto size    = model.getDataManager().getDataView().getDataSize();
  auto img     = fl_create_offscreen(resolution.x(), resolution.y());
  fl_begin_offscreen(img);
  fl_rectf(0, 0, resolution.x(), resolution.y(), FL_BACKGROUND_COLOR);
  int textoffset = 16;
  double length  = (std::min(resolution.x(), resolution.y()) - textoffset) * (1.0 + 0.5 / std::sqrt(2)) / 2.0;
  double lengthz = length / (2 * std::sqrt(2));
  double shift   = 0.5 * (length - lengthz);
  ivec2 center(resolution.x() / 2 - shift + textoffset / 2, resolution.y() / 2 + shift - textoffset / 2);
  ivec2 e_x(length, 0);
  ivec2 e_y(0, -length);
  ivec2 e_z(-lengthz, lengthz);

  e_x += center;
  e_y += center;
  e_z += center;

  fl_line_style(0, 2);
  fl_color(FL_BLACK);
  draw_arrow(center, e_x);
  draw_arrow(center, e_y);
  draw_arrow(center, e_z);
  fl_line_style(0);

  int w = 0;
  int h = 0;
  char s[1024];
  fl_font(FL_HELVETICA, 14);

  get_axis_name(s, 1024, coords.transformVectorInverse(ivec3(1, 0, 0)), size);
  fl_measure(s, w, h);
  fl_draw(s, e_x.x() - w - 2, e_x.y() + 5, w, h, FL_ALIGN_TOP_RIGHT);

  get_axis_name(s, 1024, coords.transformVectorInverse(ivec3(0, 1, 0)), size);
  fl_draw(s, e_y.x() + 7, e_y.y(), 20, 20, FL_ALIGN_TOP_LEFT);

  get_axis_name(s, 1024, coords.transformVectorInverse(ivec3(0, 0, 1)), size);
  fl_draw(s, e_z.x() - 15, e_z.y() + 2, 20, 20, FL_ALIGN_TOP_LEFT);

  fl_end_offscreen();
  return img;
}

void CoordinateSystemPresentation::setResolution(unsigned int x, unsigned int y) {
  resolution.x() = x;
  resolution.y() = y;
}

void CoordinateSystemPresentation::onElementChange() {
  updateView();
}
