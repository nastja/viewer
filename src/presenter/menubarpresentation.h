/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_PRESENTER_MENUBARPRESENTATION_H_
#define SRC_PRESENTER_MENUBARPRESENTATION_H_

#include "model/modelmanager.h"
#include "presenter/presentation.h"
#include <utility>

/**
 * The Presenter Component for MenuBarView that performs Actions done by the menu
 *
 * @ingroup presenter
 */
class MenuBarPresentation : public Presentation {
public:
  explicit MenuBarPresentation(ModelManager& model)
      : Presentation("MenuBar", model) {
    model.getControlPoint().addListener(this);
    model.getPresentationMode().addListener(this);
    model.getColorscaleManager().addListener(this);
  }

  /**
   * Resets the view. This will cause AnchorPointStack to go back to its original state and the event system will handle the rest.
   */
  void resetView() {
    model.reset();
  }

  /**
   * Reloads the files. This includes the action of BlockFilenameParser so that new files can also be recognized.
   */
  void reloadData() {
    model.reload();
  }

  /**
   * toggleLabel() in PresentationMode. this will cause the labels in the 3D view to disappear or appear
   */
  void toggleNumbers() {
    model.getPresentationMode().toggleLabel();
  }

  /**
   * jumps to next time step
   */
  void nextTime() {
    model.getDataManager().moveFrame(1);
  }

  /**
   * jumps to previous time step
   */
  void previousTime() {
    model.getDataManager().moveFrame(-1);
  }

  /**
   * activates or deactivates grid
   */
  void toggleGridState(unsigned int flag = 0) {
    model.getPresentationMode().toggleGridLine(flag);
  }

  Grid getGridState() const {
    return model.getPresentationMode().getGridState();
  }

  void toggleBlockLine() {
    model.getPresentationMode().toggleBlockLine();
  }

  bool isShowBlockLine() const {
    return model.getPresentationMode().isShowBlockLine();
  }

  void setJumpScale(int scale) {
    model.getControlPoint().setJumpScale(scale);
  }

  int getJumpScale() const {
    return model.getControlPoint().getJumpScale();
  }

  /**
   * jumps some time steps forward according th the jumpscale for time
   */
  void jumpForward() const {
    model.getDataManager().moveFrame(getJumpScale());
  }

  /**
   * jumps some time steps backward according to the jumpscale for time
   */
  void jumpBackward() const {
    model.getDataManager().moveFrame(-getJumpScale());
  }

  /** activate or deactivate numbers */
  void toggleLabels() {
    model.getPresentationMode().toggleLabel();
  }

  void switchColorbarSet(Direction dir) {
    model.getColorscaleManager().switchColorbarSet(dir);
  }

  void switchDataView(Direction dir) {
    lastDataView = getDataView();
    model.getDataManager().switchDataView(dir);
  }

  void switchDataView(int view) {
    lastDataView = getDataView();
    model.getDataManager().switchDataView(view);
  }

  void switchLastDataView() {
    auto tmp = getDataView();
    model.getDataManager().switchDataView(lastDataView);
    lastDataView = tmp;
  }

  int getDataView() const {
    return model.getDataManager().getView();
  }

  void switchComponent(Direction dir) {
    model.getDataManager().switchComponent(dir);
  }

  void switchComponent(int view) {
    model.getDataManager().switchComponent(view);
  }

  unsigned int getComponent() const {
    return model.getDataManager().getComponent();
  }

  unsigned int getNumberOfComponents() const {
    return model.getDataManager().getNumberOfComponents();
  }

  void setGridDataViewAuto() {
    model.getDataManager().setGridDataViewAuto();
  }

  void switchGridDataView(int view) {
    model.getDataManager().switchGridDataView(view);
  }

  int getGridDataView() const {
    return model.getDataManager().getGridView();
  }

  const std::vector<std::string>& getMappingNames() const {
    return model.getDataManager().getMappingNames();
  }

  int getNumberOfColorMappings() const {
    return model.getDataManager().getNumberOfViews();
  }

  void toggleDiscreteColor() {
    model.getColorscaleManager().toggleDiscreteColor();
  }

  bool colorBarIsDiscrete() const {
    return model.getColorscaleManager().isDiscrete();
  }

  void invertColorscale() {
    model.getColorscaleManager().invertColorscale();
  }

  void switchDataField() {
    model.switchDataField();
  }

  void maxTime() {
    model.getDataManager().moveFrame(std::numeric_limits<int>::max());
  }

  void minTime() {
    model.getDataManager().moveFrame(std::numeric_limits<int>::min());
  }

  void onElementChange() override { updateView(); }

  int lastDataView{0};  // holds the last selected dataView.
};

#endif  // SRC_PRESENTER_MENUBARPRESENTATION_H_
