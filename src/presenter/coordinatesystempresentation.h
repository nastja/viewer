/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_PRESENTER_COORDINATESYSTEMPRESENTATION_H_
#define SRC_PRESENTER_COORDINATESYSTEMPRESENTATION_H_

#include "event/modellistener.h"
#include "presenter/imagepresentation.h"
#include <FL/x.H>

/**
 * The Presenter Component for CoordinateSystemView that renders the currently rotated Coordinate System as an Image.
 *
 * @ingroup presenter
 */
class CoordinateSystemPresentation : public ImagePresentation {
public:
  explicit CoordinateSystemPresentation(ModelManager& modelManager);

  /**
   * Set the resolution which should be used for rendering the Image
   *
   * @param x the width
   * @param y the height
   */
  void setResolution(unsigned int x, unsigned int y);

  void onElementChange() override;

  Fl_Offscreen draw() override;

private:
  uvec2 resolution;
};

#endif  // SRC_PRESENTER_COORDINATESYSTEMPRESENTATION_H_
