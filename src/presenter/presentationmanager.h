/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_PRESENTER_PRESENTATIONMANAGER_H_
#define SRC_PRESENTER_PRESENTATIONMANAGER_H_

#include "model/modelmanager.h"
#include "presenter/colorscalepresentation.h"
#include "presenter/coordinatesystempresentation.h"
#include "presenter/infopresentation.h"
#include "presenter/menubarpresentation.h"
#include "presenter/datapresentation/datapresentation.h"
#include "view/guiframe.h"
#include "view/guisettings.h"
#include "view/guiwarning.h"
#include "view/helpwindow.h"

/**
 * @defgroup presenter
 *
 * This package is the intermediate layer between model and view. It consists of a PresentationManager which owns all Presentation objects.
 * Each Presentation object has a reference to ModelManager and is linkes to one specific View object.
 *
 * This layer is used to pass information from the GUI to the model, from the model to the GUI and to process the data on this way.
 */

/**
 * This class owns all currently exisiting Presentation objects. It is used in the Initialization process,
 * in which each View object will call one of the createXXXPresentation() methods to create the matching Presentation for the View.
 *
 * @ingroup presenter
 */
class PresentationManager {
public:
  /**
   * Constructs a new PresentationManager using a ModelManager
   *
   * @param modelManager the ModelManager to link to
   */
  explicit PresentationManager(ModelManager& modelManager)
      : dataPresentation(modelManager),
        infoPresentation(modelManager),
        coordinateSystemPresentation(modelManager),
        colorScalePresentation(modelManager),
        menuBarPresentation(modelManager) {}

  DataPresentation& getDataPresentation() { return dataPresentation; }
  InfoPresentation& getInfoPresentation() { return infoPresentation; }
  CoordinateSystemPresentation& getCoordinateSystemPresentation() { return coordinateSystemPresentation; }
  ColorScalePresentation& getColorScalePresentation() { return colorScalePresentation; }
  MenuBarPresentation& getMenuBarPresentation() { return menuBarPresentation; }
  GUISettings* getSettingsDialog() { return settingsDialog; }
  GUIWarning* getRotationWarning() { return rotationWarning; }
  GUIFrame& getGui() { return gui; }
  HelpWindow* getHelpWindow() { return helpWindow; }

private:
  DataPresentation dataPresentation;
  InfoPresentation infoPresentation;
  CoordinateSystemPresentation coordinateSystemPresentation;
  ColorScalePresentation colorScalePresentation;
  MenuBarPresentation menuBarPresentation;

  GUISettings* settingsDialog = new GUISettings();
  GUIWarning* rotationWarning = new GUIWarning();
  HelpWindow* helpWindow      = new HelpWindow(100, 100, 700, 400);
  GUIFrame gui{*this};
};

#endif  // SRC_PRESENTER_PRESENTATIONMANAGER_H_
