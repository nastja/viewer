/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_PRESENTER_DATAPRESENTATION_DATAPRESENTATION_H_
#define SRC_PRESENTER_DATAPRESENTATION_DATAPRESENTATION_H_

#include "model/modelmanager.h"
#include "presenter/imagepresentation.h"
#include "presenter/datapresentation/data1dmode.h"
#include "presenter/datapresentation/data2dmode.h"
#include "presenter/datapresentation/data3dmode.h"
#include "presenter/datapresentation/datapresentationmode.h"
#include "view/view.h"

/**
 * @defgroup dataPresentation
 *
 * This package contains the DataPresentation Presentation. This package renders the main image that is visible on the screen.
 *
 * It is organizes as a Strategy pattern. DataPresentation is the Facade to the View and Model, and the DataPresentationMode handles the actual rendering.
 *
 * @ingroup presenter
 */

/**
 * The Presenter Component for DataView that renders the main view into a Image.
 * It has different modes that can be switch in PresentationMode.
 *
 * @ingroup dataPresentation
 */
class DataPresentation : public ImagePresentation {
public:
  explicit DataPresentation(ModelManager& model);

  /**
   * change the presentation strategy in model
   */
  void selectPresentation(int presentation);

  /**
   * Set the resolution of all Strategies
   *
   * @param x the width
   * @param y the height
   */
  void setResolution(unsigned int x, unsigned int y);

  /**
   * returns the resolution
   */
  uvec2 getResolution() {
    return resolution;
  }

  /**
   *
   * @return the rendered image
   */
  Fl_Offscreen draw() override {
    return mode->draw();
  }

  /**
   * moves the controlPoint in the direction
   * @param direction moved to direction
   */
  void moveControlPoint(Direction direction) {
    mode->move(direction);
  }

  /**
   * controlPoint jump in one direction according to jumpscale
   * @param direction the direction
   */
  void jumpControlPoint(Direction direction) {
    mode->jump(direction);
  }
  /**
   * rotates the cuboid
   * @param rotation the used rotation
   */
  void rotate(Direction direction) {
    mode->rotate(direction);
  }

  /**
   * inverts or reverses inversion
   */
  void invert() {
    mode->invert();
  }

  /**
   *
   * @param x
   * @param y
   * @param rightClick
   */
  void clickAt(unsigned int x, unsigned int y, bool rightClick) {
    mode->clickAt(x, y, rightClick);
    updateView();
  }


  void resetSelection() {
    mode->resetSelection();
  }

  void release();

  void moveMax(Direction dir) {
    mode->moveMax(dir);
  }

  bool isCursorNS(unsigned int x, unsigned int y) {
    return mode->isCursorNS(x, y);
  }
  bool isCursorWE(unsigned int x, unsigned int y) {
    return mode->isCursorWE(x, y);
  }
  bool isCursorNWSE(unsigned int x, unsigned int y) {
    return mode->isCursorNWSE(x, y);
  }
  bool isCursorNESW(unsigned int x, unsigned int y) {
    return mode->isCursorNESW(x, y);
  }

  void saveValue(unsigned int x, unsigned int y) {
    model.getPresentationMode().setCurrentVortexValue(mode->getValue(x, y));
  }

  bool showsOneBlock();

  void onElementChange() override;

  void zoomIn() {
    mode->zoomIn();
  }

  void zoomOut() {
    mode->zoomOut();
  }

  bool is2DPresentation() {
    return is2D;
  }

private:
  bool is1D;
  bool is2D;
  uvec2 resolution;
  DataPresentationMode* mode;

  /**
   * changes mode to the right MultidimensionalMode
   */
  void changeToMultiDimMode();

  Data1DMode data1DMode;
  Data2DMode data2DMode;
  Data3DMode data3DMode;
};

#endif  // SRC_PRESENTER_DATAPRESENTATION_DATAPRESENTATION_H_
