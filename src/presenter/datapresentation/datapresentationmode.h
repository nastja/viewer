/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_PRESENTER_DATAPRESENTATION_DATAPRESENTATIONMODE_H_
#define SRC_PRESENTER_DATAPRESENTATION_DATAPRESENTATIONMODE_H_

#include "model/modelmanager.h"
#include "presenter/presentation.h"
#include "util/direction.h"
#include <FL/x.H>
#include <cmath>
#include <memory>

/**
 * Abstract base class for different Strategies to render the main view.
 *
 * @ingroup dataPresentation
 */
class DataPresentationMode {
public:
  explicit DataPresentationMode(ModelManager& model, uvec2 resolution) : model(model), resolution(resolution) {}
  virtual ~DataPresentationMode() = default;

  /**
   * set the Resulution to a new value
   *
   * @param resolution the new resolution
   */
  virtual void setResolution(const uvec2& resolution) { this->resolution = resolution; }

  virtual Fl_Offscreen draw() = 0;

  /**
   * move the ControlPoint
   *
   * @param direction the Direction to move in
   */
  virtual void move(Direction direction) = 0;

  virtual void moveMax(Direction dir) = 0;

  /**
   * rotatae the View
   *
   * @param rotate the Rotation action to perform
   */
  virtual void rotate(Direction direction) = 0;

  /**
   * Zoom in to Selection
   */
  virtual void zoomIn() = 0;

  /**
   * Zoom out of current selection
   */
  virtual void zoomOut() = 0;

  /**
   * Invert view in InvertMode
   */
  virtual void invert() = 0;

  /**
   * Toggle grid line mode in PresentationMode
   */
  virtual void changeGridLines() = 0;

  /**
   * toggle label mode in  PresentationMode
   */
  virtual void toggleLabel() = 0;

  /** handle click event at pixel coordinates x and y. Origin is in the top left corner.  */
  virtual void clickAt(unsigned int x, unsigned int y, bool rightClick) = 0;

  virtual void jump(Direction direction) = 0;

  virtual void resetSelection() = 0;

  virtual bool isCursorNS(unsigned int x, unsigned int y)   = 0;
  virtual bool isCursorWE(unsigned int x, unsigned int y)   = 0;
  virtual bool isCursorNWSE(unsigned int x, unsigned int y) = 0;
  virtual bool isCursorNESW(unsigned int x, unsigned int y) = 0;

  virtual void release() = 0;

  virtual std::string getValue(unsigned int x, unsigned y) = 0;

protected:
  ModelManager& model;

  uvec2 resolution;
};

#endif  // SRC_PRESENTER_DATAPRESENTATION_DATAPRESENTATIONMODE_H_
