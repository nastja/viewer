/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "data1dmode.h"
#include "model/continuouscolorscale.h"
#include "model/controlpoint.h"
#include <FL/fl_draw.H>
#include <algorithm>
#include <cmath>
// #include <cstring>

Data1DMode::Data1DMode(ModelManager& model, uvec2 resolution) : DataPresentationMode(model, resolution) {}

/**
 * Computes position, where one specific direction is replaced with a float value.
 *
 * @param direction     the direction
 * @param val           the position on this direction
 * @param controlPoint  the controlpoint's position
 *
 * @return a vector in the direction
 */
ivec3 Data1DMode::vectorInDirection(int direction, int val, const uvec3& controlPoint) {
  switch (direction) {
    case 0: return ivec3(val, static_cast<int>(controlPoint.y()), static_cast<int>(controlPoint.z()));
    case 1: return ivec3(static_cast<int>(controlPoint.x()), val, static_cast<int>(controlPoint.z()));
    case 2: return ivec3(static_cast<int>(controlPoint.x()), static_cast<int>(controlPoint.y()), val);
    default: throw std::runtime_error("Data1DMode: direction has wrong value, only 0,1,2 allowed.");
  }
}

Fl_Offscreen Data1DMode::draw() {
  auto view        = model.getDataManager().getRotatedView();
  auto& presmode   = model.getPresentationMode();
  unsigned int numberOfAxises = model.getDataManager().is2D() ? 2 : 3;
  Fl_Offscreen img = fl_create_offscreen(resolution.x(), resolution.y());
  fl_font(0, 14);
  unsigned int width      = resolution.x();  // width in px including left
  unsigned int height     = resolution.y() / numberOfAxises;
  unsigned int left       = 30;  // left padding
  unsigned int bot        = 20;
  unsigned int axisHeight = height - bot - 2;
  unsigned int axisWidth  = width - left - 10;
  std::array<float, 3> xScale{};
  auto max      = model.getColorscaleManager().getMax();
  auto min      = model.getColorscaleManager().getMin();
  auto ticks    = calculateTicks(max, min);
  auto lMin     = *ticks.begin();
  auto lMax     = *ticks.rbegin();
  auto exponent = ceil(log10(lMax - lMin)) - 1;
  float yScale  = (axisHeight - 1) / (lMax - lMin);

  uvec3 size = view.getDataSize();
  uvec3 pos  = view.transformToScreenSpace(model.getControlPoint().getControlPoint());

  for (int i = 0; i < numberOfAxises; i++) {
    xScale[i] = static_cast<float>(axisWidth) / static_cast<float>(size[i] - 1);
  }

  auto axisImg  = drawAxis(width, height);
  auto valueImg = drawValues(axisWidth, axisHeight, lMin, lMax);
  for (int i = 0; i < numberOfAxises; i++) {
    fl_begin_offscreen(axisImg[i]);
    fl_copy_offscreen(left + 1, 0, axisWidth, axisHeight, valueImg[i], 0, 0);

    //controlpoint
    fl_rectf((float)left + std::round(pos[i] * xScale[i]), 0, 2, height - bot / 2,
             presmode.isActive(i) && presmode.hasSelection() ? presmode.getColor(Colors::selection) : FL_BLACK);

    float valueRate = (float)(size[i] - 1) / axisWidth;
    valueRate       = (valueRate < 1) ? 1 : valueRate;

    // draw scale
    for (unsigned int j = 0; j * valueRate < size[i]; j++) {
      fl_rectf(left + std::round(j * valueRate * xScale[i]), axisHeight + 3, 1, bot / 2, FL_BLACK);

      fl_draw(std::to_string(j).c_str(), std::round(left + j * valueRate * xScale[i] - 10), height);
    }

    //y-axis
    for (auto tick : ticks) {
      double val = (axisHeight - 1) - (yScale * (tick - lMin));
      fl_rectf(left / 2, val, left / 2, 1, FL_BLACK);
      auto stringValue = std::to_string(tick);
      bool hasMinus    = stringValue.find('-') != std::string::npos;
      if (exponent > -2) {
        fl_draw(stringValue.substr(0, 3 + hasMinus).c_str(), 0, val + 12);
      } else {
        if (hasMinus) {
          fl_draw(("-" + stringValue.substr(abs(exponent) + 2, 1)).c_str(), 0, val + 12);
        } else {
          fl_draw(stringValue.substr(abs(exponent) + 1, 1).c_str(), 0, val + 12);
        }
      }
    }
    auto stringLMin = std::to_string(lMin);
    bool hasMinus   = stringLMin.find('-') != std::string::npos;
    if (exponent > 1) {
      fl_draw(("x10^" + std::to_string((int)(exponent))).c_str(), left, 15);
    } else if (exponent < -1) {
      auto offset = stod(stringLMin.substr(0, abs(exponent) + hasMinus + 1));
      if (offset == 0) {
        fl_draw(("x10^" + std::to_string((int)exponent)).c_str(), left, 15);
      } else {
        fl_draw(("x10^" + std::to_string((int)exponent) + " + " + std::to_string(offset)).c_str(), left, 15);
      }
    }
    fl_end_offscreen();
  }

  fl_begin_offscreen(img);
  fl_rectf(0, 0, resolution.x(), resolution.y(), FL_BACKGROUND_COLOR);
  for (int i = 0; i < numberOfAxises; i++) {
    fl_copy_offscreen(0, i * height, width, height, axisImg[i], 0, 0);
  }
  fl_end_offscreen();

  for (int i = 0; i < numberOfAxises; i++) {
    fl_delete_offscreen(valueImg[i]);
    fl_delete_offscreen(axisImg[i]);
  }

  return img;
}


std::array<Fl_Offscreen, 3> Data1DMode::drawAxis(unsigned int w, unsigned int h) {
  int left       = 30;
  int bot        = 20;
  char name      = 'x';
  auto& presmode = model.getPresentationMode();
  unsigned int numberOfAxises = model.getDataManager().is2D() ? 2 : 3;
  std::array<Fl_Offscreen, 3> img{};
  for (int i = 0; i < numberOfAxises; i++) {
    img[i] = fl_create_offscreen(w, h);
    fl_begin_offscreen(img[i]);
    fl_rectf(0, 0, w, h, FL_BACKGROUND_COLOR);
    fl_rectf(left, 0, w - left, h - bot - 1,
             presmode.isActive(i) && presmode.hasSelection() ? presmode.getColor(Colors::selection) : FL_BLACK);
    fl_rectf(left + 1, 0, w - left, h - bot - 2, FL_BACKGROUND_COLOR);
    fl_color(FL_BLACK);
    char temp[] = {name, '\0'};
    fl_draw(&temp[0], left - 10, h / 2);
    name++;

    fl_end_offscreen();
  }
  return img;
}

std::array<Fl_Offscreen, 3> Data1DMode::drawValues(unsigned int w, unsigned int h, double min, double max) {
  std::array<Fl_Offscreen, 3> img;
  auto& view         = model.getDataManager().getDataView();
  auto& presmode     = model.getPresentationMode();
  uvec3 pos          = model.getControlPoint().getControlPoint();
  uvec3 size         = view.getDataSize();
  auto lineThickness = presmode.getLineThickness();
  unsigned int numberOfAxises = model.getDataManager().is2D() ? 2 : 3;

  std::array<float, 3> xScale;
  float yScale = (h - 1) / (max - min);
  float yBias  = min;

  for (int i = 0; i < numberOfAxises; i++) {
    xScale[i] = (float)w / (size[i] - 1);

    img[i] = fl_create_offscreen(w, h);
    fl_begin_offscreen(img[i]);

    fl_rectf(0, 0, w, h, FL_WHITE);

    float valueRate = size[i] / w;

    valueRate = (valueRate < 1) ? 1 : valueRate;

    fl_color(FL_BLUE);
    for (unsigned int j = 1; j * valueRate < size[i]; j++) {
      float k   = j * valueRate;
      auto tmp1 = view.getDouble(vectorInDirection(i, k - valueRate, pos));
      auto tmp2 = view.getDouble(vectorInDirection(i, k, pos));
      double val1, val2;
      val1 = (h - 1) - (yScale * (tmp1 - yBias));
      val2 = (h - 1) - (yScale * (tmp2 - yBias));

      if ((val1 >= 0 && val1 <= h) || (val2 >= 0 && val2 <= h)) {
        float x1     = xScale[i] * (k - valueRate);
        float x2     = xScale[i] * k;
        float a      = (val2 - val1) / (x2 - x1);
        float b      = val1 - a * x1;
        bool drawDot = val1 >= 0 && val1 <= h - 1;
        bool lastDot = val2 >= 0 && val2 <= h - 1 && k + valueRate >= size[i];

        if (val1 > h - 1) {
          drawDot = false;
          val1    = h - 1;
          x1      = (val1 - b) / a;
        } else if (val1 < 0) {
          drawDot = false;
          val1    = 0;
          x1      = (val1 - b) / a;
        }
        if (val2 > h - 1) {
          val2 = h - 1;
          x2   = (val2 - b) / a;
        } else if (val2 < 0) {
          val2 = 0;
          x2   = (val2 - b) / a;
        }

        val1 = (val1 > h - 1) ? h - 1 : val1;
        val1 = (val1 < 0) ? 0 : val1;
        val2 = (val2 > h - 1) ? h - 1 : val2;
        val2 = (val2 < 0) ? 0 : val2;

        switch (presmode.getLineStyle()) {
          case NORMAL:
            fl_line_style(FL_SOLID, lineThickness);
            fl_line(std::round(x1), std::round(val1), std::round(x2), std::round(val2));
            fl_line_style(FL_SOLID, 1);
            break;
          case DOTTED:
            if (drawDot) {
              int dotVal1 = val1 - lineThickness / 2;
              dotVal1     = (dotVal1 > 0) ? dotVal1 : 0;
              int dotx1   = x1 - lineThickness / 2;
              dotx1       = (dotx1 > 0) ? dotx1 : 0;
              fl_pie(dotx1, dotVal1, lineThickness + 1, lineThickness + 1, 0, 360);
              if (lastDot) {
                int lastVal1 = val2 - lineThickness / 2;
                lastVal1     = (lastVal1 > 0) ? lastVal1 : 0;
                int lastx1   = x2 - lineThickness / 2;
                lastx1       = (lastx1 > 0) ? lastx1 : 0;
                fl_pie(lastx1, lastVal1, lineThickness + 1, lineThickness + 1, 0, 360);
              }
            }
            break;
          case DASHED:
            fl_line_style(FL_DASH, lineThickness);
            fl_line(x1, val1, x2, val2);
            fl_line_style(FL_SOLID, 1);
            break;
        }
      }
    }
    fl_end_offscreen();
  }
  return img;
}

void Data1DMode::move(Direction direction) {
  auto& presmode     = model.getPresentationMode();
  auto& controlPoint = model.getControlPoint();
  switch (direction) {
    case Direction::up:
      presmode.activatePreviousAxis();
      break;
    case Direction::down:
      presmode.activateNextAxis();
      break;
    case Direction::left:
      if (presmode.isActive(0))
        controlPoint.move(direction);
      if (presmode.isActive(1))
        controlPoint.move(Direction::down);
      if (presmode.isActive(2))
        controlPoint.move(Direction::backward);
      break;
    case Direction::right:
      if (presmode.isActive(0))
        controlPoint.move(direction);
      if (presmode.isActive(1))
        controlPoint.move(Direction::up);
      if (presmode.isActive(2))
        controlPoint.move(Direction::forward);
      break;
    default: {
    }
  }
}

void Data1DMode::moveMax(Direction direction) {
  auto& presmode     = model.getPresentationMode();
  auto& controlPoint = model.getControlPoint();
  switch (direction) {
    case Direction::left:
      if (presmode.isActive(0))
        controlPoint.moveMax(direction);
      if (presmode.isActive(1))
        controlPoint.moveMax(Direction::down);
      if (presmode.isActive(2))
        controlPoint.moveMax(Direction::forward);
      break;
    case Direction::right:
      if (presmode.isActive(0))
        controlPoint.moveMax(direction);
      if (presmode.isActive(1))
        controlPoint.moveMax(Direction::up);
      if (presmode.isActive(2))
        controlPoint.moveMax(Direction::backward);
      break;
    default: {
    }
  }
}

void Data1DMode::jump(Direction direction) {
  auto& presmode     = model.getPresentationMode();
  auto& controlPoint = model.getControlPoint();
  switch (direction) {
    case Direction::left:
      if (presmode.isActive(0))
        controlPoint.jump(direction);
      if (presmode.isActive(1))
        controlPoint.jump(Direction::down);
      if (presmode.isActive(2))
        controlPoint.jump(Direction::forward);
      break;
    case Direction::right:
      if (presmode.isActive(0))
        controlPoint.jump(direction);
      if (presmode.isActive(1))
        controlPoint.jump(Direction::up);
      if (presmode.isActive(2))
        controlPoint.jump(Direction::backward);
      break;
    default: {
    }
  }
}

void Data1DMode::rotate(Direction direction) {
  auto& presmode     = model.getPresentationMode();
  auto lineThickness = presmode.getLineThickness();
  switch (direction) {
    case Direction::up:
      presmode.setGraphLineThickness(lineThickness + 1);
      break;
    case Direction ::down:
      if (lineThickness > 1)
        presmode.setGraphLineThickness(lineThickness - 1);
      break;
    case Direction::left:
      presmode.previousLineStyle();
      break;
    case Direction::right:
      presmode.nextLineStyle();
      break;
    default: {
    }
  }
}

/**
 * Based on https://doi.org/10.1109/TVCG.2010.130
 *
 * @param max  the maximum of the data
 * @param min  the minimum of the data
 *
 * @return set of ticks for an axis
 */
std::set<double> Data1DMode::calculateTicks(double max, double min) {
  std::vector<float> bases{1, 5, 2, 4, 3};
  std::array<float, 3> param{1, 1, 2};
  std::set<double> result;
  float maxScore = -std::numeric_limits<float>::max();
  float i        = 1;
  for (auto base : bases) {
    float maxSimplicty = 1 - (i - 1) / (float)(bases.size() - 1);
    for (float j = 1; maxScore < param[0] * maxSimplicty + param[1] + param[2]; j++) {
      maxSimplicty     = 1 - (i - 1) / (float)(bases.size() - 1) - j + 1;
      float maxDensity = 1;
      for (float k = 2; maxScore < param[0] * maxSimplicty + param[1] * maxDensity + param[2]; k++) {
        maxDensity        = 2 - std::max(k / 5, 5 / k);
        float delta       = (max - min) / k / (j * base);
        float maxCoverage = 1;
        for (int z = ceil(log10(delta)); maxScore < param[0] * maxSimplicty + param[1] * maxDensity + param[2] * maxCoverage; z++) {
          float lStep = base * j * pow(10, z);
          maxCoverage = 1 - abs(max - (k * lStep + min));
          for (float start = floor(max / lStep) - k; start <= ceil(min / lStep); start += 1 / j) {
            float lMin  = start * lStep;
            float lMax  = lMin + k * lStep;
            auto labels = createLabels(lMin, lMax, lStep);
            auto simpl  = simplicity(i, bases.size(), j, labels);
            auto cover  = coverage(lMin, lMax, min, max);
            auto dens   = 2 - std::max((float)labels.size() / 5, 5 / (float)labels.size());
            if (maxScore < param[0] * simpl + param[1] * dens + param[2] * cover) {
              maxScore = param[0] * simpl + param[1] * dens + param[2] * cover;
              result   = labels;
            }
          }
        }
      }
    }
    i++;
  }
  return result;
}

float Data1DMode::simplicity(int baseIndex, int baseSize, int skip, const std::set<double>& labels) {
  auto hasZero = labels.find(0) != labels.end();
  return 1 - (float)(baseIndex - 1) / (float)(baseSize - 1) - skip + hasZero;
}

float Data1DMode::coverage(double lmin, double lmax, double dmin, double dmax) {
  return 1 - (pow(dmax - lmax, 2) + pow(dmin - lmin, 2)) / (0.1 * pow(dmax - dmin, 2));
}

std::set<double> Data1DMode::createLabels(double min, double max, double step) {
  std::set<double> result;
  for (double i = min; i <= max; i += step) {
    result.insert(i);
  }
  return result;
}
