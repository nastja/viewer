/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "data3dmode.h"
#include "model/coordinatesystem.h"
#include "util/boundingbox.h"
#include "util/direction.h"
#include <FL/Enumerations.H>
#include <FL/fl_draw.H>
#include <algorithm>
#include <cmath>

constexpr double perspectiveFactor = 0.5 * M_SQRT1_2;  // 1/sqrt(8) x, y scale for 45 degree 1/2 factor on z-axis

Fl_Offscreen Data3DMode::draw() {
  auto view         = model.getDataManager().getRotatedView();
  auto img          = fl_create_offscreen(resolution.x(), resolution.y());
  auto controlPoint = view.transformToScreenSpace(model.getControlPoint().getControlPoint());

  // COMPUTE HELPER VARIABLES

  // size in cells
  uvec3 size   = view.getDataSize();
  auto spacing = view.getSpacing();
  auto xyRatio = spacing.y() / spacing.x();
  auto xzRatio = spacing.z() / spacing.x();
  auto yzRatio = spacing.z() / spacing.y();
  float xSpacing;
  float ySpacing;
  float zSpacing;
  xSpacing = (float)resolution.x() / ((float)size.x() + xzRatio * perspectiveFactor * size.z());
  ySpacing = (float)(resolution.y() - 10) / ((float)size.y() + yzRatio * perspectiveFactor * size.z());
  if (xSpacing * xyRatio <= ySpacing) {
    ySpacing = xyRatio * xSpacing;
    zSpacing = xzRatio * xSpacing;
  } else {
    xSpacing = ySpacing / xyRatio;
    zSpacing = yzRatio * ySpacing;
  }
  sizePerCell = vec3(xSpacing, ySpacing, zSpacing);

  // origin.x() is the z axis plus half of the margin
  origin = uvec2(
      static_cast<int>(round(sizePerCell.z() * perspectiveFactor * size.z() + (resolution.x() - sizePerCell.z() * perspectiveFactor * size.z() - size.x() * sizePerCell.x()) / 2)),
      static_cast<int>(round(sizePerCell.y() * size.y() + (resolution.y() - sizePerCell.z() * perspectiveFactor * size.z() - size.y() * sizePerCell.y()) / 2)));

  visCoordSys.setOrigin(origin);
  visCoordSys.setResolution(resolution);
  visCoordSys.setSizePerCell(sizePerCell);
  visCoordSys.setControlPoint(model.getDataManager().getRotatedView().transformToScreenSpace(model.getControlPoint().getControlPoint()));

  drawer.setRotatedBorder(calculateRotatedBorders());

  // PAINT SCREEN
  fl_begin_offscreen(img);
  fl_rectf(0, 0, resolution.x(), resolution.y(), FL_BACKGROUND_COLOR);

  if (model.getPresentationMode().getRedraw()) {
    if (model.getInvertMode().isInverted()) {
      drawer.renderInverted(controlPoint);
    } else {
      drawer.renderNormal(controlPoint);
    }
    // make a backup copy
    img_data.clear();
    img_data.resize(resolution.x() * resolution.y() * 3);
    fl_read_image(img_data.data(), 0, 0, resolution.x(), resolution.y());
  }

  if (!model.getInvertMode().isInverted() && model.getPresentationMode().hasSelection()) {
    // if we have a selection, get backup copy and draw selection.
    fl_draw_image(img_data.data(), 0, 0, resolution.x(), resolution.y());
    auto controlPoint = model.getDataManager().getRotatedView().transformToScreenSpace(model.getControlPoint().getControlPoint());
    auto labels       = model.getPresentationMode().hasLabels();
    selection.drawSelection(labels, controlPoint);
  }

  fl_end_offscreen();
  return img;
}

void Data3DMode::rotate(Direction direction) {
  if (model.getInvertMode().isInverted())
    invert();

  model.getCoordinateSystem().rotate(direction);
}

void Data3DMode::invert() {
  model.getInvertMode().toggleInverted();

  if (model.getInvertMode().isInverted()) {
    auto controlPoint = model.getControlPoint().getControlPoint();
    controlPoint.x() += (controlPoint.x() == 0);
    controlPoint.y() += (controlPoint.y() == 0);
    controlPoint.z() += (controlPoint.z() == 0);
    model.getControlPoint().setControlPoint(controlPoint);
  }

  if (model.getPresentationMode().hasSelection()) {
    model.getPresentationMode().toggleSelection();
    model.getPresentationMode().setRedraw(true);
  }
}

void Data3DMode::clickAt(unsigned int x, unsigned int y, bool secondClick) {
  if (model.getInvertMode().isInverted()) return;

  auto ssControlPoint = model.getDataManager().getRotatedView().transformToScreenSpace(model.getControlPoint().getControlPoint());

  if (!draggingEdge) {
    this->selection.clickAt(x, y, !secondClick, ssControlPoint);
  }
}

bool Data3DMode::isCursorNS(unsigned int x, unsigned int y) {
  return selection.isTopEdgeOnFrontPlane(x, y) || selection.isBotEdgeOnFrontPlane(x, y) ||
         selection.isFrontEdgeOnTopPlane(x, y) || selection.isBackEdgeOnTopPlane(x, y);
}

bool Data3DMode::isCursorWE(unsigned int x, unsigned int y) {
  return selection.isLeftEdgeOnFrontPlane(x, y) || selection.isRightEdgeOnFrontPlane(x, y) ||
         selection.isFrontEdgeOnSidePlane(x, y) || selection.isBackEdgeOnSidePlane(x, y);
}

bool Data3DMode::isCursorNWSE(unsigned int x, unsigned int y) {
  return (selection.isTopEdgeOnFrontPlane(x, y) && selection.isLeftEdgeOnFrontPlane(x, y)) ||
         (selection.isBotEdgeOnFrontPlane(x, y) && selection.isRightEdgeOnFrontPlane(x, y)) ||
         selection.isLeftEdgeOnTopPlane(x, y) || selection.isRightEdgeOnTopPlane(x, y) ||
         selection.isTopEdgeOnSidePlane(x, y) || selection.isBotEdgeOnSidePlane(x, y);
}

bool Data3DMode::isCursorNESW(unsigned int x, unsigned int y) {
  return (selection.isTopEdgeOnFrontPlane(x, y) && selection.isRightEdgeOnFrontPlane(x, y)) ||
         (selection.isBotEdgeOnFrontPlane(x, y) && selection.isLeftEdgeOnFrontPlane(x, y));
}

void Data3DMode::release() {
  if (draggingEdge) {
    auto a            = selection.getBox()[0];
    auto controlPoint = model.getDataManager().getRotatedView().transformToScreenSpace(model.getControlPoint().getControlPoint());
    draggingEdge      = false;

    model.getPresentationMode().toggleSelection();
    model.getPresentationMode().setRedraw(true);
    auto pos = model.getDataManager().getRotatedView().transformFromScreenSpace(uvec3(draggingXAxis ? a.x() : controlPoint.x(),
                                                                                      draggingYAxis ? a.y() : controlPoint.y(),
                                                                                      draggingZAxis ? a.z() - 1 : controlPoint.z()));
    model.getControlPoint().setControlPoint(pos);

    draggingXAxis = false;
    draggingYAxis = false;
    draggingZAxis = false;
  }
  selection.release();
}

void Data3DMode::zoomIn() {
  auto sel          = selection.getBox();
  auto controlPoint = model.getDataManager().getRotatedView().transformToScreenSpace(model.getControlPoint().getControlPoint());
  if (sel[1].x() > controlPoint.x() || sel[1].y() > controlPoint.y() || sel[1].z() > controlPoint.z() + 1) {
    sel = BoundingBox<unsigned int>(uvec3(0, 0, 0), controlPoint + uvec3(0, 0, 1));
  }
  model.zoomIn(sel);
}

void Data3DMode::zoomOut() {
  model.zoomOut();
  selection.setBox(BoundingBox<unsigned int>(uvec3(0, 0, 0), model.getDataManager().getRotatedView().getDataSize()));
}

std::tuple<ivec3, ivec3> Data3DMode::calculateRotatedBorders() {
  auto anchorA        = model.getAnchorPointStack().getAnchorPoints().getAnchorA();
  auto anchorB        = model.getAnchorPointStack().getAnchorPoints().getAnchorB();
  auto size           = model.getDataManager().getDataView().getDataSize();
  auto rotatedBorderA = model.getCoordinateSystem().transformVector(anchorA);
  auto borderB        = size - anchorB - vec3(1, 1, 1);
  auto rotatedBorderB = model.getCoordinateSystem().transformVector(borderB);

  for (int i = 0; i < 3; i++) {
    rotatedBorderB[i] = (rotatedBorderB[i] > 0) ? 0 : rotatedBorderB[i];
    rotatedBorderA[i] = (rotatedBorderA[i] < 0) ? 0 : rotatedBorderA[i];
  }

  return {rotatedBorderA, rotatedBorderB};
}

void Data3DMode::setResolution(const uvec2& resolution) {
  this->resolution = resolution;

  if (model.getPresentationMode().hasSelection()) {
    model.getPresentationMode().setRedraw(true);
    auto controlPoint = model.getDataManager().getRotatedView().transformToScreenSpace(
        model.getControlPoint().getControlPoint());
    auto labels = model.getPresentationMode().hasLabels();
    selection.drawSelection(labels, controlPoint);
  }
}

std::string Data3DMode::getValue(unsigned int x, unsigned y) {
  auto coord = visCoordSys.inverseTransformCoord(uvec2(x, y));
  coord.z() -= 1;
  auto ssControlPoint = model.getDataManager().getRotatedView().transformToScreenSpace(model.getControlPoint().getControlPoint());
  if (coord.x() <= ssControlPoint.x() && coord.y() <= ssControlPoint.y() &&
      coord.z() <= ssControlPoint.z()) {
    return model.getDataManager().getRotatedView().getString(coord);
  } else {
    return "";
  }
}
