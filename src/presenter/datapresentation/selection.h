/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef NASTJA_VIEWER_SELECTION_H
#define NASTJA_VIEWER_SELECTION_H

#include "model/controlpoint.h"
#include "model/presentationmode.h"
#include "presenter/datapresentation/visualizationcoordinatesystem.h"
#include "util/boundingbox.h"

class Selection {
public:
  explicit Selection(VisualizationCoordinateSystem& coordSys, PresentationMode& presentationMode, const uvec3& size)
      : coordSys(coordSys), presentationMode(presentationMode) {
    box = BoundingBox<unsigned int>(uvec3(0, 0, 0), size);
  }

  void drawSelection(bool drawLabels, const uvec3& controlPoint);

  void clickAt(unsigned int x, unsigned int y, bool init, const uvec3& controlPoint);
  void release();

  Fl_Color getColor(Colors color) const { return presentationMode.getColor(color); }

  bool isBackEdgeOnTopPlane(unsigned int x, unsigned int y);
  bool isFrontEdgeOnTopPlane(unsigned int x, unsigned int y);
  bool isRightEdgeOnTopPlane(unsigned int x, unsigned int y);
  bool isLeftEdgeOnTopPlane(unsigned int x, unsigned int y);
  bool isBackEdgeOnSidePlane(unsigned int x, unsigned int y);
  bool isFrontEdgeOnSidePlane(unsigned int x, unsigned int y);
  bool isTopEdgeOnSidePlane(unsigned int x, unsigned int y);
  bool isBotEdgeOnSidePlane(unsigned int x, unsigned int y);
  bool isTopEdgeOnFrontPlane(unsigned int x, unsigned int y);
  bool isBotEdgeOnFrontPlane(unsigned int x, unsigned int y);
  bool isLeftEdgeOnFrontPlane(unsigned int x, unsigned int y);
  bool isRightEdgeOnFrontPlane(unsigned int x, unsigned int y);

  BoundingBox<unsigned int> getBox() { return box; }
  void setBox(BoundingBox<unsigned int> box) { this->box = box; }

private:
  inline void drawBox(uvec2 upperleft, uvec2 upperright, uvec2 lowerright, uvec2 lowerleft) const;
  inline int labelWidth(const std::string& label) const;

  bool isEdgeOnFrontPlane(unsigned int x, unsigned int y, int axis, int line);
  bool isEdgeOnSidePlane(unsigned int x, unsigned int y, int axis, int line);

  VisualizationCoordinateSystem& coordSys;
  PresentationMode& presentationMode;
  uvec3 fixedPoint;
  uvec3 controlPoint;
  BoundingBox<unsigned int> box;

  bool dragging{false};
  bool draggingX{false};
  bool draggingLeft{false};
  bool draggingY{false};
  bool draggingBot{false};
  bool draggingZ{false};
  bool draggingBack{false};
};

#endif  //NASTJA_VIEWER_SELECTION_H
