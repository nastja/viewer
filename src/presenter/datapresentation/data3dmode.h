/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_PRESENTER_DATAPRESENTATION_DATA3DMODE_H_
#define SRC_PRESENTER_DATAPRESENTATION_DATA3DMODE_H_

#include "model/modelmanager.h"
#include "presenter/datapresentation/datapresentationmode.h"
#include "presenter/datapresentation/drawer3d.h"
#include "presenter/datapresentation/multidimensionalmode.h"
#include "presenter/datapresentation/selection.h"
#include "presenter/datapresentation/visualizationcoordinatesystem.h"
#include "util/rect.h"
#include <tuple>

/**
 * DataPresentationMode which shows the cuboid in cavalier perspective.
 *
 * @ingroup dataPresentation
 */
class Data3DMode : public MultidimensionalMode {
public:
  explicit Data3DMode(ModelManager& model, uvec2 resolution)
      : MultidimensionalMode(model, resolution),
        img_data(resolution.x() * resolution.y()),
        visCoordSys(model.getDataManager().getRotatedView().transformToScreenSpace(model.getControlPoint().getControlPoint())),
        selection(visCoordSys, model.getPresentationMode(), model.getDataManager().getRotatedView().getDataSize()),
        drawer(visCoordSys, model.getDataManager().getRotatedView(), model.getPresentationMode()) {}

  Fl_Offscreen draw() override;

  void setResolution(const uvec2& resolution) override;

  void rotate(Direction direction) override;
  void invert() override;

  void clickAt(unsigned int x, unsigned int y, bool rightClick) override;
  void release() override;

  void zoomIn() override;
  void zoomOut() override;

  bool isCursorNS(unsigned int x, unsigned int y) override;
  bool isCursorWE(unsigned int x, unsigned int y) override;
  bool isCursorNWSE(unsigned int x, unsigned int y) override;
  bool isCursorNESW(unsigned int x, unsigned int y) override;

  std::string getValue(unsigned int x, unsigned y) override;

private:
  double distance(const vec3& v1, const vec3& v2) {
    return sqrt(pow(v1.x() - v2.x(), 2) + pow(v1.y() - v2.y(), 2) + pow(v1.z() - v2.z(), 2));
  }

  std::tuple<ivec3, ivec3> calculateRotatedBorders();

  ivec2 origin;      ///< origin of cube in pixels
  vec3 sizePerCell;  ///< size of one cells in pixels
  std::vector<unsigned char> img_data;

  bool draggingEdge{false};

  bool draggingXAxis{false};
  bool draggingYAxis{false};
  bool draggingZAxis{false};

  VisualizationCoordinateSystem visCoordSys;
  Selection selection;
  Drawer3D drawer;
};

#endif  // SRC_PRESENTER_DATAPRESENTATION_DATA3DMODE_H_
