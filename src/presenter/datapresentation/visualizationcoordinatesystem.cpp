/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "visualizationcoordinatesystem.h"
#include <cmath>

constexpr double perspectiveFactor = 0.5 * M_SQRT1_2;  // 1/sqrt(8) x, y scale for 45 degree 1/2 factor on z-axis

/**
 * transform data coordinates to screen coordinates
 *
 * @param coord  data coordinates
 *
 * @return screen coordinates
 */
uvec2 VisualizationCoordinateSystem::transformCoord(const uvec3& coord) {
  uvec2 vec{
      static_cast<unsigned int>(origin.x() + std::round(sizePerCell.x() * coord.x()) - std::round(sizePerCell.z() * coord.z() * perspectiveFactor)),
      static_cast<unsigned int>(origin.y() - std::round(sizePerCell.y() * coord.y()) + std::round(sizePerCell.z() * coord.z() * perspectiveFactor))};

  // clamp to allowed range
  if (vec.x() >= resolution.x()) vec.x() = resolution.x() - 1;
  if (vec.y() >= resolution.y()) vec.y() = resolution.y() - 1;

  return vec;
}

uvec3 VisualizationCoordinateSystem::inverseTransformCoord(const uvec2& coord) {
  auto localOrigin = origin + ivec2{static_cast<int>(round(-perspectiveFactor * sizePerCell.z() * (controlPoint.z() + 1))),
                                    static_cast<int>(round(perspectiveFactor * sizePerCell.z() * (controlPoint.z() + 1)))};

  unsigned int xIndex = ((float)coord.x() - (float)localOrigin.x()) / sizePerCell.x();
  unsigned int yIndex = (localOrigin.y() - coord.y()) / sizePerCell.y();
  unsigned int zIndex = controlPoint.z() + 1;

  if (xIndex > controlPoint.x()) {
    float temp;
    xIndex = controlPoint.x();
    temp   = ((float)coord.x() - (float)origin.x() - sizePerCell.x() * ((float)xIndex + 1)) / (-perspectiveFactor * sizePerCell.z());
    yIndex = round(((float)coord.y() - (float)origin.y() - sizePerCell.z() * perspectiveFactor * temp) / -sizePerCell.y());
    zIndex = round(temp);
  }

  if (yIndex > controlPoint.y()) {
    yIndex = controlPoint.y();
    float temp;
    temp   = ((float)coord.y() - (float)origin.y() + sizePerCell.y() * ((float)yIndex + 1)) / (perspectiveFactor * sizePerCell.z());
    xIndex = round(((float)coord.x() - (float)origin.x() + sizePerCell.z() * (perspectiveFactor * temp)) / sizePerCell.x());
    zIndex = round(temp);
  }
  return uvec3(xIndex, yIndex, zIndex);
}
