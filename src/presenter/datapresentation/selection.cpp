/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "selection.h"
#include "util/boundingbox.h"
#include <FL/fl_draw.H>

constexpr int distanceOfNumbers = 2;

/**
 * Draws a box.
 *
 * @param upperleft   The upper left corner.
 * @param upperright  The upper right corner.
 * @param lowerright  The lower right corner.
 * @param lowerleft   The lower left corner.
 */
inline void Selection::drawBox(uvec2 upperleft, uvec2 upperright, uvec2 lowerright, uvec2 lowerleft) const {
  fl_loop(upperleft.x(), upperleft.y(),
          upperright.x(), upperright.y(),
          lowerright.x(), lowerright.y(),
          lowerleft.x(), lowerleft.y());
}

inline int Selection::labelWidth(const std::string& label) const {
  int labelHeight = 0;
  int labelWidth  = 0;
  fl_measure(label.c_str(), labelWidth, labelHeight);
  return labelWidth;
}

/**
 * Draws the selection box if present.
 *
 * @param drawLabels    Flag for drawing the labels of the size.
 */
void Selection::drawSelection(bool drawLabels, const uvec3& controlPoint) {
  this->controlPoint = controlPoint;

  if (box[1].x() > controlPoint.x() || box[1].y() > controlPoint.y() || box[1].z() > controlPoint.z() + 1) {
    return;
  }

  auto a = box[0];
  auto b = box[1] + uvec3(1, 1, 0);

  fl_color(getColor(Colors::selection));

  // front plane
  // TL .--. TR
  //    |  |
  // BL .--. BR
  auto frontBottomLeft  = coordSys.transformCoord(uvec3(a.x(), a.y(), controlPoint.z() + 1));
  auto frontBottomRight = coordSys.transformCoord(uvec3(b.x(), a.y(), controlPoint.z() + 1));
  auto frontTopLeft     = coordSys.transformCoord(uvec3(a.x(), b.y(), controlPoint.z() + 1));
  auto frontTopRight    = coordSys.transformCoord(uvec3(b.x(), b.y(), controlPoint.z() + 1));
  drawBox(frontTopLeft, frontTopRight, frontBottomRight, frontBottomLeft);

  // top plane
  //   BL .--. BR
  //     /  /
  // FL .--. FR
  auto topFrontLeft  = coordSys.transformCoord(uvec3(a.x(), controlPoint.y() + 1, b.z()));
  auto topFrontRight = coordSys.transformCoord(uvec3(b.x(), controlPoint.y() + 1, b.z()));
  auto topBackLeft   = coordSys.transformCoord(uvec3(a.x(), controlPoint.y() + 1, a.z()));
  auto topBackRight  = coordSys.transformCoord(uvec3(b.x(), controlPoint.y() + 1, a.z()));
  drawBox(topBackLeft, topBackRight, topFrontRight, topFrontLeft);

  // right plane
  //   TF .--. TB
  //     /  /
  // BF .--. BB
  auto rightBottomFront = coordSys.transformCoord(uvec3(controlPoint.x() + 1, a.y(), b.z()));
  auto rightBottomBack  = coordSys.transformCoord(uvec3(controlPoint.x() + 1, a.y(), a.z()));
  auto rightTopFront    = coordSys.transformCoord(uvec3(controlPoint.x() + 1, b.y(), b.z()));
  auto rightTopBack     = coordSys.transformCoord(uvec3(controlPoint.x() + 1, b.y(), a.z()));
  drawBox(rightTopFront, rightTopBack, rightBottomBack, rightBottomFront);

  if (drawLabels) {
    auto labelX    = std::to_string(b.x() - a.x());
    int edgeLength = frontTopRight.x() - frontTopLeft.x();
    int start      = frontTopLeft.x() + 0.5 * (edgeLength - labelWidth(labelX));
    fl_draw(labelX.c_str(), start, frontTopLeft.y() - distanceOfNumbers);

    auto labelY = std::to_string(b.y() - a.y());
    edgeLength  = frontBottomLeft.y() - frontTopLeft.y();
    start       = frontBottomLeft.y() - 0.5 * (edgeLength - labelWidth(labelY));
    fl_draw(90, labelY.c_str(), frontTopLeft.x() - distanceOfNumbers, start);

    auto labelZ = std::to_string(b.z() - a.z());
    auto diff   = topBackLeft - topFrontLeft;
    start       = M_SQRT1_2 * 0.5 * (diff.length() - labelWidth(labelZ));
    fl_draw(45, labelZ.c_str(), topFrontLeft.x() + start - M_SQRT1_2 * distanceOfNumbers, topFrontLeft.y() - start - M_SQRT1_2 * distanceOfNumbers);
    fl_draw(45, labelZ.c_str(), rightTopFront.x() + start - M_SQRT1_2 * distanceOfNumbers, rightTopFront.y() - start - M_SQRT1_2 * distanceOfNumbers);
  }
}

void Selection::clickAt(unsigned int x, unsigned int y, bool init, const uvec3& controlPoint) {
  this->controlPoint = controlPoint;

  auto selection = coordSys.inverseTransformCoord(uvec2(x, y));

  auto a = box[0];
  auto b = box[1];

  auto newBox = BoundingBox<unsigned int>(a, b);

  if (!dragging) {
    draggingLeft = isLeftEdgeOnFrontPlane(x, y) || isLeftEdgeOnTopPlane(x, y);
    draggingBot  = isBotEdgeOnFrontPlane(x, y) || isBotEdgeOnSidePlane(x, y);
    draggingBack = isBackEdgeOnTopPlane(x, y) || isBackEdgeOnSidePlane(x, y) || draggingBack;
    dragging = true;
  }

  if (draggingZ || ((isFrontEdgeOnTopPlane(x, y) || isBackEdgeOnTopPlane(x, y) ||
                     isFrontEdgeOnSidePlane(x, y) || isBackEdgeOnSidePlane(x, y)) &&
                    presentationMode.hasSelection() && init)) {
    draggingZ = true;

    if (draggingBack) {
      a.z() = (selection.z() < b.z()) ? selection.z() : b.z() - 1;
    } else {
      b.z() = (selection.z() > a.z()) ? selection.z() : a.z() + 1;
    }
  }

  if (draggingX || ((isLeftEdgeOnFrontPlane(x, y) || isRightEdgeOnFrontPlane(x, y) ||
                     isLeftEdgeOnTopPlane(x, y) || isRightEdgeOnTopPlane(x, y)) &&
                    presentationMode.hasSelection() && init)) {
    draggingX = true;

    if (draggingLeft) {
      newBox[0].x() = (newBox[1].x() >= selection.x()) ? selection.x() : newBox[1].x();
    } else {
      newBox[1].x() = (newBox[0].x() <= selection.x()) ? selection.x() - isRightEdgeOnTopPlane(x, y) : newBox[0].x();
    }
  }

  if (draggingY || ((isBotEdgeOnFrontPlane(x, y) || isTopEdgeOnFrontPlane(x, y) ||
                     isTopEdgeOnSidePlane(x, y) || isBotEdgeOnSidePlane(x, y)) &&
                    presentationMode.hasSelection() && init)) {
    draggingY = true;

    if (draggingBot) {
      newBox[0].y() = (newBox[1].y() >= selection.y()) ? selection.y() : newBox[1].y();
    } else {
      newBox[1].y() = (newBox[0].y() <= selection.y()) ? selection.y() - isTopEdgeOnSidePlane(x, y) : newBox[0].y();
    }
  }
  if (draggingX || draggingY) {
    box = newBox;
  } else {
    box = BoundingBox<unsigned int>(a, b);
  }

  if (!(draggingX || draggingY || draggingZ) && selection.x() <= controlPoint.x() && selection.y() <= controlPoint.y() &&
      selection.z() <= controlPoint.z() + 1) {
    selection.z() = controlPoint.z() + 1;
    if (init) {
      fixedPoint = selection;
    } else {
      box = BoundingBox<unsigned int>(fixedPoint, selection);

      box[0].z() = 0;
    }
    if (!presentationMode.hasSelection()) {
      presentationMode.toggleSelection();
      presentationMode.setRedraw(false);
    }
  }
}

void Selection::release() {
  draggingX    = false;
  draggingLeft = false;
  draggingY    = false;
  draggingBot  = false;
  draggingZ    = false;
  draggingBack = false;
  dragging     = false;
}

bool Selection::isBackEdgeOnTopPlane(unsigned int x, unsigned int y) {
  auto a         = box[0];
  auto b         = box[1] + uvec3(1, 1, 0);
  auto edgeStart = coordSys.transformCoord(uvec3(a.x(), controlPoint.y() + 1, a.z()));
  auto edgeEnd   = coordSys.transformCoord(uvec3(b.x(), controlPoint.y() + 1, a.z()));

  return y < edgeStart.y() + 5 && x > edgeStart.x() && x < edgeEnd.x() && presentationMode.hasSelection();
}

bool Selection::isFrontEdgeOnTopPlane(unsigned int x, unsigned int y) {
  auto a         = box[0];
  auto b         = box[1] + uvec3(1, 1, 1);
  auto edgeStart = coordSys.transformCoord(uvec3(a.x(), controlPoint.y() + 1, b.z() - 1));
  auto edgeEnd   = coordSys.transformCoord(uvec3(b.x(), controlPoint.y() + 1, b.z() - 1));

  return y < edgeStart.y() + 10 && y > edgeStart.y() - 10 && x > edgeStart.x() && x < edgeEnd.x() && presentationMode.hasSelection();
}

bool Selection::isRightEdgeOnTopPlane(unsigned int x, unsigned int y) {
  return isEdgeOnSidePlane(x, y, 0, 1);
}

bool Selection::isLeftEdgeOnTopPlane(unsigned int x, unsigned int y) {
  return isEdgeOnSidePlane(x, y, 0, 0);
}

bool Selection::isBackEdgeOnSidePlane(unsigned int x, unsigned int y) {
  auto a         = box[0];
  auto b         = box[1] + uvec3(1, 1, 0);
  auto edgeStart = coordSys.transformCoord(uvec3(controlPoint.x() + 1, a.y(), a.z()));
  auto edgeEnd   = coordSys.transformCoord(uvec3(controlPoint.x() + 1, b.y(), a.z()));

  return x < edgeStart.x() + 10 && x > edgeStart.x() - 10 && y < edgeStart.y() && y > edgeEnd.y() && presentationMode.hasSelection();
}

bool Selection::isFrontEdgeOnSidePlane(unsigned int x, unsigned int y) {
  auto a = box[0];
  auto b = box[1] + uvec3(1, 1, 0);

  auto edgeStart = coordSys.transformCoord(uvec3(controlPoint.x() + 1, a.y(), b.z()));
  auto edgeEnd   = coordSys.transformCoord(uvec3(controlPoint.x() + 1, b.y(), b.z()));

  return x < edgeStart.x() + 10 && x > edgeStart.x() - 10 && y < edgeStart.y() && y > edgeEnd.y() && presentationMode.hasSelection();
}

bool Selection::isTopEdgeOnSidePlane(unsigned int x, unsigned int y) {
  return isEdgeOnSidePlane(x, y, 1, 1);
}

bool Selection::isBotEdgeOnSidePlane(unsigned int x, unsigned int y) {
  return isEdgeOnSidePlane(x, y, 1, 0);
}

bool Selection::isTopEdgeOnFrontPlane(unsigned int x, unsigned int y) {
  return isEdgeOnFrontPlane(x, y, 0, 1);
}
bool Selection::isBotEdgeOnFrontPlane(unsigned int x, unsigned int y) {
  return isEdgeOnFrontPlane(x, y, 0, 0);
}

bool Selection::isLeftEdgeOnFrontPlane(unsigned int x, unsigned int y) {
  return isEdgeOnFrontPlane(x, y, 1, 0);
}
bool Selection::isRightEdgeOnFrontPlane(unsigned int x, unsigned int y) {
  return isEdgeOnFrontPlane(x, y, 1, 1);
}
/**
 * checks whether the coordinates are on the edge of the selection on one of the side plane
 *
 * @param x     x-coordinate
 * @param y     y-coordinate
 * @param axis  0 for the top plane and 1 for the right plane
 * @param line  0 for min and 1 for max coordinates on the other axis
 *
 * @return true when coordinates are on edge
 */
bool Selection::isEdgeOnSidePlane(unsigned int x, unsigned int y, int axis, int line) {
  BoundingBox<unsigned int> boundingbox(box[0], box[1]);
  boundingbox[1] += uvec3(1, 1, 0);
  uvec3 v;
  v[axis]        = boundingbox[line][axis];
  v[1 - axis]    = controlPoint[1 - axis] + 1;
  vec2 edgeStart = coordSys.transformCoord(uvec3(v[0], v[1], boundingbox[1].z()));
  vec2 edgeEnd   = coordSys.transformCoord(uvec3(v[0], v[1], (boundingbox[1].z() == boundingbox[0].z()) ? 0 : boundingbox[0].z()));
  float slope    = (edgeEnd.y() - edgeStart.y()) / (edgeEnd.x() - edgeStart.x());
  float bias     = edgeStart.y() - (slope * edgeStart.x());
  float tmp      = round(slope * (float)x + bias);

  return y > tmp - 10 && y < tmp + 10 && x > edgeStart.x() && x < edgeEnd.x();
}

/**
 * checks whether the coordinates are on the edge of the selection on the front plane
 *
 * @param x     x-coordinate
 * @param y     y-coordinate
 * @param axis  the axis that is parallel to the edge
 * @param line  0 for min and 1 for max coordinates on the other axis
 *
 * @return true when coordinates are on edge
 */
bool Selection::isEdgeOnFrontPlane(unsigned int x, unsigned int y, int axis, int line) {
  uvec3 a = box[0];
  uvec3 b = box[1];
  BoundingBox<unsigned int> boundingbox(a, b);
  boundingbox[1] += uvec3(1, 1, 0);
  uvec3 v1, v2;
  v1[axis]       = boundingbox[0][axis];
  v1[1 - axis]   = boundingbox[line][1 - axis];
  v2[axis]       = boundingbox[1][axis];
  v2[1 - axis]   = boundingbox[line][1 - axis];
  auto edgeStart = coordSys.transformCoord(uvec3(v1.x(), v1.y(), controlPoint.z() + 1));
  auto edgeEnd   = coordSys.transformCoord(uvec3(v2.x(), v2.y(), controlPoint.z() + 1));

  auto oppositeEdge = coordSys.transformCoord(uvec3(boundingbox[1 - line].x(), boundingbox[1 - line].y(), controlPoint.z()));

  if (axis == 1) {
    bool tmp = abs((int)oppositeEdge.x() - (int)x) - 2 > abs((int)edgeStart.x() - (int)x);

    return x < edgeEnd.x() + 10 && x > edgeStart.x() - 10 && y < edgeStart.y() + 10 &&
           y > edgeEnd.y() - 10 && tmp && presentationMode.hasSelection();
  } else {
    bool tmp = abs((int)oppositeEdge.y() - (int)y) - 2 > abs((int)edgeStart.y() - (int)y);
    return y < edgeEnd.y() + 10 && y > edgeStart.y() - 10 && x > edgeStart.x() - 10 &&
           x < edgeEnd.x() + 10 && tmp && presentationMode.hasSelection();
  }
}
