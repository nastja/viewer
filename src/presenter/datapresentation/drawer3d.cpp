#include "drawer3d.h"
#include "util/math.h"
#include <FL/fl_draw.H>

/**
 * renders the normal mode
 *
 * @param controlPoint  controlPoint position
 */
void Drawer3D::renderNormal(uvec3 controlPoint) {
  drawGridedFrontPlane(uvec3(0, 0, controlPoint.z()), controlPoint);
  drawGridedTopPlane(uvec3(0, controlPoint.y(), 0), controlPoint);
  drawGridedSidePlane(uvec3(controlPoint.x(), 0, 0), controlPoint);

  // draw edge from missing block
  char dash[] = {5, 5, 0};
  fl_color(getColor(Colors::border));
  fl_line_style(FL_SOLID, 0, dash);
  auto anchor = view.getDataSize() - uvec3(1, 1, 1);
  drawEdges(uvec3(0, 0, 0), anchor);
  auto d = coordSys.transformCoord(uvec3(controlPoint.x() + 1, 0, 0));
  auto c = coordSys.transformCoord(uvec3(anchor.x() + 1, 0, 0));
  fl_line(c.x(), c.y(), d.x(), d.y());
  d = coordSys.transformCoord(uvec3(0, 0, controlPoint.z() + 1));
  c = coordSys.transformCoord(uvec3(0, 0, anchor.z() + 1));
  fl_line(c.x(), c.y(), d.x(), d.y());
  fl_line(c.x(), c.y(), d.x(), d.y());
  d = coordSys.transformCoord(uvec3(0, controlPoint.y() + 1, 0));
  c = coordSys.transformCoord(uvec3(0, anchor.y() + 1, 0));
  fl_line(c.x(), c.y(), d.x(), d.y());
  fl_line_style(0);

  drawEdges(uvec3(0, 0, 0), controlPoint);
}

/**
 * renders the inverted mode
 *
 * @param controlPoint  controlPoint position
 */
void Drawer3D::renderInverted(uvec3 controlPoint) {
  auto anchor = view.getDataSize() - uvec3(1, 1, 1);

  // the instructions' order matters

  // front
  // right block
  drawGridedFrontPlane(uvec3(controlPoint.x(), 0, anchor.z()), uvec3(anchor.x(), controlPoint.y() - 1, anchor.z()));
  // left block
  drawGridedFrontPlane(uvec3(0, 0, anchor.z()), uvec3(controlPoint.x() - 1, anchor.y(), anchor.z()));

  // top
  // front block
  drawGridedTopPlane(uvec3(0, anchor.y(), controlPoint.z()), uvec3(controlPoint.x() - 1, anchor.y(), anchor.z()));
  // back block
  drawGridedTopPlane(uvec3(0, anchor.y(), 0), uvec3(anchor.x(), anchor.y(), controlPoint.z() - 1));

  // side
  // top block
  drawGridedSidePlane(uvec3(anchor.x(), controlPoint.y(), 0), uvec3(anchor.x(), anchor.y(), controlPoint.z() - 1));
  // bot block
  drawGridedSidePlane(uvec3(anchor.x(), 0, 0), uvec3(anchor.x(), controlPoint.y() - 1, anchor.z()));
  // inside block
  drawGridedSidePlane(controlPoint - uvec3(1, 0, 0), uvec3(controlPoint.x() - 1, anchor.y(), anchor.z()));

  // front
  // inside block
  drawGridedFrontPlane(controlPoint - uvec3(0, 0, 1), uvec3(anchor.x(), anchor.y(), controlPoint.z() - 1));

  // top
  // inside block
  drawGridedTopPlane(controlPoint - uvec3(0, 1, 0), uvec3(anchor.x(), controlPoint.y() - 1, anchor.z()));

  drawInvertedModeEdges(controlPoint);
}

/**
 * draws the visible edges of the block from a to b
 */
void Drawer3D::drawEdges(uvec3 a, uvec3 b) {
  auto c = coordSys.transformCoord(uvec3(a.x(), b.y() + 1, b.z() + 1));
  auto d = coordSys.transformCoord(uvec3(b.x() + 1, b.y() + 1, b.z() + 1));
  fl_line(c.x(), c.y(), d.x(), d.y());
  c = coordSys.transformCoord(uvec3(a.x(), a.y(), b.z() + 1));
  d = coordSys.transformCoord(uvec3(b.x() + 1, a.y(), b.z() + 1));
  fl_line(c.x(), c.y(), d.x(), d.y());
  c = coordSys.transformCoord(uvec3(a.x(), a.y(), b.z() + 1));
  d = coordSys.transformCoord(uvec3(a.x(), b.y() + 1, b.z() + 1));
  fl_line(c.x(), c.y(), d.x(), d.y());
  c = coordSys.transformCoord(uvec3(b.x() + 1, a.y(), b.z() + 1));
  d = coordSys.transformCoord(uvec3(b.x() + 1, b.y() + 1, b.z() + 1));
  fl_line(c.x(), c.y(), d.x(), d.y());
  c = coordSys.transformCoord(uvec3(b.x() + 1, b.y() + 1, a.z()));
  d = coordSys.transformCoord(uvec3(b.x() + 1, b.y() + 1, b.z() + 1));
  fl_line(c.x(), c.y(), d.x(), d.y());
  c = coordSys.transformCoord(uvec3(b.x() + 1, a.y(), a.z()));
  d = coordSys.transformCoord(uvec3(b.x() + 1, a.y(), b.z() + 1));
  fl_line(c.x(), c.y(), d.x(), d.y());
  c = coordSys.transformCoord(uvec3(a.x(), b.y() + 1, a.z()));
  d = coordSys.transformCoord(uvec3(b.x() + 1, b.y() + 1, a.z()));
  fl_line(c.x(), c.y(), d.x(), d.y());
  c = coordSys.transformCoord(uvec3(a.x(), b.y() + 1, a.z()));
  d = coordSys.transformCoord(uvec3(a.x(), b.y() + 1, b.z() + 1));
  fl_line(c.x(), c.y(), d.x(), d.y());
  c = coordSys.transformCoord(uvec3(b.x() + 1, a.y(), a.z()));
  d = coordSys.transformCoord(uvec3(b.x() + 1, b.y() + 1, a.z()));
  fl_line(c.x(), c.y(), d.x(), d.y());
}

void Drawer3D::drawInvertedModeEdges(uvec3 controlPoint) {
  uvec3 max = view.getDataSize();

  fl_color(getColor(Colors::border));

  // top plane's edges
  auto a = coordSys.transformCoord(uvec3(0, max.y(), 0));
  auto b = coordSys.transformCoord(uvec3(0, max.y(), max.z()));
  fl_line(a.x(), a.y(), b.x(), b.y());
  a = coordSys.transformCoord(uvec3(0, max.y(), max.z()));
  b = coordSys.transformCoord(uvec3(controlPoint.x(), max.y(), max.z()));
  fl_line(a.x(), a.y(), b.x(), b.y());
  a = coordSys.transformCoord(uvec3(controlPoint.x(), max.y(), max.z()));
  b = coordSys.transformCoord(uvec3(controlPoint.x(), max.y(), controlPoint.z()));
  fl_line(a.x(), a.y(), b.x(), b.y());
  a = coordSys.transformCoord(uvec3(controlPoint.x(), max.y(), controlPoint.z()));
  b = coordSys.transformCoord(uvec3(max.x(), max.y(), controlPoint.z()));
  fl_line(a.x(), a.y(), b.x(), b.y());
  a = coordSys.transformCoord(uvec3(max.x(), max.y(), controlPoint.z()));
  b = coordSys.transformCoord(uvec3(max.x(), max.y(), 0));
  fl_line(a.x(), a.y(), b.x(), b.y());
  a = coordSys.transformCoord(uvec3(max.x(), max.y(), 0));
  b = coordSys.transformCoord(uvec3(0, max.y(), 0));
  fl_line(a.x(), a.y(), b.x(), b.y());

  // front plane's edges
  a = coordSys.transformCoord(uvec3(0, 0, max.z()));
  b = coordSys.transformCoord(uvec3(max.x(), 0, max.z()));
  fl_line(a.x(), a.y(), b.x(), b.y());
  a = coordSys.transformCoord(uvec3(max.x(), 0, max.z()));
  b = coordSys.transformCoord(uvec3(max.x(), controlPoint.y(), max.z()));
  fl_line(a.x(), a.y(), b.x(), b.y());
  a = coordSys.transformCoord(uvec3(max.x(), controlPoint.y(), max.z()));
  b = coordSys.transformCoord(uvec3(controlPoint.x(), controlPoint.y(), max.z()));
  fl_line(a.x(), a.y(), b.x(), b.y());
  a = coordSys.transformCoord(uvec3(controlPoint.x(), controlPoint.y(), max.z()));
  b = coordSys.transformCoord(uvec3(controlPoint.x(), max.y(), max.z()));
  fl_line(a.x(), a.y(), b.x(), b.y());
  a = coordSys.transformCoord(uvec3(controlPoint.x(), max.y(), max.z()));
  b = coordSys.transformCoord(uvec3(0, max.y(), max.z()));
  fl_line(a.x(), a.y(), b.x(), b.y());
  a = coordSys.transformCoord(uvec3(0, max.y(), max.z()));
  b = coordSys.transformCoord(uvec3(0, 0, max.z()));
  fl_line(a.x(), a.y(), b.x(), b.y());

  // side plane's edges
  a = coordSys.transformCoord(uvec3(max.x(), 0, 0));
  b = coordSys.transformCoord(uvec3(max.x(), max.y(), 0));
  fl_line(a.x(), a.y(), b.x(), b.y());
  a = coordSys.transformCoord(uvec3(max.x(), max.y(), 0));
  b = coordSys.transformCoord(uvec3(max.x(), max.y(), controlPoint.z()));
  fl_line(a.x(), a.y(), b.x(), b.y());
  a = coordSys.transformCoord(uvec3(max.x(), max.y(), controlPoint.z()));
  b = coordSys.transformCoord(uvec3(max.x(), controlPoint.y(), controlPoint.z()));
  fl_line(a.x(), a.y(), b.x(), b.y());
  a = coordSys.transformCoord(uvec3(max.x(), controlPoint.y(), controlPoint.z()));
  b = coordSys.transformCoord(uvec3(max.x(), controlPoint.y(), max.z()));
  fl_line(a.x(), a.y(), b.x(), b.y());
  a = coordSys.transformCoord(uvec3(max.x(), controlPoint.y(), max.z()));
  b = coordSys.transformCoord(uvec3(max.x(), 0, max.z()));
  fl_line(a.x(), a.y(), b.x(), b.y());
  a = coordSys.transformCoord(uvec3(max.x(), 0, max.z()));
  b = coordSys.transformCoord(uvec3(max.x(), 0, 0));
  fl_line(a.x(), a.y(), b.x(), b.y());

  // draw inside edges
  a = coordSys.transformCoord(controlPoint);
  b = coordSys.transformCoord(uvec3(controlPoint.x(), controlPoint.y(), max.z()));
  fl_line(a.x(), a.y(), b.x(), b.y());
  b = coordSys.transformCoord(uvec3(controlPoint.x(), max.y(), controlPoint.z()));
  fl_line(a.x(), a.y(), b.x(), b.y());
  b = coordSys.transformCoord(uvec3(max.x(), controlPoint.y(), controlPoint.z()));
  fl_line(a.x(), a.y(), b.x(), b.y());
}

/**
 * draws the frontal plane with grid
 *
 * @param a     start data coordinate
 * @param b     end  data coordinate
 */
void Drawer3D::drawGridedFrontPlane(uvec3 a, uvec3 b) {
  drawFrontPlane(a, b);
}

/**
 * draws the right plane with grid
 *
 * @param a     start data coordinate
 * @param b     end  data coordinate
 */
void Drawer3D::drawGridedSidePlane(uvec3 a, uvec3 b) {
  drawSidePlane(a, b);
}

/**
 * draws the top plane with grid
 *
 * @param a     start data coordinate
 * @param b     end  data coordinate
 */
void Drawer3D::drawGridedTopPlane(uvec3 a, uvec3 b) {
  drawTopPlane(a, b);
}

/**
 * draws the frontal plane without grid
 *
 * @param a     start data coordinate
 * @param b     end  data coordinate
 */
void Drawer3D::drawFrontPlane(uvec3 a, uvec3 b) {
  auto blockDimensions = view.getBlockDimensions();
  auto blockNum        = calculateBlockNum(a, b);

  std::vector<bool> dontDraw;
  dontDraw.resize(blockNum.x() * blockNum.y());

  uvec3 pos = a;
  uvec3 rectSize(0, blockDimensions.y() - (pos.y() + std::get<0>(rotatedBorder).y() - std::get<1>(rotatedBorder).y()) % blockDimensions.y() - 1, 0);
  for (unsigned int i = 0; i < blockNum.y(); ++i) {
    rectSize.x() = blockDimensions.x() - (pos.x() + std::get<0>(rotatedBorder).x() - std::get<1>(rotatedBorder).x()) % blockDimensions.x() - 1;

    if (pos.y() + rectSize.y() > b.y()) {
      rectSize.y() = b.y() - pos.y();
    }

    for (unsigned int j = 0; j < blockNum.x(); ++j) {
      if (pos.x() + rectSize.x() > b.x()) {
        rectSize.x() = b.x() - pos.x();
      }
      if (!dontDraw[i * blockNum.x() + j]) {
        // increase by one so that temp is another rectangle for id check
        auto temp = pos + rectSize + uvec3(1, 1, 0);

        int width  = 0;
        int height = 0;

        std::tie(temp, width) = calculateBlockSize(0, pos, temp, b, blockDimensions, blockNum.x() - j);

        std::tie(temp, height) = calculateBlockSize(1, pos, temp, b, blockDimensions, blockNum.y() - i);

        if (temp.x() > b.x()) temp.x() = b.x();
        if (temp.y() > b.y()) temp.y() = b.y();

        for (int k = 0; k <= height; k++) {
          for (int l = 0; l <= width; l++) {
            dontDraw[(i + k) * blockNum.x() + j + l] = true;
          }
        }

        drawFrontBlockPlane(pos, temp);

        if (isShowBlockLine()) {
          auto c = coordSys.transformCoord(pos + uvec3(0, 0, 1));
          temp += uvec3(1, 1, 1);
          if (temp.x() > b.x()) temp.x() = b.x() + 1;
          if (temp.y() > b.y()) temp.y() = b.y() + 1;
          auto d = coordSys.transformCoord(temp);
          fl_color(FL_BLACK);
          fl_line(c.x(), c.y(), c.x(), d.y());
          fl_line(c.x(), c.y(), d.x(), c.y());
        }
      }
      pos.x() += rectSize.x() + 1;
      rectSize.x() = blockDimensions.x() - 1;
    }

    pos.x() = a.x();
    pos.y() += rectSize.y() + 1;
    rectSize.y() = blockDimensions.y() - 1;
  }
}

void Drawer3D::drawFrontBlockPlane(uvec3 a, uvec3 b) {
  auto colors = view.getSubarrayColors(a, b);
  auto iter   = colors.begin();
  for (unsigned int y = a.y(); y <= b.y(); ++y) {
    for (unsigned int x = a.x(); x <= b.x(); ++x) {
      uvec3 pos{x, y, a.z() + 1};
      drawPixelFront(pos, *iter);
      iter++;
    }
  }
  drawFrontGrid(a, b);
}

/**
 * draws the top plane without grid
 *
 * @param a start data coordinate
 * @param b end  data coordinate
 */
void Drawer3D::drawTopPlane(uvec3 a, uvec3 b) {
  auto blockDimensions = view.getBlockDimensions();
  auto blockNum        = calculateBlockNum(a, b);

  std::vector<bool> dontDraw;
  dontDraw.resize(blockNum.x() * blockNum.z());

  uvec3 pos = a;
  uvec3 rectSize(0, 0, blockDimensions.z() - (pos.z() + std::get<0>(rotatedBorder).z() - std::get<1>(rotatedBorder).z()) % blockDimensions.z() - 1);
  for (unsigned int i = 0; i < blockNum.z(); ++i) {
    rectSize.x() = blockDimensions.x() - (pos.x() + std::get<0>(rotatedBorder).x() - std::get<1>(rotatedBorder).x()) % blockDimensions.x() - 1;

    if (pos.z() + rectSize.z() > b.z()) {
      rectSize.z() = b.z() - pos.z();
    }

    for (unsigned int j = 0; j < blockNum.x(); ++j) {
      if (!dontDraw[i * blockNum.x() + j]) {
        // increase by one so that temp is another rectangle for id check
        auto temp = pos + rectSize + uvec3(1, 0, 1);

        int width  = 0;
        int height = 0;

        std::tie(temp, width) = calculateBlockSize(0, pos, temp, b, blockDimensions, blockNum.x() - j);

        std::tie(temp, height) = calculateBlockSize(2, pos, temp, b, blockDimensions, blockNum.z() - i);

        if (temp.x() > b.x()) temp.x() = b.x();
        if (temp.z() > b.z()) temp.z() = b.z();

        for (int k = 0; k <= height; k++) {
          for (int l = 0; l <= width; l++) {
            dontDraw[(i + k) * blockNum.x() + j + l] = true;
          }
        }

        drawTopBlockPlane(pos, temp);

        if (isShowBlockLine()) {
          auto c = coordSys.transformCoord(pos + uvec3(0, 1, 0));
          temp += uvec3(1, 0, 1);
          if (temp.x() > b.x()) temp.x() = b.x() + 1;
          if (temp.z() > b.z()) temp.z() = b.z() + 1;
          auto d = coordSys.transformCoord(uvec3(temp.x(), pos.y() + 1, pos.z()));
          fl_color(FL_BLACK);
          //horizontal lines
          fl_line(c.x(), c.y(), d.x(), c.y());
          fl_line(c.x(), d.y(), d.x(), d.y());
          //vertical lines
          d = coordSys.transformCoord(uvec3(pos.x(), pos.y() + 1, temp.z()));
          fl_line(c.x(), c.y(), d.x(), d.y());
          c = coordSys.transformCoord(uvec3(temp.x(), temp.y() + 1, pos.z()));
          d = coordSys.transformCoord(uvec3(temp.x(), temp.y() + 1, temp.z()));
          fl_line(c.x(), c.y(), d.x(), d.y());
        }
      }

      pos.x() += rectSize.x() + 1;
      rectSize.x() = blockDimensions.x() - 1;
    }

    pos.x() = a.x();
    pos.z() += rectSize.z() + 1;
    rectSize.z() = blockDimensions.z() - 1;
  }
}

void Drawer3D::drawTopBlockPlane(uvec3 a, uvec3 b) {
  auto colors = view.getSubarrayColors(a, b);
  auto iter   = colors.begin();
  for (unsigned int z = a.z(); z <= b.z(); ++z) {
    for (unsigned int x = a.x(); x <= b.x(); ++x) {
      uvec3 pos{x, a.y() + 1, z};
      drawPixelTop(pos, *iter);
      iter++;
    }
  }
  drawTopGrid(a, b);
}

/**
 * draws the right plane without grid
 *
 * @param a start data coordinate
 * @param b end  data coordinate
 */
void Drawer3D::drawSidePlane(uvec3 a, uvec3 b) {
  auto blockDimensions = view.getBlockDimensions();
  auto blockNum        = calculateBlockNum(a, b);

  std::vector<bool> dontDraw;
  dontDraw.resize(blockNum.y() * blockNum.z());

  uvec3 pos = a;
  uvec3 rectSize(0, 0, blockDimensions.z() - (pos.z() + std::get<0>(rotatedBorder).z() - std::get<1>(rotatedBorder).z()) % blockDimensions.z() - 1);
  for (unsigned int i = 0; i < blockNum.z(); ++i) {
    rectSize.y() = blockDimensions.y() - (pos.y() + std::get<0>(rotatedBorder).y() - std::get<1>(rotatedBorder).y()) % blockDimensions.y() - 1;

    if (pos.z() + rectSize.z() > b.z()) {
      rectSize.z() = b.z() - pos.z();
    }

    for (unsigned int j = 0; j < blockNum.y(); ++j) {
      if (!dontDraw[i * blockNum.y() + j]) {
        // increase by one so that temp is another rectangle for id check
        auto temp = pos + rectSize + uvec3(0, 1, 1);

        int width  = 0;
        int height = 0;

        std::tie(temp, height) = calculateBlockSize(1, pos, temp, b, blockDimensions, blockNum.y() - j);

        std::tie(temp, width) = calculateBlockSize(2, pos, temp, b, blockDimensions, blockNum.z() - i);

        if (temp.y() > b.y()) temp.y() = b.y();
        if (temp.z() > b.z()) temp.z() = b.z();

        for (int k = 0; k <= width; k++) {
          for (int l = 0; l <= height; l++) {
            dontDraw[(i + k) * blockNum.y() + j + l] = true;
          }
        }

        drawSideBlockPlane(pos, temp);

        if (isShowBlockLine()) {
          auto c = coordSys.transformCoord(pos + uvec3(1, 0, 0));
          temp += uvec3(0, 1, 1);
          if (temp.y() > b.y()) temp.y() = b.y() + 1;
          if (temp.z() > b.z()) temp.z() = b.z() + 1;
          auto d = coordSys.transformCoord(uvec3(pos.x() + 1, temp.y(), pos.z()));
          //vertical lines
          fl_color(FL_BLACK);
          fl_line(c.x(), c.y(), c.x(), d.y());
          fl_line(d.x(), c.y(), d.x(), d.y());
          //horizontal lines
          d = coordSys.transformCoord(uvec3(temp.x() + 1, pos.y(), temp.z()));
          fl_line(c.x(), c.y(), d.x(), d.y());
          c = coordSys.transformCoord(uvec3(temp.x() + 1, temp.y(), pos.z()));
          d = coordSys.transformCoord(uvec3(temp.x() + 1, temp.y(), temp.z()));
          fl_line(c.x(), c.y(), d.x(), d.y());
        }
      }
      pos.y() += rectSize.y() + 1;
      rectSize.y() = blockDimensions.y() - 1;
    }

    pos.y() = a.y();
    pos.z() += rectSize.z() + 1;
    rectSize.z() = blockDimensions.z() - 1;
  }
}

void Drawer3D::drawSideBlockPlane(uvec3 a, uvec3 b) {
  auto colors = view.getSubarrayColors(a, b);
  auto iter   = colors.begin();
  for (unsigned int z = a.z(); z <= b.z(); ++z) {
    for (unsigned int y = a.y(); y <= b.y(); ++y) {
      uvec3 pos{a.x() + 1, y, z};
      drawPixelSide(pos, *iter);
      iter++;
    }
  }
  drawSideGrid(a, b);
}

/**
 * draws a horizontal line on the frontal plane
 *
 * @param a  start data coordinate
 * @param b  end  data coordinate
 */
void Drawer3D::drawFrontHorizontalLines(uvec3 a, uvec3 b) {
  if (getGridState() == Grid::on) {
    fl_color(getColor(Colors::grid));
    for (unsigned int i = a.y(); i <= b.y(); ++i) {
      auto c = coordSys.transformCoord(uvec3(a.x(), i, a.z() + 1));
      auto d = coordSys.transformCoord(uvec3(b.x() + 1, i, b.z() + 1));
      fl_line(c.x(), c.y(), d.x(), d.y());
      auto level = view.getLevel(uvec3(a.x(), i, a.z()));
      i += pow2(level) - 1;
    }
  } else if (getGridState() == Grid::gradient) {
    fl_color(getColor(Colors::grid));
    for (unsigned int y = a.y() - (a.y() > 0); y < b.y(); ++y) {
      for (unsigned int x = a.x(); x <= b.x(); ++x) {
        if (view.isScreenSpaceGradientY(uvec3(x, y, a.z()))) {
          auto c = coordSys.transformCoord(uvec3(x, y + 1, a.z() + 1));
          auto d = coordSys.transformCoord(uvec3(x + 1, y + 1, b.z() + 1));
          fl_line(c.x(), c.y(), d.x(), d.y());
        }
      }
    }
  }
}

/**
 * draws a vertical line on the right plane
 *
 * @param a  start data coordinate
 * @param b  end  data coordinate
 */
void Drawer3D::drawFrontVerticalLines(uvec3 a, uvec3 b) {
  if (getGridState() == Grid::on) {
    fl_color(getColor(Colors::grid));
    for (unsigned int i = a.x(); i <= b.x(); ++i) {
      auto c = coordSys.transformCoord(uvec3(i, b.y() + 1, b.z() + 1));
      auto d = coordSys.transformCoord(uvec3(i, a.y(), a.z() + 1));
      fl_line(c.x(), c.y(), d.x(), d.y());
      auto level = view.getLevel(uvec3(i, b.y(), b.z()));
      i += pow2(level) - 1;
    }
  } else if (getGridState() == Grid::gradient) {
    fl_color(getColor(Colors::grid));
    for (unsigned int y = a.y(); y <= b.y(); ++y) {
      for (unsigned int x = a.x() - (a.x() > 0); x < b.x(); ++x) {
        if (view.isScreenSpaceGradientX(uvec3(x, y, a.z()))) {
          auto c = coordSys.transformCoord(uvec3(x + 1, y + 1, a.z() + 1));
          auto d = coordSys.transformCoord(uvec3(x + 1, y, b.z() + 1));
          fl_line(c.x(), c.y(), d.x(), d.y());
        }
      }
    }
  }
}

/**
 * draws a horizontal line on the top plane
 *
 * @param a  start data coordinate
 * @param b  end  data coordinate
 */
void Drawer3D::drawTopHorizontalLines(uvec3 a, uvec3 b) {
  if (getGridState() == Grid::on) {
    fl_color(getColor(Colors::grid));
    for (unsigned int i = a.z(); i <= b.z(); ++i) {
      auto c = coordSys.transformCoord(uvec3(a.x(), a.y() + 1, i));
      auto d = coordSys.transformCoord(uvec3(b.x() + 1, b.y() + 1, i));
      fl_line(c.x(), c.y(), d.x(), d.y());
      auto level = view.getLevel(uvec3(a.x(), a.y(), i));
      i += pow2(level) - 1;
    }
  } else if (getGridState() == Grid::gradient) {
    fl_color(getColor(Colors::grid));
    for (unsigned int z = a.z() - (a.z() > 0); z < b.z(); ++z) {
      for (unsigned int x = a.x(); x <= b.x(); ++x) {
        if (view.isScreenSpaceGradientZ(uvec3(x, a.y(), z))) {
          auto c = coordSys.transformCoord(uvec3(x, a.y() + 1, z + 1));
          auto d = coordSys.transformCoord(uvec3(x + 1, a.y() + 1, z + 1));
          fl_line(c.x(), c.y(), d.x(), d.y());
        }
      }
    }
  }
}

/**
 * draws a vertical line on the top plane
 *
 * @param a  start data coordinate
 * @param b  end  data coordinate
 */
void Drawer3D::drawTopVerticalLines(uvec3 a, uvec3 b) {
  auto y = a.y();
  if (getGridState() == Grid::on) {
    fl_color(getColor(Colors::grid));
    for (unsigned int x = a.x(); x <= b.x(); ++x) {
      auto c = coordSys.transformCoord(uvec3(x, y + 1, a.z()));
      auto d = coordSys.transformCoord(uvec3(x, y + 1, b.z() + 1));
      fl_line(c.x(), c.y(), d.x(), d.y());
      auto level = view.getLevel(uvec3(x, y, a.z()));
      x += pow2(level) - 1;
    }
  } else if (getGridState() == Grid::gradient) {
    fl_color(getColor(Colors::grid));
    for (unsigned int z = a.z(); z <= b.z(); ++z) {
      for (unsigned int x = a.x() - (a.x() > 0); x < b.x(); ++x) {
        if (view.isScreenSpaceGradientX(uvec3(x, a.y(), z))) {
          auto c = coordSys.transformCoord(uvec3(x + 1, a.y() + 1, z));
          auto d = coordSys.transformCoord(uvec3(x + 1, a.y() + 1, z + 1));
          fl_line(c.x(), c.y(), d.x(), d.y());
        }
      }
    }
  }
}

/**
 * draws a horinzontal line on the right plane
 *
 * @param a  start data coordinate
 * @param b  end  data coordinate
 */
void Drawer3D::drawSideHorizontalLines(uvec3 a, uvec3 b) {
  if (getGridState() == Grid::on) {
    fl_color(getColor(Colors::grid));
    for (unsigned int i = a.y(); i <= b.y(); ++i) {
      auto c = coordSys.transformCoord(uvec3(a.x() + 1, i, a.z()));
      auto d = coordSys.transformCoord(uvec3(b.x() + 1, i, b.z() + 1));
      fl_line(c.x(), c.y(), d.x(), d.y());
      auto level = view.getLevel(uvec3(a.x(), i, a.z()));
      i += pow2(level) - 1;
    }
  } else if (getGridState() == Grid::gradient) {
    fl_color(getColor(Colors::grid));
    for (unsigned int z = a.z(); z <= b.z(); ++z) {
      for (unsigned int y = a.y() - (a.y() > 0); y < b.y(); ++y) {
        if (view.isScreenSpaceGradientY(uvec3(a.x(), y, z))) {
          auto c = coordSys.transformCoord(uvec3(a.x() + 1, y + 1, z));
          auto d = coordSys.transformCoord(uvec3(a.x() + 1, y + 1, z + 1));
          fl_line(c.x(), c.y(), d.x(), d.y());
        }
      }
    }
  }
}

/**
 * draws a vertical line on right plane
 *
 * @param a  start data coordinate
 * @param b  end  data coordinate
 */
void Drawer3D::drawSideVerticalLines(uvec3 a, uvec3 b) {
  if (getGridState() == Grid::on) {
    fl_color(getColor(Colors::grid));
    for (unsigned int i = a.z(); i <= b.z(); ++i) {
      auto d = coordSys.transformCoord(uvec3(a.x() + 1, a.y(), i));
      auto c = coordSys.transformCoord(uvec3(b.x() + 1, b.y() + 1, i));
      fl_line(c.x(), c.y(), d.x(), d.y());
      auto level = view.getLevel(uvec3(a.x(), a.y(), i));
      i += pow2(level) - 1;
    }
  } else if (getGridState() == Grid::gradient) {
    fl_color(getColor(Colors::grid));
    for (unsigned int z = a.z() - (a.z() > 0); z < b.z(); ++z) {
      for (unsigned int y = a.y(); y <= b.y(); ++y) {
        if (view.isScreenSpaceGradientZ(uvec3(a.x(), y, z))) {
          auto c = coordSys.transformCoord(uvec3(a.x() + 1, y, z + 1));
          auto d = coordSys.transformCoord(uvec3(a.x() + 1, y + 1, z + 1));
          fl_line(c.x(), c.y(), d.x(), d.y());
        }
      }
    }
  }
}

/**
 * draws the frontal grid
 *
 * @param a  start data coordinate
 * @param b  end  data coordinate
 */
void Drawer3D::drawFrontGrid(uvec3 a, uvec3 b) {
  drawFrontVerticalLines(a, b);
  drawFrontHorizontalLines(a, b);
}

/**
 * draws the right side grid
 *
 * @param a  start data coordinate
 * @param b  end  data coordinate
 */
void Drawer3D::drawSideGrid(uvec3 a, uvec3 b) {
  drawSideHorizontalLines(a, b);
  drawSideVerticalLines(a, b);
}

/**
 * draws top grid
 *
 * @param a  start data coordinate
 * @param b  end  data coordinate
 */
void Drawer3D::drawTopGrid(uvec3 a, uvec3 b) {
  drawTopVerticalLines(a, b);
  drawTopHorizontalLines(a, b);
}

uvec3 Drawer3D::calculateBlockNum(uvec3 start, uvec3 end) {
  auto blockDimensions = view.getBlockDimensions();
  auto diff            = end - start;

  uvec3 localEnd{static_cast<unsigned int>(end.x() + std::get<0>(rotatedBorder).x() - std::get<1>(rotatedBorder).x()) % blockDimensions.x(),
                 static_cast<unsigned int>(end.y() + std::get<0>(rotatedBorder).y() - std::get<1>(rotatedBorder).y()) % blockDimensions.y(),
                 static_cast<unsigned int>(end.z() + std::get<0>(rotatedBorder).z() - std::get<1>(rotatedBorder).z()) % blockDimensions.z()};

  auto tmp = diff + blockDimensions - uvec3(1, 1, 1) - localEnd;

  return uvec3{tmp.x() / blockDimensions.x() + 1, tmp.y() / blockDimensions.y() + 1, tmp.z() / blockDimensions.z() + 1};
}

/**
 * Draws a pixel.
 *
 * @param upperleft   The upper left corner.
 * @param upperright  The upper right corner.
 * @param lowerright  The lower right corner.
 * @param lowerleft   The lower left corner.
 * @param color       The color.
 */
inline void Drawer3D::drawPixel(uvec2 upperleft, uvec2 upperright, uvec2 lowerright, uvec2 lowerleft, Fl_Color color) {
  fl_color(color);
  fl_polygon(upperleft.x(), upperleft.y(),
             upperright.x(), upperright.y(),
             lowerright.x(), lowerright.y(),
             lowerleft.x(), lowerleft.y());
}

/**
 * Draws a pixel on the front plane.
 *
 * @param coord  The coordinate.
 * @param color  The color.
 */
inline void Drawer3D::drawPixelFront(uvec3 coord, Fl_Color color) {
  auto upperleft  = coordSys.transformCoord(coord + uvec3(0, 1, 0));
  auto lowerright = coordSys.transformCoord(coord + uvec3(1, 0, 0));
  fl_rectf(upperleft.x(), upperleft.y(), lowerright.x() - upperleft.x(), lowerright.y() - upperleft.y(), color);
}

/**
 * Draws a pixel on the top plane.
 *
 * @param coord  The coordinate.
 * @param color  The color.
 */
inline void Drawer3D::drawPixelTop(uvec3 coord, Fl_Color color) {
  auto upperleft  = coordSys.transformCoord(coord);
  auto upperright = coordSys.transformCoord(coord + uvec3(1, 0, 0));
  auto lowerleft  = coordSys.transformCoord(coord + uvec3(0, 0, 1));
  auto lowerright = coordSys.transformCoord(coord + uvec3(1, 0, 1));

#ifdef __APPLE__
  // fix fl_polygon by moving lower and right line
  lowerleft.x()--;
  lowerleft.y()++;
  lowerright.y()++;
  upperright.x()++;
#endif

  drawPixel(upperleft, upperright, lowerright, lowerleft, color);
}

/**
 * Draws a pixel one the side plane.
 *
 * @param coord  The coordinate.
 * @param color  The color.
 */
inline void Drawer3D::drawPixelSide(uvec3 coord, Fl_Color color) {
  auto upperleft  = coordSys.transformCoord(coord + uvec3(0, 1, 1));
  auto upperright = coordSys.transformCoord(coord + uvec3(0, 1, 0));
  auto lowerleft  = coordSys.transformCoord(coord + uvec3(0, 0, 1));
  auto lowerright = coordSys.transformCoord(coord);

#ifdef __APPLE__
  // fix fl_polygon by moving lower and left line
  upperleft.x()--;
  upperleft.y()++;
  lowerleft.x()--;
  lowerleft.y()++;
  lowerright.y()++;
#endif

  drawPixel(upperleft, upperright, lowerright, lowerleft, color);
}

/**
 * calculates the size of the block in cells and the size in the number of the smallest block
 *
 * @param index           index of the axis
 * @param pos             start position
 * @param end             end position
 * @param planeEnd        coordinate of the plane end
 * @param blockSize       the size of the smallest block in coordinates
 * @param maxBlockNumber  number of smallest blocks until plane end
 *
 * @return size in cells and in smallest blocks
 */
std::tuple<uvec3, unsigned long> Drawer3D::calculateBlockSize(int index, uvec3 pos, uvec3 end, uvec3 planeEnd,
                                                              uvec3 blockSize, int maxBlockNumber) {
  int width  = 0;
  int offset = 0;
  auto id    = view.getId(pos);
  auto level = view.getLevel(pos);
  for (int k = 1; k < pow2(level) && k <= maxBlockNumber; k++) {
    if (end[index] > planeEnd[index]) {
      end[index] = planeEnd[index];
      offset     = 1;
      break;
    }
    auto temp   = pos;
    temp[index] = end[index];
    if (view.getId(temp) != id) break;

    width++;
    offset++;
    end[index] += blockSize[index] - 1;
  }
  end[index] += offset - 1;
  return {end, width};
}
