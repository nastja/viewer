/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef NASTJA_VIEWER_VISUALIZATIONCOORDINATESYSTEM_H
#define NASTJA_VIEWER_VISUALIZATIONCOORDINATESYSTEM_H

#include "util/types.h"

class VisualizationCoordinateSystem {
public:
  explicit VisualizationCoordinateSystem(uvec3 controlPoint) : controlPoint(controlPoint) {}

  uvec2 transformCoord(const uvec3& coord);
  uvec3 inverseTransformCoord(const uvec2& coord);

  void setOrigin(ivec2 origin) {
    this->origin = origin;
  }
  void setSizePerCell(vec3 sizePerCell) {
    this->sizePerCell = sizePerCell;
  }

  void setResolution(uvec2 resolution) {
    this->resolution = resolution;
  }

  void setControlPoint(uvec3 controlPoint) {
    this->controlPoint = controlPoint;
  }

private:
  uvec3 controlPoint;
  ivec2 origin;
  vec3 sizePerCell;
  uvec2 resolution;
};

#endif  //NASTJA_VIEWER_VISUALIZATIONCOORDINATESYSTEM_H
