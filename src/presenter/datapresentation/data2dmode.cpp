/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "data2dmode.h"
#include "model/coordinatesystem.h"
#include <FL/Enumerations.H>
#include <FL/fl_draw.H>

Fl_Offscreen Data2DMode::draw() {
  auto view          = model.getDataManager().getRotatedView();
  auto size          = view.getDataSize();
  auto controlPoint  = view.transformToScreenSpace(model.getControlPoint().getControlPoint());
  data_t sizePerCell = std::min(resolution.x() / (controlPoint.x() + 1), resolution.y() / (controlPoint.y() + 1));
  auto img           = fl_create_offscreen(resolution.x(), resolution.y());
  auto offset        = uvec2((resolution.x() - sizePerCell * (controlPoint.x() + 1)) / 2, (resolution.y() - sizePerCell * (controlPoint.y() + 1)) / 2);

  fl_begin_offscreen(img);
  // fill with background color
  fl_rectf(0, 0, resolution.x(), resolution.y(), FL_BACKGROUND_COLOR);

  // draw colored data
  for (unsigned int y = 0; y <= controlPoint.y(); y++) {
    for (unsigned int x = 0; x <= controlPoint.x(); x++) {
      fl_rectf(x * sizePerCell + offset.x(), resolution.y() - (y + 1) * sizePerCell - offset.y(),
               sizePerCell, sizePerCell,
               view.getColor(x, y, controlPoint.z()));
    }
  }

  fl_color(model.getPresentationMode().getColor(Colors::grid));
  // draw grid
  if (model.getPresentationMode().getGridState() == Grid::on) {
    // vertical
    for (unsigned int i = 0; i <= controlPoint.x(); ++i) {
      fl_line(sizePerCell * i + offset.x(), resolution.y() - size.y() * sizePerCell - offset.y(), sizePerCell * i + offset.x(), resolution.y() - 1 - offset.y());
    }
    // horizontal
    for (unsigned int i = 0; i <= controlPoint.y(); ++i) {
      fl_line(offset.x(), resolution.y() - sizePerCell * i - 1 - offset.y(), sizePerCell * size.x() + offset.x(), resolution.y() - sizePerCell * i - 1 - offset.y());
    }
  } else {
    // vertical
    for (unsigned int y = 0; y < controlPoint.y(); ++y) {
      for (unsigned int x = 0; x < controlPoint.x() - 1; ++x) {
        if (view.isScreenSpaceGradientX(uvec3(x, y, controlPoint.z()))) {
          fl_rectf((x + 1) * sizePerCell + offset.x(), resolution.y() - (y + 1) * sizePerCell - offset.y(),
                   1, sizePerCell,
                   FL_BLACK);
        }
      }
    }
    // horizontal
    for (unsigned int y = 0; y < controlPoint.y() - 1; ++y) {
      for (unsigned int x = 0; x < controlPoint.x(); ++x) {
        if (view.isScreenSpaceGradientY(uvec3(x, y, controlPoint.z()))) {
          fl_rectf(x * sizePerCell + offset.x(), resolution.y() - 1 - (y + 1) * sizePerCell - offset.y(),
                   sizePerCell, 1,
                   FL_BLACK);
        }
      }
    }
  }

  fl_color(model.getPresentationMode().getColor(Colors::blockLine));

  auto rotatedBorder = calculateRotatedBorders();

  if (model.getPresentationMode().isShowBlockLine()) {
    auto blockDimensions = view.getBlockDimensions();
    auto pos             = 0;
    auto blockSize       = blockDimensions.x() - (pos + std::get<0>(rotatedBorder).x() - std::get<1>(rotatedBorder).x()) % blockDimensions.x();

    pos += blockSize;
    //vertical
    while (pos <= controlPoint.x()) {
      fl_rectf(pos * sizePerCell + offset.x(), resolution.y() - (controlPoint.y() + 1) * sizePerCell - offset.y(),
               1, sizePerCell * (controlPoint.y() + 1),
               FL_BLACK);
      blockSize = blockDimensions.x();
      pos += blockSize;
    }

    pos = 0;
    blockSize = blockDimensions.y() - (pos + std::get<0>(rotatedBorder).y() - std::get<1>(rotatedBorder).y()) % blockDimensions.y();

    pos += blockSize;
    //horizontal
    while (pos <= controlPoint.y()) {
      fl_rectf(offset.x(), resolution.y() - pos * sizePerCell - offset.y(),
               (controlPoint.x() + 1) * sizePerCell, 1,
               FL_BLACK);
      blockSize = blockDimensions.x();
      pos += blockSize;
    }
  }
  fl_end_offscreen();
  return img;
}

void Data2DMode::rotate(Direction direction) {
  switch (direction) {
    case Direction::backward:
    case Direction::forward:
      model.getCoordinateSystem().rotate(direction);
      break;
    default:
      break;
  }
}

void Data2DMode::zoomIn() {
  auto controlPoint = model.getDataManager().getRotatedView().transformToScreenSpace(model.getControlPoint().getControlPoint());
  model.zoomIn(BoundingBox<unsigned int>(uvec3(0, 0, 0), controlPoint + uvec3(0, 0, 1)));
}

void Data2DMode::move(Direction direction) {
  if (direction == Direction::backward || direction == Direction::forward) model.getControlPoint().move(direction);
}

std::tuple<ivec3, ivec3> Data2DMode::calculateRotatedBorders() {
  auto anchorA        = model.getAnchorPointStack().getAnchorPoints().getAnchorA();
  auto anchorB        = model.getAnchorPointStack().getAnchorPoints().getAnchorB();
  auto size           = model.getDataManager().getDataView().getDataSize();
  auto rotatedBorderA = model.getCoordinateSystem().transformVector(anchorA);
  auto borderB        = size - anchorB - vec3(1, 1, 1);
  auto rotatedBorderB = model.getCoordinateSystem().transformVector(borderB);

  for (int i = 0; i < 3; i++) {
    rotatedBorderB[i] = (rotatedBorderB[i] > 0) ? 0 : rotatedBorderB[i];
    rotatedBorderA[i] = (rotatedBorderA[i] < 0) ? 0 : rotatedBorderA[i];
  }

  return {rotatedBorderA, rotatedBorderB};
}
