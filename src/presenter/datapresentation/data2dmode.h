/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_PRESENTER_DATAPRESENTATION_DATA2DMODE_H_
#define SRC_PRESENTER_DATAPRESENTATION_DATA2DMODE_H_

#include "model/modelmanager.h"
#include "presenter/datapresentation/datapresentationmode.h"
#include "presenter/datapresentation/multidimensionalmode.h"

/**
 * DataPresentationMode which should be active when one axis of the Data is only 1 unit large. Then the data should be rendered as a flas 2D image.
 *
 * @ingroup dataPresentation
 */
class Data2DMode : public MultidimensionalMode {
public:
  explicit Data2DMode(ModelManager& model, uvec2 resolution) : MultidimensionalMode(model, resolution) {}

  Fl_Offscreen draw() override;

  void rotate(Direction direction) override;

  void clickAt(unsigned int /*x*/, unsigned int /*y*/, bool /*rightClick*/) override {}

  // disable following actions:
  void move(Direction direction) override;
  void invert() override {}

  bool isCursorNS(unsigned int /*x*/, unsigned int /*y*/) override { return false; }
  bool isCursorWE(unsigned int /*x*/, unsigned int /*y*/) override { return false; }
  bool isCursorNWSE(unsigned int /*x*/, unsigned int /*y*/) override { return false; }
  bool isCursorNESW(unsigned int /*x*/, unsigned int /*y*/) override { return false; }

  void release() override{};

  std::string getValue(unsigned int, unsigned) override { return ""; }

  //zoom in is only needed for rotations
  void zoomIn() override;

private:
  std::tuple<ivec3, ivec3> calculateRotatedBorders();
};

#endif  // SRC_PRESENTER_DATAPRESENTATION_DATA2DMODE_H_
