/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_PRESENTER_DATAPRESENTATION_DATA1DMODE_H_
#define SRC_PRESENTER_DATAPRESENTATION_DATA1DMODE_H_

#include "model/modelmanager.h"
#include "presenter/datapresentation/datapresentationmode.h"
#include "util/linestyle.h"

/**
 * DataPresentationMode which shows each axis as a 1D line graph
 *
 * @ingroup dataPresentation
 */
class Data1DMode : public DataPresentationMode {
public:
  explicit Data1DMode(ModelManager& model, uvec2 resolution);

  Fl_Offscreen draw() override;

  void rotate(Direction direction) override;

  void zoomIn() override {}

  void zoomOut() override {}

  void invert() override {}

  void changeGridLines() override {}

  void toggleLabel() override {}

  void move(Direction direction) override;

  void moveMax(Direction dir) override;

  void jump(Direction direction) override;

  void clickAt(unsigned int /*x*/, unsigned int /*y*/, bool /*rightClick*/) override {}

  void resetSelection() override {
    model.getPresentationMode().toggleSelection();
    model.getPresentationMode().setRedraw(true);
  }

  bool isCursorNS(unsigned int /*x*/, unsigned int /*y*/) override { return false; }
  bool isCursorWE(unsigned int /*x*/, unsigned int /*y*/) override { return false; }
  bool isCursorNWSE(unsigned int /*x*/, unsigned int /*y*/) override { return false; }
  bool isCursorNESW(unsigned int /*x*/, unsigned int /*y*/) override { return false; }

  void release() override{};

  std::string getValue(unsigned int, unsigned) override { return ""; }

private:
  ivec3 vectorInDirection(int direction, int val, const uvec3& controlPoint);
  std::array<Fl_Offscreen, 3> drawAxis(unsigned int w, unsigned int h);
  std::array<Fl_Offscreen, 3> drawValues(unsigned int w, unsigned int h, double min, double max);
  std::set<double> calculateTicks(double max, double min);
  float simplicity(int baseIndex, int baseSize, int skip, const std::set<double>& labels);
  float coverage(double lmin, double lmax, double dmin, double dmax);
  std::set<double> createLabels(double min, double max, double step);
};

#endif  // SRC_PRESENTER_DATAPRESENTATION_DATA1DMODE_H_
