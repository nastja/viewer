/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "datapresentation.h"
#include "model/anchorpointstack.h"
#include "model/continuouscolorscale.h"
#include <iostream>

DataPresentation::DataPresentation(ModelManager& model)
    : ImagePresentation("Data", model),
      resolution(100, 100),
      data1DMode(model, resolution),
      data2DMode(model, resolution),
      data3DMode(model, resolution) {
  model.getAnchorPointStack().addListener(this);
  model.getDataManager().addListener(this);
  model.getControlPoint().addListener(this);
  model.getInvertMode().addListener(this);
  model.getPresentationMode().addListener(this);
  model.getColorscaleManager().addListener(this);

  is1D = false;

  changeToMultiDimMode();
}

void DataPresentation::onElementChange() {
  //only change on mode change
  if (is1D != model.getPresentationMode().is1DPresentation() || is2D !=  model.getPresentationMode().is2DPresentation()) {
    if (model.getPresentationMode().is1DPresentation()) {
      mode = &data1DMode;
      is1D = true;
      if (!model.getPresentationMode().hasSelection()) {
        model.getPresentationMode().toggleSelection();
        model.getPresentationMode().setRedraw(true);
      }
    } else {
      changeToMultiDimMode();
      is1D = false;
      if (model.getPresentationMode().hasSelection()) {
        model.getPresentationMode().toggleSelection();
        model.getPresentationMode().setRedraw(true);
      }
    }
  }
  updateView();
}

void DataPresentation::selectPresentation(int presentation) {
  model.getPresentationMode().selectPresentation(presentation);
}

void DataPresentation::changeToMultiDimMode() {
  if (model.getPresentationMode().is2DPresentation()) {
    mode = &data2DMode;
    is2D = true;
  }  else {
    mode = &data3DMode;
    is2D = false;
  }
}

void DataPresentation::setResolution(unsigned int x, unsigned int y) {
  resolution.x() = x;
  resolution.y() = y;

  data1DMode.setResolution(resolution);
  data2DMode.setResolution(resolution);
  data3DMode.setResolution(resolution);
}

void DataPresentation::release() {
  mode->release();
}

bool DataPresentation::showsOneBlock() {
  return model.getPresentationMode().isOneBlock();
}
