/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef NASTJA_VIEWER_DRAWER3D_H
#define NASTJA_VIEWER_DRAWER3D_H

#include "model/presentationmode.h"
#include "model/volumetric/rotatedview.h"
#include "presenter/datapresentation/visualizationcoordinatesystem.h"
#include "util/grid.h"
#include "util/types.h"
#include <FL/Enumerations.H>

class Drawer3D {
public:
  Drawer3D(VisualizationCoordinateSystem& coordSys, RotatedView& view, PresentationMode& presentationMode)
      : coordSys(coordSys),
        view(view),
        presentationMode(presentationMode) {}

  void renderNormal(uvec3 controlPoint);

  void renderInverted(uvec3 controlPoint);

  void setRotatedBorder(std::tuple<ivec3, ivec3> border) {
    rotatedBorder = border;
  }

private:
  void drawGridedFrontPlane(uvec3 a, uvec3 b);
  void drawGridedSidePlane(uvec3 a, uvec3 b);
  void drawGridedTopPlane(uvec3 a, uvec3 b);

  void drawFrontPlane(uvec3 a, uvec3 b);
  void drawTopPlane(uvec3 a, uvec3 b);
  void drawSidePlane(uvec3 a, uvec3 b);

  void drawFrontGrid(uvec3 a, uvec3 b);
  void drawTopGrid(uvec3 a, uvec3 b);
  void drawSideGrid(uvec3 a, uvec3 b);

  void drawFrontHorizontalLines(uvec3 a, uvec3 b);
  void drawFrontVerticalLines(uvec3 a, uvec3 b);
  void drawTopHorizontalLines(uvec3 a, uvec3 b);
  void drawTopVerticalLines(uvec3 a, uvec3 b);
  void drawSideHorizontalLines(uvec3 a, uvec3 b);
  void drawSideVerticalLines(uvec3 a, uvec3 b);

  void drawEdges(uvec3 a, uvec3 b);

  void drawFrontBlockPlane(uvec3 a, uvec3 b);
  void drawTopBlockPlane(uvec3 a, uvec3 b);
  void drawSideBlockPlane(uvec3 a, uvec3 b);

  void drawInvertedModeEdges(uvec3 controlPoint);

  uvec3 calculateBlockNum(uvec3 start, uvec3 end);

  inline void drawPixel(uvec2 upperleft, uvec2 upperright, uvec2 lowerright, uvec2 lowerleft, Fl_Color color);
  inline void drawPixelFront(uvec3 coord, Fl_Color color);
  inline void drawPixelTop(uvec3 coord, Fl_Color color);
  inline void drawPixelSide(uvec3 coord, Fl_Color color);

  std::tuple<uvec3, unsigned long> calculateBlockSize(int index, uvec3 pos, uvec3 end, uvec3 planeEnd,
                                                      uvec3 blockSize, int maxBlockNumber);

  Grid getGridState() { return presentationMode.getGridState(); }
  Fl_Color getColor(Colors color) { return presentationMode.getColor(color); }
  bool isShowBlockLine() { return presentationMode.isShowBlockLine(); }

  VisualizationCoordinateSystem& coordSys;
  RotatedView& view;
  PresentationMode& presentationMode;

  std::tuple<ivec3, ivec3> rotatedBorder;
};

#endif  //NASTJA_VIEWER_DRAWER3D_H
