/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_PRESENTER_DATAPRESENTATION_MULTIDIMENSIONALMODE_H_
#define SRC_PRESENTER_DATAPRESENTATION_MULTIDIMENSIONALMODE_H_

#include "model/controlpoint.h"
#include "presenter/datapresentation/datapresentationmode.h"
#include <utility>

/**
 * Abstact base class for Data2DMode and Data3DMode
 *
 * @ingroup dataPresentation
 */
class MultidimensionalMode : public DataPresentationMode {
public:
  explicit MultidimensionalMode(ModelManager& model, uvec2 resolution) : DataPresentationMode(model, resolution) {}

  void zoomIn() override{};

  void zoomOut() override { model.zoomOut(); };

  void changeGridLines() override {
    model.getPresentationMode().toggleGridLine();
  }

  void toggleLabel() override {
    model.getPresentationMode().toggleLabel();
  }

  void move(Direction direction) override {
    model.getControlPoint().move(direction);
  }

  void moveMax(Direction dir) override {
    model.getControlPoint().moveMax(dir);
  }

  void jump(Direction direction) override {
    model.getControlPoint().jump(direction);
  }

  void resetSelection() override {
    if (model.getPresentationMode().hasSelection()) {
      model.getPresentationMode().toggleSelection();
    }

    model.getPresentationMode().setRedraw(true);
  }
};

#endif  // SRC_PRESENTER_DATAPRESENTATION_MULTIDIMENSIONALMODE_H_
