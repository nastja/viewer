/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "colorscalepresentation.h"
#include "presenter/presentationmanager.h"
#include <FL/fl_draw.H>

ColorScalePresentation::ColorScalePresentation(ModelManager& modelManager)
    : ImagePresentation("ColorScale", modelManager), colScale(model.getColorscaleManager()), dataManager(model.getDataManager()) {
  modelManager.getColorscaleManager().addListener(this);
  modelManager.getDataManager().addListener(this);
  resolution = ivec2(1, 1);
}

void ColorScalePresentation::setMinMax(data_t min, data_t max) {
  colScale.setMinMax(min, max);
}

void ColorScalePresentation::setResolution(unsigned int x, unsigned int y) {
  resolution.x() = x;
  resolution.y() = y;
}

Fl_Offscreen ColorScalePresentation::draw() {
  auto height = resolution.y();
  auto width  = resolution.x();
  auto img    = fl_create_offscreen(width, height);
  double min  = colScale.getMin();
  double max  = colScale.getMax();
  if (colScale.isDiscrete()) {
    max += 1.0;
  }
  fl_begin_offscreen(img);
  for (unsigned int y = 0; y <= height; y++) {
    // (y + 1 / height + 2) is in (0, 1]
    auto value = max - (static_cast<double>(y + 1) / static_cast<double>(height + 2) * (max - min));
    fl_color(colScale.getColor(value));
    fl_line(-1, y, width, y);
  }

  fl_end_offscreen();
  return img;
}

void ColorScalePresentation::onElementChange() {
  updateView();
}
