/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_PRESENTER_COLORSCALEPRESENTATION_H_
#define SRC_PRESENTER_COLORSCALEPRESENTATION_H_

#include "event/modellistener.h"
#include "model/colorscalemanager.h"
#include "model/volumetric/datamanager.h"
#include "presenter/imagepresentation.h"
#include <FL/x.H>

/**
 * The Presenter Component for ColorscaleView that renders the current Colorscale gradient as an Image and updates ColorscaleView
 *
 * @ingroup presenter
*/
class ColorScalePresentation : public ImagePresentation {
public:
  explicit ColorScalePresentation(ModelManager& modelManager);

  /**
   * Sets the minimum Value in Colorscale.
   *
   * @param min the new min value
   */
  void setMinMax(data_t min, data_t max);
  void setMin(data_t min) { setMinMax(min, getMax()); }
  void setMax(data_t max) { setMinMax(getMin(), max); }

  /**
   * Set the resolution which should be used for rendering the Image
   *
   * @param x the width
   * @param y the height
   */
  void setResolution(unsigned int x, unsigned int y);

  void onElementChange() override;

  /**
   *
   * @return minimum
   */
  double getMin() {
    return colScale.getMin();
  }

  /**
   *
   * @return maximum
   */
  double getMax() {
    return colScale.getMax();
  }

  const std::string& getCurrentName() const {
    return dataManager.getMappingName();
  }

  const std::vector<std::string>& getAllNames() const {
    return dataManager.getMappingNames();
  }

  void setModel(std::string /*name*/) {
    // colScale.setModel(name);
  }

  Fl_Offscreen draw() override;

private:
  uvec2 resolution;
  ColorscaleManager& colScale;
  DataManager& dataManager;
};

#endif  // SRC_PRESENTER_COLORSCALEPRESENTATION_H_
