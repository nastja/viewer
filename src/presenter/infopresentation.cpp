/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "infopresentation.h"
#include <utility>

InfoPresentation::InfoPresentation(ModelManager& modelManager)
    : Presentation("Info", modelManager) {
  model.getControlPoint().addListener(this);
  model.getDataManager().addListener(this);
  model.getPresentationMode().addListener(this);
}

void InfoPresentation::onElementChange() {
  updateView();
}

uvec3 InfoPresentation::getOrigin() {
  return model.getDataManager().getRotatedView().transformFromScreenSpace(uvec3(0, 0, 0));
}

uvec3 InfoPresentation::getUpper() {
  auto origin  = getOrigin();
  auto anchorA = model.getAnchorPointStack().getAnchorPoints().getAnchorA();
  auto anchorB = model.getAnchorPointStack().getAnchorPoints().getAnchorB();

  uvec3 upper;
  for (int i = 0; i < 3; i++) {
    upper[i] = (origin[i] == anchorA[i]) ? anchorB[i] : anchorA[i];
  }

  return upper;
}
