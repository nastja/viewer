/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_EVENT_MODELELEMENT_H_
#define SRC_EVENT_MODELELEMENT_H_

#include <memory>
#include <queue>
#include <string>
#include <vector>

class ModelListener;

/**
 * @defgroup event
 *
 * This is the event system of the program. Here, the Observer pattern is used:
 *
 * - ModelElement is the Observable
 * - ModelListener is the Observer
 *
 * It enables the model and presenter to get notified when a specific ModelElement changed its state.
 */

/**
 *
 * Observable part of Observer pattern.
 * This is used to pass messages thoughout the presentation.
 * This class is the base class for all elements of the model and can be listened by any class in the model or presentation.
 *
 * @ingroup event
 */
class ModelElement {
public:
  explicit ModelElement(const std::string& name) : name(name) {}
  virtual ~ModelElement() = default;

  /**
   * Adds a ModelListener to this ModelElement. All future calls to notify() will call onElementChange() on the listener.
   * @param listener the Listener to add to this ModelElement
   */

  void addListener(ModelListener* listener);
  /**
   * removes a ModelListener from this ModelElement. All future calls to notify() will *not* do anything to this ModelListener.
   * @param listener the Listener to remove from this ModelElement
   */
  void removeListener(ModelListener* listener);

protected:
  /**
   * Call onElementChange() on all registered ModelListeners
   */
  void notify();

private:
  /** name of this ModelElement for debugging purposes */
  std::string name;

  std::vector<ModelListener*> listeners;
};

#endif  // SRC_EVENT_MODELELEMENT_H_
