/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_EVENT_MODELLISTENER_H_
#define SRC_EVENT_MODELLISTENER_H_

#include <string>

/**
 * This Interface is the Observer part of the Observer Pattern used for update messages in the program.
 *
 * Classed implementing this Interface can register itself to a ModelElement and will get notified whenever something in the ModelElement changes.
 * If that happens, the abstract method onElementChange() will be called by the ModelElement.
 *
 * @ingroup event
 */
class ModelListener {
public:
  explicit ModelListener(const std::string& name) : name(name) {}
  virtual ~ModelListener() = default;

  /**
   * This abstract method will be called whenever something in the ModelElement changes.
   */
  virtual void onElementChange() = 0;
  std::string& getName() { return name; }

private:
  std::string name;
};

#endif  // SRC_EVENT_MODELLISTENER_H_
