/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_VIEW_GUIFRAME_H_
#define SRC_VIEW_GUIFRAME_H_

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Menu_Item.H>
#include <FL/Fl_Window.H>
#include <string>
#include "view/colorscaleview.h"

class PresentationManager;
class VisualizationView;
class MenuBarView;
class InfoView;

/**
 * @defgroup view
 *
 * In this package the User Interface is defined.
 * Only this package has the dependency to FLTK, the used GUI Toolkit.
 */

/**
 * The main Frame which contains all the View instances.
 *
 * @ingroup view
 */
class GUIFrame : public Fl_Window {
public:
  explicit GUIFrame(PresentationManager& presentationManager);

  void showStatusMessage(const std::string& msg);
  void hideStatusMessage();

  void setTitle(const std::string& newTitle) {
    title = "NAStJA viewer | " + newTitle;
    label(title.c_str());
  }

private:
  /**
   * handles input
   * @param e
   * @return whether or not the window could use the input
   */
  int handle(int e) override;

  static void statustimer_cb(void* v);

  std::string statusMessage;
  std::string title;

  MenuBarView* menuBarView;
  VisualizationView* visualizationView;
  Fl_Group* info;
  InfoView* infoView;
  ColorScaleView* colorScaleView;
  int statusheight = 20;
  Fl_Box* status;
};

#endif  // SRC_VIEW_GUIFRAME_H_
