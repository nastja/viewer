/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "flimagebox.h"
#include <FL/fl_draw.H>
#include <dirent.h>
#include <iomanip>
#include <iostream>
#include <lodepng.h>
#include <regex>
#include <sstream>

FLImageBox::FLImageBox(int x, int y, int w, int h, ImagePresentation& presentation) : Fl_Box(x, y, w, h), presentation(&presentation) {
  createScreenshot = false;
}

void FLImageBox::draw() {
  auto img = presentation->draw();
  fl_copy_offscreen(x(), y(), w(), h(), img, 0, 0);
  if (createScreenshot) {
    auto* img_data = new unsigned char[w() * h() * 3];
    fl_read_image(img_data, x(), y(), w(), h());
    saveScreenshot(img_data);
    createScreenshot = false;
  }
  fl_delete_offscreen(img);
}

void FLImageBox::screenshot() {
  createScreenshot = true;
}

void FLImageBox::saveScreenshot(unsigned char* img_data) {
  const std::string start = "Screenshot";
  const std::regex reg("[1-9][0-9]*");

  int i    = 1;
  DIR* dir = opendir(".");

  struct dirent* dp;
  while ((dp = readdir(dir))) {
    std::string name = dp->d_name;

    // if file starts with startstring, extract number and compute i
    if (std::string(name).substr(0, start.size()) == start) {
      // extract index add add to i
      std::smatch match;
      if (std::regex_search(name, match, reg)) {
        int num = std::stoi(match[0]);
        i       = std::max(i, num + 1);
      }
    }
  }
  closedir(dir);

  std::stringstream ss;
  ss << start << std::setw(4) << std::setfill('0') << i << ".png";

  auto fileName = ss.str();
  int error     = lodepng::encode(fileName, img_data, w(), h(), LCT_RGB);
  if (error)
    std::cerr << "Image writing failed!" << std::endl;
  else {
#ifndef NDEBUG
    std::cout << "[Image] File " + fileName + " written." << std::endl;
#endif
  }
}
