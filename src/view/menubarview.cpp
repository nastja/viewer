/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "menubarview.h"
#include "presenter/presentationmanager.h"
#include <FL/Fl.H>
#include <FL/Fl_Preferences.H>
#include <FL/fl_ask.H>
#include <iostream>
#include <utility>

constexpr unsigned int NV_Invert     = 1;
constexpr unsigned int NV_DirectionX = 2;
constexpr unsigned int NV_DirectionY = 4;
constexpr unsigned int NV_DirectionZ = 8;
constexpr unsigned int NV_Jump       = 16;
constexpr unsigned int NV_Max        = 32;
constexpr unsigned int NV_Rotate     = 64;

constexpr unsigned int NV_Forward  = NV_DirectionZ;
constexpr unsigned int NV_Backward = NV_DirectionZ + NV_Invert;

constexpr unsigned int NV_ForwardJump  = NV_Forward + NV_Jump;
constexpr unsigned int NV_BackwardJump = NV_Backward + NV_Jump;
constexpr unsigned int NV_ForwardMax   = NV_Forward + NV_Max;
constexpr unsigned int NV_BackwardMax  = NV_Backward + NV_Max;

constexpr unsigned int NV_Left      = NV_DirectionX + NV_Invert;
constexpr unsigned int NV_Right     = NV_DirectionX;
constexpr unsigned int NV_LeftJump  = NV_Left + NV_Jump;
constexpr unsigned int NV_RightJump = NV_Right + NV_Jump;
constexpr unsigned int NV_LeftMax   = NV_Left + NV_Max;
constexpr unsigned int NV_RightMax  = NV_Right + NV_Max;

constexpr unsigned int NV_Down     = NV_DirectionY + NV_Invert;
constexpr unsigned int NV_Up       = NV_DirectionY;
constexpr unsigned int NV_DownJump = NV_Down + NV_Jump;
constexpr unsigned int NV_UpJump   = NV_Up + NV_Jump;
constexpr unsigned int NV_DownMax  = NV_Down + NV_Max;
constexpr unsigned int NV_UpMax    = NV_Up + NV_Max;

constexpr unsigned int NV_DownRotate     = NV_Down + NV_Rotate;
constexpr unsigned int NV_UpRotate       = NV_Up + NV_Rotate;
constexpr unsigned int NV_ForwardRotate  = NV_Forward + NV_Rotate;
constexpr unsigned int NV_BackwardRotate = NV_Backward + NV_Rotate;
constexpr unsigned int NV_LeftRotate     = NV_Left + NV_Rotate;
constexpr unsigned int NV_RightRotate    = NV_Right + NV_Rotate;

constexpr unsigned int NV_Discrete   = 128;
constexpr unsigned int NV_GridToggle = 0;
constexpr unsigned int NV_GridOff    = 1;
constexpr unsigned int NV_GridGrad   = 2;
constexpr unsigned int NV_GridOn     = 3;

// Workaround for not showing arrow keys on mac osx. Uses the block uio,jkl
#if defined(__APPLE__)
constexpr unsigned int NVFL_Left       = 'j';
constexpr unsigned int NVFL_Right      = 'l';
constexpr unsigned int NVFL_Down       = 'k';
constexpr unsigned int NVFL_Up         = 'i';
constexpr unsigned int NVFL_Page_Up    = 'o';
constexpr unsigned int NVFL_Page_Down  = 'u';
constexpr unsigned int NVFL_Page_UpR   = 'u';
constexpr unsigned int NVFL_Page_DownR = 'o';
#else
constexpr unsigned int NVFL_Left       = FL_Left;
constexpr unsigned int NVFL_Right      = FL_Right;
constexpr unsigned int NVFL_Down       = FL_Down;
constexpr unsigned int NVFL_Up         = FL_Up;
constexpr unsigned int NVFL_Page_Up    = FL_Page_Up;
constexpr unsigned int NVFL_Page_Down  = FL_Page_Down;
constexpr unsigned int NVFL_Page_UpR   = FL_Page_Up;
constexpr unsigned int NVFL_Page_DownR = FL_Page_Down;
#endif

constexpr std::array<int, 60> NV_NumberArray{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,30, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59};

MenuBarView::MenuBarView(int w, PresentationManager& presentationManager)
    : Fl_Sys_Menu_Bar(0, 0, w, 20),
      menuBarPresentation(presentationManager.getMenuBarPresentation()),
      helpWindow(presentationManager.getHelpWindow()),
      settingsDialog(presentationManager.getSettingsDialog()),
      rotationWarning(presentationManager.getRotationWarning()),
      dataPresentation(presentationManager.getDataPresentation()),
      infoPresentation(presentationManager.getInfoPresentation()),
      gui(presentationManager.getGui()) {
  menuBarPresentation.setView(this);

  readSettings();

  // clang-format off
  add("File/Reload",                        FL_COMMAND + 'r',                      reload_cb,      0,                           FL_MENU_DIVIDER);
  add("File/Change Data File",              'y',                                   changeData_cb,  0,                           FL_MENU_DIVIDER);
  add("File/Screenshot",                    'p',                                   0,              0,                           FL_MENU_DIVIDER);
#if !defined(__APPLE__)
  add("File/Preferences...",                0,                                     settings_cb,    this,                        FL_MENU_DIVIDER);
  add("File/Exit",                          0,                                     exit_cb);
#endif

  add("View/Zoom",                          'z',                                   zoom_cb,        (void *)&NV_Forward);
  add("View/Zoom out",                      FL_BackSpace,                          zoom_cb,        (void *)&NV_Backward,        FL_MENU_INVISIBLE);
  add("View/Zoom out",                      FL_SHIFT + 'z',                        zoom_cb,        (void *)&NV_Backward,        FL_MENU_DIVIDER);
  add("View/1D View",                       FL_COMMAND + '1',                      viewMode_cb,    (void *)&NV_NumberArray[1],  FL_MENU_RADIO);
  add("View/2D View",                       FL_COMMAND + '2',                      viewMode_cb,    (void *)&NV_NumberArray[2],  FL_MENU_RADIO);
  add("View/3D View",                       FL_COMMAND + '3',                      viewMode_cb,    (void *)&NV_NumberArray[3],  FL_MENU_RADIO | FL_MENU_VALUE | FL_MENU_DIVIDER);
  add("View/Show Numbers",                  'n',                                   showNumber_cb);
  add("View/Flip Inside Out",               'f',                                   invert_cb,      0,                           FL_MENU_TOGGLE);
  add("View/Reset",                         'r',                                   reset_cb,       0,                           FL_MENU_DIVIDER);
  add("View/Next Colorbar Map",             'c',                                   color_cb,       (void *)&NV_Forward);
  add("View/Previous Colorbar Map",         FL_SHIFT + 'c',                        color_cb,       (void *)&NV_Backward);
  add("View/Invert Colorbar",               FL_ALT + 'c',                          color_cb,       (void *)&NV_Invert);
  add("View/Discrete Colorbar",             'd',                                   color_cb,       (void *)&NV_Discrete,        FL_MENU_TOGGLE);
  add("View/Data Mapping/Next Mapping",     'm',                                   color_cb,       (void *)&NV_ForwardJump);
  add("View/Data Mapping/Previous Mapping", FL_SHIFT + 'm',                        color_cb,       (void *)&NV_BackwardJump,    FL_MENU_DIVIDER);
  add("View/Data Mapping/Last Mapping",     FL_ALT + '0',                          color_cb,       (void *)&NV_Jump);
  add("View/Components/Next Component",     'x',                                   component_cb,   (void *)&NV_Forward);
  add("View/Components/Previous Component", FL_SHIFT + 'x',                        component_cb,   (void *)&NV_Backward,        FL_MENU_DIVIDER);
  add("View/Grid",                          'g',                                   grid_cb,        (void *)&NV_GridToggle,      FL_MENU_INVISIBLE);
  add("View/Grid/Block Lines",              FL_ALT + 'g',                          blockgrid_cb,   0,                           FL_MENU_TOGGLE | FL_MENU_DIVIDER);
  add("View/Grid/Off",                      0,                                     grid_cb,        (void *)&NV_GridOff,         FL_MENU_RADIO | FL_MENU_VALUE);
  add("View/Grid/Gradient",                 0,                                     grid_cb,        (void *)&NV_GridGrad,        FL_MENU_RADIO);
  add("View/Grid/On",                       0,                                     grid_cb,        (void *)&NV_GridOn,          FL_MENU_RADIO | FL_MENU_DIVIDER);
  add("View/Grid/Gradient Mapping/Auto",    FL_CONTROL + FL_ALT + '0',             gridView_cb,    (void *)&NV_Discrete,        FL_MENU_RADIO | FL_MENU_VALUE);

  add("View/Reset Selection",               FL_Escape,                             unselect_cb,    0);

  add("Navigation/Previous Time",           '-',                                   time_cb,        (void *)&NV_Backward);
  add("Navigation/Next Time",               '+',                                   time_cb,        (void *)&NV_Forward,         FL_MENU_DIVIDER);
  add("Navigation/Left",                    NVFL_Left,                             move_cb,        (void *)&NV_Left);
  add("Navigation/Right",                   NVFL_Right,                            move_cb,        (void *)&NV_Right);
  add("Navigation/Down",                    NVFL_Down,                             move_cb,        (void *)&NV_Down);
  add("Navigation/Up",                      NVFL_Up,                               move_cb,        (void *)&NV_Up);
  add("Navigation/Backward",                NVFL_Page_Up,                          move_cb,        (void *)&NV_Backward);
  add("Navigation/Forward",                 NVFL_Page_Down,                        move_cb,        (void *)&NV_Forward,         FL_MENU_DIVIDER);
  add("Navigation/Jump/Previous Time",      keyModifierJump + '-',                 time_cb,        (void *)&NV_BackwardJump);
  add("Navigation/Jump/Next Time",          keyModifierJump + '+',                 time_cb,        (void *)&NV_ForwardJump,     FL_MENU_DIVIDER);
  add("Navigation/Jump/Left",               keyModifierJump + NVFL_Left,           move_cb,        (void *)&NV_LeftJump);
  add("Navigation/Jump/Right",              keyModifierJump + NVFL_Right,          move_cb,        (void *)&NV_RightJump);
  add("Navigation/Jump/Down",               keyModifierJump + NVFL_Down,           move_cb,        (void *)&NV_DownJump);
  add("Navigation/Jump/Up",                 keyModifierJump + NVFL_Up,             move_cb,        (void *)&NV_UpJump);
  add("Navigation/Jump/Backward",           keyModifierJump + NVFL_Page_Up,        move_cb,        (void *)&NV_BackwardJump);
  add("Navigation/Jump/Forward",            keyModifierJump + NVFL_Page_Down,      move_cb,        (void *)&NV_ForwardJump,     FL_MENU_DIVIDER);
  add("Navigation/Jump/Scale: 10",          0,                                     setjump_cb,     0);
  add("Navigation/Maximum/Previous Time",   keyModifierMaximum + '-',              time_cb,        (void *)&NV_BackwardMax);
  add("Navigation/Maximum/Next Time",       keyModifierMaximum + '+',              time_cb,        (void *)&NV_ForwardMax,      FL_MENU_DIVIDER);
  add("Navigation/Maximum/Left",            keyModifierMaximum + NVFL_Left,        move_cb,        (void *)&NV_LeftMax);
  add("Navigation/Maximum/Right",           keyModifierMaximum + NVFL_Right,       move_cb,        (void *)&NV_RightMax);
  add("Navigation/Maximum/Down",            keyModifierMaximum + NVFL_Down,        move_cb,        (void *)&NV_DownMax);
  add("Navigation/Maximum/Up",              keyModifierMaximum + NVFL_Up,          move_cb,        (void *)&NV_UpMax);
  add("Navigation/Maximum/Backward",        keyModifierMaximum + NVFL_Page_Up,     move_cb,        (void *)&NV_BackwardMax);
  add("Navigation/Maximum/Forward",         keyModifierMaximum + NVFL_Page_Down,   move_cb,        (void *)&NV_ForwardMax);
  add("Navigation/Rotate/Flip Left",        keyModifierRotation + NVFL_Left,       move_cb,        (void *)&NV_LeftRotate);
  add("Navigation/Rotate/Flip Right",       keyModifierRotation + NVFL_Right,      move_cb,        (void *)&NV_RightRotate);
  add("Navigation/Rotate/Flip Back",        keyModifierRotation + NVFL_Up,         move_cb,        (void *)&NV_UpRotate);
  add("Navigation/Rotate/Flip Front",       keyModifierRotation + NVFL_Down,       move_cb,        (void *)&NV_DownRotate,      FL_MENU_DIVIDER);
  add("Navigation/Rotate/Clockwise",        keyModifierRotation + NVFL_Page_DownR, move_cb,        (void *)&NV_ForwardRotate);
  add("Navigation/Rotate/Counterclockwise", keyModifierRotation + NVFL_Page_UpR,   move_cb,        (void *)&NV_BackwardRotate);

  add("Help/Show Keys",                     FL_F + 1,                              help_cb,        0,                           FL_MENU_INVISIBLE);
  add("Help/Show Keys",                     'h',                                   help_cb);
  // clang-format on

#ifdef __APPLE__
  Fl_Mac_App_Menu::custom_application_menu_items(settings);
#endif
  update();
}

void MenuBarView::readSettings() {
  Fl_Preferences prefs(Fl_Preferences::USER, "nastja", "viewer");
  Fl_Preferences prefkeys(prefs, "keys");
  prefkeys.get("ModifierJump", keyModifierJump, FL_ALT);
  prefkeys.get("ModifierMaximum", keyModifierMaximum, FL_COMMAND);
  prefkeys.get("ModifierRotation", keyModifierRotation, FL_ALT + FL_COMMAND);

  // Check for correct modifier values, if not use default
  if ((keyModifierJump & (FL_ALT + FL_COMMAND + FL_CONTROL + FL_SHIFT)) != keyModifierJump) keyModifierJump = FL_ALT;
  if ((keyModifierMaximum & (FL_ALT + FL_COMMAND + FL_CONTROL + FL_SHIFT)) != keyModifierMaximum) keyModifierMaximum = FL_COMMAND;
  if ((keyModifierRotation & (FL_ALT + FL_COMMAND + FL_CONTROL + FL_SHIFT)) != keyModifierRotation) keyModifierRotation = FL_ALT + FL_COMMAND;

  Fl_Preferences prefRotation(prefs, "defaultAction");
  prefRotation.get("onRotation", rotationDefault, 0);
  if (rotationDefault < 0 || rotationDefault > 2) rotationDefault = 0;
}

void MenuBarView::saveDefaultAction(const std::string& action, int value) {
  Fl_Preferences prefs(Fl_Preferences::USER, "nastja", "viewer");
  Fl_Preferences prefRotation(prefs, "defaultAnswers");
  prefRotation.set(action.c_str(), value);
}

void MenuBarView::updateShortcuts(const Fl_Menu_Item* item, int modifier, int size) {
  for (int i = 0; i < size; i++) {
    auto subitem = (Fl_Menu_Item*)item->next(i);
    int shortcut = subitem->shortcut() & 0xFFFF;
    subitem->shortcut(shortcut + modifier);
  }
}

void MenuBarView::updateShortcuts() {
  updateShortcuts(find_item("Navigation/Jump/Previous Time"), keyModifierJump, 8);
  updateShortcuts(find_item("Navigation/Maximum/Previous Time"), keyModifierMaximum, 8);
  updateShortcuts(find_item("Navigation/Rotate/Left"), keyModifierRotation, 6);

#ifdef __APPLE__
  Fl_Sys_Menu_Bar::update();
#endif
  gui.showStatusMessage("Shortcuts updated");
}

void MenuBarView::time_cb(Fl_Widget* w, void* v) {
  auto view = dynamic_cast<MenuBarView*>(w);
  auto flag = *(unsigned int*)v;
  view->gui.showStatusMessage("Load data...");
  switch (flag) {
    case NV_Forward:
      view->menuBarPresentation.nextTime();
      break;
    case NV_Backward:
      view->menuBarPresentation.previousTime();
      break;
    case NV_ForwardJump:
      view->menuBarPresentation.jumpForward();
      break;
    case NV_BackwardJump:
      view->menuBarPresentation.jumpBackward();
      break;
    case NV_ForwardMax:
      view->menuBarPresentation.maxTime();
      break;
    case NV_BackwardMax:
      view->menuBarPresentation.minTime();
      break;
  }
  view->gui.showStatusMessage("Data loaded");
}

void MenuBarView::move_cb(Fl_Widget* w, void* v) {
  auto view      = dynamic_cast<MenuBarView*>(w);
  auto flag      = *(unsigned int*)v;
  auto direction = flag & 0x0F;
  auto* warning  = view->rotationWarning;

  if ((flag & NV_Jump) == NV_Jump) {
    view->dataPresentation.jumpControlPoint(static_cast<Direction>(direction));
  } else if ((flag & NV_Max) == NV_Max) {
    view->dataPresentation.moveMax(static_cast<Direction>(direction));
  } else if ((flag & NV_Rotate) == NV_Rotate) {
    if (view->infoPresentation.getControlPoint() != view->infoPresentation.getUpper()) {
      unsigned int choice = 0;
      if (view->rotationDefault == 0 && !view->dataPresentation.is2DPresentation()) {
        choice = warning->showChoice();
        if (choice == NV_Cancel) return;

        if (choice & NV_Remember) {
          choice &= ~NV_Remember;
          view->saveDefaultAction("onRotation", choice);
          view->rotationDefault = choice;
        }
      } else {
        choice = view->rotationDefault;
      }
      if (choice == 1 || view->dataPresentation.is2DPresentation()) {
        view->dataPresentation.zoomIn();
      }
    }
    view->dataPresentation.rotate(static_cast<Direction>(direction));
  } else {
    view->dataPresentation.moveControlPoint(static_cast<Direction>(direction));
  }
}

void MenuBarView::zoom_cb(Fl_Widget* w, void* v) {
  auto view = dynamic_cast<MenuBarView*>(w);
  auto flag = *(unsigned int*)v;

  switch (flag) {
    case NV_Forward:
      view->dataPresentation.zoomIn();
      break;
    case NV_Backward:
      view->dataPresentation.zoomOut();
      break;
  }
}

void MenuBarView::color_cb(Fl_Widget* w, void* v) {
  auto view = dynamic_cast<MenuBarView*>(w);
  auto flag = *(unsigned int*)v;

  switch (flag) {
    case NV_Forward:
    case NV_Backward:
      view->menuBarPresentation.switchColorbarSet(static_cast<Direction>(flag));
      break;
    case NV_ForwardJump:
    case NV_BackwardJump:
      view->menuBarPresentation.switchDataView(static_cast<Direction>(flag - NV_Jump));
      break;
    case NV_Invert:
      view->menuBarPresentation.invertColorscale();
      break;
    case NV_Discrete:
      view->menuBarPresentation.toggleDiscreteColor();
      break;
    case NV_Jump:
      view->menuBarPresentation.switchLastDataView();
      break;
  }
}

void MenuBarView::component_cb(Fl_Widget* w, void* v) {
  auto view = dynamic_cast<MenuBarView*>(w);
  auto flag = *(unsigned int*)v;

  view->menuBarPresentation.switchComponent(static_cast<Direction>(flag));
}

void MenuBarView::setjump_cb(Fl_Widget* w, void* /*v*/) {
  fl_message_title("Jump width");
  auto ret = fl_input("Insert jump width.", "");
  int jump = 0;
  if (*ret != '\0') jump = std::atoi(ret);

  if (jump <= 0) {
    fl_message("Invalid input, use a positive number.");
  } else {
    auto view = dynamic_cast<MenuBarView*>(w);
    view->menuBarPresentation.setJumpScale(jump);
  }
}

void MenuBarView::settings_cb(Fl_Widget* /*w*/, void* v) {
  auto view      = static_cast<MenuBarView*>(v);
  auto* settings = view->settingsDialog;

  settings->setModifierJump(view->keyModifierJump);
  settings->setModifierMaximum(view->keyModifierMaximum);
  settings->setModifierRotation(view->keyModifierRotation);

  int value = settings->showSettings();
  if (value) {
    view->readSettings();
  }
  if (value & NV_ModifiedKeys) {
    view->updateShortcuts();
  }
}

void MenuBarView::gridView_cb(Fl_Widget* w, void* v) {
  auto view = dynamic_cast<MenuBarView*>(w);
  auto flag = *(int*)v;
  if (flag == NV_Discrete) {
    view->menuBarPresentation.setGridDataViewAuto();
  } else {
    view->menuBarPresentation.switchGridDataView(flag);
  }
}

/**
 * Removes all menu items in the submenu after the given baseItem.
 *
 * @param baseItem  The menu item.
 */
void MenuBarView::removeMenuItems(const Fl_Menu_Item* baseItem) {
  auto* item = baseItem->next();
  while (item->label() != nullptr) {
    remove(find_index(item));
    item = baseItem->next();
  }
}

/**
 * Inserts the mapping menu items to the submenu given by baseItem.
 *
 * @param baseItem      The menu item after that the items will be inserted.
 * @param shortcutBase  The basis of the shortcuts.
 * @param cb            The callback function.
 */
void MenuBarView::insertMappingMenuItems(const Fl_Menu_Item* baseItem, int shortcutBase, Fl_Callback* cb) {
  auto& mappings = menuBarPresentation.getMappingNames();

  auto index = find_index(baseItem->next(1));
  for (int i = 0; i < mappings.size() && i < NV_NumberArray.size(); i++) {
    int key = 0;
    if (i < 9) {
      key = shortcutBase + i;
    } else if (i < 18) {
      key = shortcutBase + FL_SHIFT + (i - 9);
    }

    index = insert(index, mappings[i].c_str(), key, cb, (void *)&NV_NumberArray[i], FL_MENU_RADIO);
    index++;
  }
}

/**
 * Inserts the components menu items to the submenu given by baseItem.
 *
 * @param baseItem      The menu item after that the items will be inserted.
 * @param shortcutBase  The basis of the shortcuts.
 * @param cb            The callback function.
 */
void MenuBarView::insertComponentMenuItems(const Fl_Menu_Item* baseItem, int shortcutBase, Fl_Callback* cb) {
  auto index = find_index(baseItem->next(1));
  for (int i = 0; i < menuBarPresentation.getNumberOfComponents() && i < NV_NumberArray.size(); i++) {
    std::string s = "Component ";
    if (i >= componentNames.size()) {
      s.append(std::to_string(i));
      componentNames.push_back(s);
    }

    int key = 0;
    if (i < 9) {
      key = shortcutBase + i;
    } else if (i < 18) {
      key = shortcutBase + FL_SHIFT + (i - 9);
    }

    index = insert(index, componentNames[i].c_str(), key, cb, (void *)&NV_NumberArray[i], FL_MENU_RADIO);
    index++;
  }
}

void MenuBarView::updateJumpScale() {
  auto jump = menuBarPresentation.getJumpScale();

  jumpMenuText  = "Scale: " + std::to_string(jump);
  auto menuitem = find_index(setjump_cb);
  replace(menuitem, jumpMenuText.c_str());
}

void MenuBarView::updateGrid() {
  auto* itemBlockLine = (Fl_Menu_Item*)find_item("View/Grid/Block Lines");
  if (dataPresentation.showsOneBlock()) {
    itemBlockLine->deactivate();
    itemBlockLine->clear();
  } else {
    itemBlockLine->activate();
    if (menuBarPresentation.isShowBlockLine()) {
      itemBlockLine->set();
    } else {
      itemBlockLine->clear();
    }
  }

  auto grid = static_cast<unsigned int>(menuBarPresentation.getGridState());

  auto* item = (Fl_Menu_Item*)find_item("View/Grid/Off")->next(grid - 1);
  setonly(item);

  auto* itemAdd = (Fl_Menu_Item*)find_item("View/Grid/Gradient Mapping/Auto");

  removeMenuItems(itemAdd);
  insertMappingMenuItems(itemAdd, FL_CONTROL + FL_ALT + '1', gridView_cb);
  setonly(itemAdd->next(menuBarPresentation.getGridDataView() + 1));

  auto* itemSubmenu = (Fl_Menu_Item*)find_item("View/Grid/Gradient Mapping");
  if (grid == NV_GridGrad) {
    itemSubmenu->activate();
  } else {
    itemSubmenu->deactivate();
  }
}

void MenuBarView::updateDataMapping() {
  if (menuBarPresentation.getNumberOfColorMappings() > 1) {
    auto* item = (Fl_Menu_Item*)find_item("View/Data Mapping");
    item->activate();
    auto* itemAdd = (Fl_Menu_Item*)find_item("View/Data Mapping/Last Mapping");

    removeMenuItems(itemAdd);
    insertMappingMenuItems(itemAdd, FL_COMMAND + FL_ALT + '1', setDataView_cb);

    // set the selected mapping
    setonly(itemAdd->next(menuBarPresentation.getDataView() + 1));
  } else {
    auto* item = (Fl_Menu_Item*)find_item("View/Data Mapping");
    item->deactivate();
  }
}

void MenuBarView::updateComponents() {
  if (menuBarPresentation.getNumberOfComponents() > 1) {
    auto* item = (Fl_Menu_Item*)find_item("View/Components");
    item->activate();
    auto* itemAdd = (Fl_Menu_Item*)find_item("View/Components/Previous Component");

    removeMenuItems(itemAdd);
    insertComponentMenuItems(itemAdd, FL_COMMAND + FL_ALT + '1', setComponent_cb);

    // set the selected mapping
    setonly(itemAdd->next(menuBarPresentation.getComponent() + 1));
  } else {
    auto* item = (Fl_Menu_Item*)find_item("View/Components");
    item->deactivate();
  }
}

void MenuBarView::updateDiscreteColorbar() {
  auto* item       = (Fl_Menu_Item*)find_item("View/Discrete Colorbar");
  auto* itemInvert = (Fl_Menu_Item*)find_item("View/Invert Colorbar");
  if (menuBarPresentation.colorBarIsDiscrete()) {
    item->set();
    itemInvert->deactivate();
  } else {
    item->clear();
    itemInvert->activate();
  }
}

void MenuBarView::update() {
  updateJumpScale();
  updateGrid();
  updateDataMapping();
  updateComponents();
  updateDiscreteColorbar();

#ifdef __APPLE__
  Fl_Sys_Menu_Bar::update();
#endif

  redraw();
}

constexpr unsigned int lookup_key[6] = {
    NV_Left,      // FL_Left
    NV_Up,        // FL_Up
    NV_Right,     // FL_Right
    NV_Down,      // FL_Down
    NV_Backward,  // FL_Page_Up
    NV_Forward    // FL_Page_Down
};

int MenuBarView::handle(int event) {
  if (Fl_Sys_Menu_Bar::handle(event)) return 1;
  if (event != FL_SHORTCUT) return 0;

  auto state = Fl::event_state(FL_SHIFT | FL_COMMAND | FL_ALT | FL_CONTROL);
  auto key   = Fl::event_key();

  if (key >= FL_Left && key <= FL_Page_Down) {
    unsigned int val = lookup_key[key - FL_Left];
    if (state == keyModifierMaximum) {
      val |= NV_Max;
    } else if (state == keyModifierJump) {
      val |= NV_Jump;
    } else if (state == keyModifierRotation) {
      val |= NV_Rotate;
    }

    move_cb(this, (void*)&val);
    return 1;
  }

  return 0;
}
