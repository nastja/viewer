/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "helpwindow.h"
#include <FL/Fl.H>
#include <FL/Fl_Tabs.H>

HelpWindow::HelpWindow(int x, int y, int w, int h) : Fl_Window(x, y, w, h, "Help") {
  auto shortcutBuff = new Fl_Text_Buffer();
  auto abouBuff     = new Fl_Text_Buffer();

  shortcutBuff->text(
      "Controls:\n\n"

      "Help window:\n"
      "[H], [F1]:  Opens the help window.\n\n"

      "Reset:\n"
      "[R]: Resets the 1D & 3D presentation to the original perspective.\n\n"

      "Reload:\n"
      "[Ctrl] + [R]: Reloads the current data.\n\n"

      "Frames:\n"
      "[+]:         Shows the next frame.\n"
      "[-]:           Shows the previous frame.\n"

      "Modify with [Alt] according to the jumpcalibration or [Cmd] for maximum\n"

      "----------\n"
      "Switch between 1D & 3D presentation\n"
      "[V]:         Switches between 1D & 3D presentation.\n\n"
      "----------\n"
      "1D presentation:\n"

      "Controling between, and on the axes:\n"
      "[←]:         Moves the control point on the axis to the left.\n"
      "[→]:         Moves the control point on the axis to the right.\n"
      "[↑]:         Focus the axis above.\n"
      "[↓]:         Focus the axis below.\n\n"

      "Adjust line style:\n"
      "[Ctrl] + [↑]: Line is getting thicker.\n"
      "[Ctrl] + [↓]: Line is getting thinner.\n"
      "[Ctrl] + [←]: Line style changes.\n"
      "[Ctrl] + [→]: Line style changes.\n\n"

      "----------\n"
      "3D presentation:\n"

      "Turning the cuboid:\n"
      "[Ctrl] + [←]: Turns the cuboid by 90° to the left.\n"
      "[Ctrl] + [↑]: Turns the cuboid by 90° upwards\n"
      "[Ctrl] + [→]: Turns the cuboid by 90° to the downwardst.\n"
      "[Ctrl] + [↓]: Turns the cuboid by 90° down\n\n"

      "Rotation of the cuboid in the plane:\n"
      "[Ctrl] + [Pg Up]: Cuboid is rotated 90° left in the view plane.\n"
      "[Ctrl] + [Pg Dn]: Cuboid is rotated 90° right in the view plane.\n\n"

      "Control of the Conrolpoint:\n"
      "[←]:     Moves the control point to the left along the horizontal axis.\n"
      "[→]:     Moves the control point to the right along the horizontal axis.\n"
      "[↑]:     Moves the control point upwards along the vertical axis.\n"
      "[↓]:     Moves the control point downwards along the vertical axis.\n"
      "[Pg Up]: Moves the control point backwards along the depth axis.\n"
      "[Pg Dn]: Moves the control point forwards along the depth axis.\n\n"

      "Modify with [Alt] according to the jumpcalibration or [Cmd] for maximum\n"

      "[I]:    Inverts the control point\n\n"

      "Zoom:\n"
      "[Z]:   Zooms in.\n"
      "[Backspace] or [Shift] + [Z] : Zooms out one step.\n\n"

      "View:\n"
      "[N]:             Toggles measures.\n"
      "[G]:             Switch between 'grid lines' and 'grid only for gradients'.\n"
      "[C]:             Change Colors\n"
      "[X]:             Change data mapping\n"
      "[Y]:             Switch between files\n"
      "[Alt] + [C]: Invert Colorscale\n"
      "[D]:             Change colorscale mode\n\n"

      "Screenshot:\n"
      "[P]:   Takes screenshot and saves it.\n");

  abouBuff->text(
      "Version 1.0 \n\n"

      "Alexander Dick \n"
      "Patrick Jaberg \n"
      "Thomas Koch \n"
      "Marcel Riedel \n"
      "Marco Strohmaier \n");

  auto tabs = new Fl_Tabs(0, 5, w, h);
  {
    auto Shortcuts = new Fl_Group(0, 30, w, h - 30, "Shortcuts");
    {
      auto text = new Fl_Text_Display(0, 30, w, h - 30);
      text->buffer(shortcutBuff);
    }
    Shortcuts->end();

    auto about = new Fl_Group(0, 30, w, h - 30, "About");
    {
      auto text = new Fl_Text_Display(0, 30, w, h - 30);
      text->buffer(abouBuff);
    }
    about->end();
  }
  tabs->end();

  end();
}

void HelpWindow::update() {
  redraw();
}

void HelpWindow::toggle() {
  if (shown() != 0) {
    hide();
  } else {
    show();
  }
}

int HelpWindow::handle(int event) {
  switch (event) {
    case FL_KEYBOARD:
      return handle_key(event);
    case FL_PUSH:
    case FL_RELEASE:
    case FL_DRAG:
    case FL_MOVE:
    case FL_FOCUS:
    case FL_UNFOCUS:
    default:
      return Fl_Group::handle(event);
  }
}

int HelpWindow::handle_key(int /*event*/) {
  auto state = Fl::event_state(FL_SHIFT | FL_COMMAND | FL_ALT | FL_CONTROL);
  auto key   = Fl::event_key();

  if ((key == 'h' || key == FL_F + 1) && state == 0) {
    toggle();
  } else {
    return 0;
  }

  return 1;
}
