/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_VIEW_DATAVIEW_H_
#define SRC_VIEW_DATAVIEW_H_

#include "presenter/presentationmanager.h"
#include "view/flimagebox.h"
#include "view/view.h"
#include <FL/Fl_Box.H>

/**
 * The GUI Component for DataPresentation that draws the main view to the loaded data.
 *
 * It will only show an Image that is rendered by DataPresentation in the correct resolution.
 *
 * @ingroup view
 */
class VisualizationView : public Fl_Box, View {
public:
  VisualizationView(unsigned int x, unsigned int y, unsigned int w, unsigned int h, PresentationManager& presentationManager);

  /**
   * Redraws this View and updates  all values
   */
  void update() override;

  /**
   * overwritten resize operation
   *
   * @param x  x Coordinate of the window
   * @param y  y Coordinate of the window
   * @param w  with of the window
   * @param h  height of the window
   */
  void resize(int x, int y, int w, int h) override;

  /**
   * handles user input
   *
   * @param e  { parameter_description }
   *
   * @return whether or not the window could use the input
   */
  int handle(int e) override;

  void screenshot();

private:
  FLImageBox* box;
  DataPresentation& dataPresentation;
};

#endif  // SRC_VIEW_DATAVIEW_H_
