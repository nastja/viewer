/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "infoview.h"
#include "view/coordinatesystemview.h"
#include "view/guiframe.h"
#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Value_Input.H>
#include <FL/Fl_Value_Output.H>
#include <FL/fl_draw.H>
#include <iostream>

constexpr int innerBorder    = 5;
constexpr int groupLabelSize = 15;

InfoView::InfoView(int x, int y, int w, int h, PresentationManager& presentationManager)
    : Fl_Group(x, y, w, h), infoPresentation(presentationManager.getInfoPresentation()) {
  infoPresentation.setView(this);

  auto infoBox = new Fl_Box(x, y, w, h);
  infoBox->box(FL_UP_BOX);

  int coordinateSystemSize  = 145;
  auto tocenter             = (w - coordinateSystemSize) / 2;
  auto coordinateSystemView = new CoordinateSystemView(x + tocenter, y + innerBorder, coordinateSystemSize, coordinateSystemSize, presentationManager);
  coordinateSystemView->end();
  y = coordinateSystemView->y() + coordinateSystemView->h();

  y += groupLabelSize + innerBorder;
  auto coordGroup = new Fl_Group(x + innerBorder, y, w - 2 * innerBorder, 50, "Coordinates:");
  coordGroup->align(FL_ALIGN_TOP_LEFT);
  coordGroup->labelfont(FL_BOLD);
  // coordGroup->box(FL_FRAME_BOX);

  coords = new Fl_Multiline_Output(x + innerBorder, y, w - 2 * innerBorder, 50);
  coords->box(FL_NO_BOX);
  coords->textfont(FL_COURIER);
#ifdef __APPLE__
  coords->textsize(coords->textsize() + 1);
#endif
  coords->clear_visible_focus();

  coordGroup->end();
  y = coordGroup->y() + coordGroup->h();

  y += groupLabelSize + innerBorder;
  auto frameGroup = new Fl_Group(x + innerBorder, y, w - 2 * innerBorder, 20, "Frame:");
  frameGroup->align(FL_ALIGN_TOP_LEFT);
  frameGroup->labelfont(FL_BOLD);
  // frameGroup->box(FL_FRAME_BOX);

  frameProgress = new Fl_Progress(x + innerBorder, y, w - 2 * innerBorder, 20, frameLabel.c_str());
  frameProgress->box(FL_UP_BOX);
  frameProgress->maximum(infoPresentation.getMaxFrame());
  frameProgress->selection_color(0x7777ff00);

  frameGroup->end();
  y = frameGroup->y() + frameGroup->h();

  y += groupLabelSize + innerBorder;
  auto selectedPointGroup = new Fl_Group(x + innerBorder, y, w, 100, "Value:");
  selectedPointGroup->align(FL_ALIGN_TOP_LEFT);
  selectedPointGroup->labelfont(FL_BOLD);

  vortexValue = new Fl_Output(x + innerBorder, y, w - 2 * innerBorder, 16);
  vortexValue->align(FL_ALIGN_TOP_LEFT);
  vortexValue->box(FL_NO_BOX);
  vortexValue->textfont(FL_COURIER);
#ifdef __APPLE__
  vortexValue->textsize(coords->textsize() + 1);
#endif
  vortexValue->clear_visible_focus();

  selectedPointGroup->end();

  update();
}

int getIntLength(int i) {
  int length = 0;
  do {
    length++;
    i /= 10;
  } while (i);

  return length;
}

void InfoView::update() {
  // Coordinates
  auto origin       = infoPresentation.getOrigin();
  auto upper        = infoPresentation.getUpper();
  auto controlPoint = infoPresentation.getControlPoint();

  int lengthOrigin = getIntLength(std::max(std::max(origin.x(), origin.y()), origin.z()));
  int lengthUpper  = getIntLength(std::max(std::max(upper.x(), upper.y()), upper.z()));

  int lengthControlPoint = std::max(lengthOrigin, lengthUpper);

  char tmp[1024];
  std::snprintf(tmp, 1024,
                "x [%*d, %*d] %*d\n"
                "y [%*d, %*d] %*d\n"
                "z [%*d, %*d] %*d",
                lengthOrigin, origin.x(), lengthUpper, upper.x(), lengthControlPoint, controlPoint.x(),
                lengthOrigin, origin.y(), lengthUpper, upper.y(), lengthControlPoint, controlPoint.y(),
                lengthOrigin, origin.z(), lengthUpper, upper.z(), lengthControlPoint, controlPoint.z());
  coords->value(tmp);

  // Frames
  frameProgress->value(infoPresentation.getFrame());
  frameLabel = std::to_string(infoPresentation.getFrame()) + "/" + std::to_string(infoPresentation.getMaxFrame());

  auto* gui = dynamic_cast<GUIFrame*>(window());
  if (gui) {
    gui->setTitle(getDataFieldName());
  }

  vortexValue->value(infoPresentation.getCurrentVortexValue().c_str());

  redraw();
}

void InfoView::callback(Fl_Widget* /*w*/, void* /*v*/) {
}
