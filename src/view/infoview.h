/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_VIEW_INFOVIEW_H_
#define SRC_VIEW_INFOVIEW_H_

#include "presenter/infopresentation.h"
#include "presenter/presentationmanager.h"
#include <FL/Fl_Box.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Multiline_Output.H>
#include <FL/Fl_Progress.H>
#include <FL/Fl_Value_Input.H>
#include <FL/Fl_Value_Output.H>

/**
 * The GUI Component for InfoPresentation that shows general information about the data.
 *
 * It shows:
 *
 * - The 3 coordinates of the control point as editable text boxes
 * - the current time as editable text box
 * - the current jumpScale for ControlPoint and time as editable text boxes
 * - 2 x 3 coordinates of the selection
 * - current values at the coordinates of the control point and both slection corners
 *
 *
 * @ingroup view
 */
class InfoView : public Fl_Group, public View {
public:
  InfoView(int x, int y, int w, int h, PresentationManager& presentationManager);

  /**
   * Redraws View and updates  all values
   */
  void update() override;

  std::string getDataFieldName() {
    return infoPresentation.getDataFieldName();
  }

private:
  /**
   * handles callback for different widgets inside this class
   *
   * @param w Widget, that calls callback
   * @param v
   */
  static void callback(Fl_Widget* w, void* v);

  InfoPresentation& infoPresentation;

  Fl_Output* coords;

  Fl_Progress* frameProgress;
  std::string frameLabel = "0";

  Fl_Output* vortexValue;
};

#endif  // SRC_VIEW_INFOVIEW_H_
