/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_VIEW_MENUBARVIEW_H_
#define SRC_VIEW_MENUBARVIEW_H_

#include "presenter/infopresentation.h"
#include "presenter/menubarpresentation.h"
#include "presenter/presentationmanager.h"
#include "view/helpwindow.h"
#include <FL/Fl.H>
#include <FL/Fl_Sys_Menu_Bar.H>
#include <FL/filename.H>
#include "guiwarning.h"

/**
 * The GUI Component containing the MenuBar.
 *
 * It can change some settings in the model.
 *
 * @ingroup view
 */
class MenuBarView : public Fl_Sys_Menu_Bar, public View {
public:
  MenuBarView(int w, PresentationManager& presentationManager);
  int handle(int event) override;

private:
  void readSettings();
  void updateShortcuts();
  void updateShortcuts(const Fl_Menu_Item* item, int modifier, int size);
  void update() override;
  void updateJumpScale();
  void updateGrid();
  void updateDataMapping();
  void updateComponents();
  void updateDiscreteColorbar();

  void saveDefaultAction(const std::string& action, int value);

  void removeMenuItems(const Fl_Menu_Item* baseItem);
  void insertMappingMenuItems(const Fl_Menu_Item* baseItem, int shortcutBase, Fl_Callback* cb);
  void insertComponentMenuItems(const Fl_Menu_Item* baseItem, int shortcutBase, Fl_Callback* cb);

  static void time_cb(Fl_Widget* w, void* v);
  static void zoom_cb(Fl_Widget* w, void* v);
  static void color_cb(Fl_Widget* w, void* v);
  static void component_cb(Fl_Widget* w, void* v);
  static void move_cb(Fl_Widget* w, void* v);
  static void setjump_cb(Fl_Widget* w, void* v);
  static void settings_cb(Fl_Widget* w, void* v);
  static void warnings_cb(Fl_Widget* w, void* v);
  static void gridView_cb(Fl_Widget* w, void* v);

  static void setDataView_cb(Fl_Widget* w, void* v) {
    auto view = dynamic_cast<MenuBarView*>(w);
    auto flag = *(int*)v;
    view->menuBarPresentation.switchDataView(flag);
  }

  static void setComponent_cb(Fl_Widget* w, void* v) {
    auto view = dynamic_cast<MenuBarView*>(w);
    auto flag = *(int*)v;
    view->menuBarPresentation.switchComponent(flag);
  }

  static void changeData_cb(Fl_Widget* w, void* /*v*/) {
    auto view = dynamic_cast<MenuBarView*>(w);
    view->menuBarPresentation.switchDataField();
  }

  static void viewMode_cb(Fl_Widget* w, void* v) {
    auto view = dynamic_cast<MenuBarView*>(w);
    auto flag = *(unsigned int*)v;
    view->dataPresentation.selectPresentation(flag);
  }

  static void grid_cb(Fl_Widget* w, void* v) {
    auto view = dynamic_cast<MenuBarView*>(w);
    auto flag = *(unsigned int*)v;
    view->menuBarPresentation.toggleGridState(flag);
  }

  static void blockgrid_cb(Fl_Widget* w, void* /*v*/) {
    auto view = dynamic_cast<MenuBarView*>(w);
    view->menuBarPresentation.toggleBlockLine();
  }

  static void reload_cb(Fl_Widget* w, void* /*v*/) {
    auto view = dynamic_cast<MenuBarView*>(w);
    view->gui.showStatusMessage("Load data...");
    view->menuBarPresentation.reloadData();
    view->gui.showStatusMessage("Data loaded");
  }

  static void exit_cb(Fl_Widget* /*w*/, void* /*v*/) {
    exit(0);
  }

  static void showNumber_cb(Fl_Widget* w, void* /*v*/) {
    auto view = dynamic_cast<MenuBarView*>(w);
    view->menuBarPresentation.toggleNumbers();
  }

  static void invert_cb(Fl_Widget* w, void* /*v*/) {
    auto view = dynamic_cast<MenuBarView*>(w);
    view->dataPresentation.invert();
  }

  static void reset_cb(Fl_Widget* w, void* /*v*/) {
    auto view = dynamic_cast<MenuBarView*>(w);
    view->menuBarPresentation.resetView();
  }

  static void help_cb(Fl_Widget* w, void* /*v*/) {
    auto view = dynamic_cast<MenuBarView*>(w);
    view->helpWindow->toggle();
  }

  static void unselect_cb(Fl_Widget* w, void* /*v*/) {
    auto view = dynamic_cast<MenuBarView*>(w);
    view->dataPresentation.resetSelection();
  }

#ifdef __APPLE__
  Fl_Menu_Item settings[2] = {
      {"Preferences...", FL_COMMAND + ',', settings_cb, this, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0}};
#endif

  MenuBarPresentation& menuBarPresentation;
  HelpWindow* helpWindow;
  GUISettings* settingsDialog;
  GUIWarning* rotationWarning;
  DataPresentation& dataPresentation;
  InfoPresentation& infoPresentation;
  GUIFrame& gui;

  std::string jumpMenuText = "Scale: 10";

  int rotationDefault{0};                        ///< Default action for rotation when control point is moved inside
  int keyModifierJump{FL_ALT};                   ///< Modifier key for jump.
  int keyModifierMaximum{FL_COMMAND};            ///< Modifier key for maximum jump.
  int keyModifierRotation{FL_ALT + FL_COMMAND};  ///< Modifier key for rotation.

  std::vector<std::string> componentNames; ///< Menu strings for components
};

#endif  // SRC_VIEW_MENUBARVIEW_H_
