/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_VIEW_COLORSCALEVIEW_H_
#define SRC_VIEW_COLORSCALEVIEW_H_

#include "presenter/colorscalepresentation.h"
#include "view/flimagebox.h"
#include <FL/Fl_Box.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Value_Input.H>
#include <FL/Fl_Value_Output.H>

class PresentationManager;

/**
 * The GUI Component for ColorscalePresentation that draws information on the Colorcale.
 *
 * It will show a vertical Bar of the currently loaded Colorscale and shows editable TextBoxes for min and max values.
 *
 * @ingroup view
 */
class ColorScaleView : public Fl_Group, public View {
public:
  ColorScaleView(int x, int y, int w, int h, PresentationManager& presentationManager);

  /**
   * Redraws this View and updates  all values
   */
  void update() override;

  /**
   * overwritten resize operation for the main-window
   *
   * @param x x Coordinate of the view
   * @param y y Coordinate of the view
   * @param w with of the view
   * @param h height of the view
   */
  void resize(int x, int y, int w, int h) override;

private:
  /**
   * handles callback for different widgets inside this class
   *
   * @param w Widget, that calls callback
   * @param v
   */
  static void callback(Fl_Widget* w, void* v);

  FLImageBox* box;
  ColorScalePresentation& colorScale;

  std::string name;

  Fl_Value_Input* csMin;
  Fl_Value_Input* csMax;

  Fl_Output* mapping;
};

#endif  // SRC_VIEW_COLORSCALEVIEW_H_
