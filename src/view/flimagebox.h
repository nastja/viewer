/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_VIEW_FLIMAGEBOX_H_
#define SRC_VIEW_FLIMAGEBOX_H_

#include "presenter/imagepresentation.h"
#include <FL/Fl_Box.H>
#include <FL/Fl_Image.H>
#include <FL/x.H>
#include <memory>

class FLImageBox : public Fl_Box {
public:
  FLImageBox(int x, int y, int w, int h, ImagePresentation& presentation);

  /**
  * Overriden operation to draw an image
  */
  void draw() override;

  void screenshot();

private:
  void saveScreenshot(unsigned char* img_data);

  ImagePresentation* presentation;
  bool createScreenshot;
};

#endif  // SRC_VIEW_FLIMAGEBOX_H_
