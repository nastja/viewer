/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef NASTJA_VIEWER_GUIWARNING_H
#define NASTJA_VIEWER_GUIWARNING_H

#include <FL/Fl.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Preferences.H>
#include <FL/Fl_Widget.H>
#include <FL/Fl_Window.H>
#include <iostream>

constexpr unsigned int NV_Cancel   = 0;
constexpr unsigned int NV_Button1  = 1 << 0;
constexpr unsigned int NV_Button2  = 1 << 1;
constexpr unsigned int NV_Remember = 1 << 4;

class GUIWarning : public Fl_Window {
public:
  explicit GUIWarning();

  int showChoice() {
    choice = 0;

    button1->take_focus();
    show();
    while (shown()) Fl::wait();

    if (choice == 0) return 0;

    choice += rememberSelection->value() ? NV_Remember : 0;
    rememberSelection->value(0);

    return choice;
  }

private:
  static void button1_cb(Fl_Widget* w, void* v);
  static void button2_cb(Fl_Widget* w, void* v);
  static void cancel_cb(Fl_Widget* w, void* v);

  void save_settings();

  Fl_Check_Button* rememberSelection;
  Fl_Button* button1;

  int choice = 0;
};

#endif  //NASTJA_VIEWER_GUIWARNING_H
