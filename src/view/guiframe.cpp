/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "guiframe.h"
#include "view/colorscaleview.h"
#include "view/helpwindow.h"
#include "view/infoview.h"
#include "view/menubarview.h"
#include "view/visualizationview.h"
#include <FL/Enumerations.H>
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/fl_draw.H>
#include <iostream>

constexpr int borderwidth    = 10;
constexpr int infoviewwidth  = 208;
constexpr int infoviewheight = 305;
constexpr int titlebarheight = 22;  // TODO: maybe only on mac?

#ifdef __APPLE__
constexpr int menuheight = 0;
#else
constexpr int menuheight = 20;
#endif

GUIFrame::GUIFrame(PresentationManager& presentationManager)
    : Fl_Window(Fl::w(), Fl::h(), "NAStJA viewer") {
#ifdef __APPLE__
  Fl_Mac_App_Menu::print = "";
#endif

  Fl::scheme("gtk+");

  int x, y, windowwidth, windowheight;

  Fl::screen_work_area(x, y, windowwidth, windowheight);
  windowheight -= titlebarheight;
  resize(x, y, windowwidth, windowheight);

  menuBarView = new MenuBarView(windowwidth, presentationManager);

  visualizationView = new VisualizationView(borderwidth, borderwidth + menuheight, windowwidth - infoviewwidth - 3 * borderwidth, windowheight - 2 * borderwidth - menuheight, presentationManager);

  info = new Fl_Group(windowwidth - infoviewwidth - 3 * borderwidth, borderwidth + menuheight, infoviewwidth, windowheight - 3 * borderwidth - menuheight);

  infoView = new InfoView(windowwidth - infoviewwidth - borderwidth, borderwidth + menuheight, infoviewwidth, infoviewheight, presentationManager);
  infoView->end();

  colorScaleView = new ColorScaleView(windowwidth - infoviewwidth - borderwidth, 2 * borderwidth + menuheight + infoviewheight, infoviewwidth, windowheight - 3 * borderwidth - menuheight - infoviewheight, presentationManager);
  colorScaleView->end();

  info->resizable(colorScaleView);
  info->end();
  resizable(info);

  status = new Fl_Box(FL_FLAT_BOX, 0, windowheight - statusheight, windowwidth - infoviewwidth - 2 * borderwidth, statusheight, statusMessage.c_str());
  status->align(FL_ALIGN_LEFT + FL_ALIGN_INSIDE);
  status->color(0xffffcc00);
  showStatusMessage("Loaded...");

  end();

  resizable(visualizationView);
  size_range(500, 650);

  uchar r;
  uchar g;
  uchar b;
  Fl::get_color(FL_BACKGROUND_COLOR, r, g, b);
  Fl::background(r, g, b);

  setTitle(infoView->getDataFieldName());
}

void GUIFrame::statustimer_cb(void* v) {
  auto gui = static_cast<GUIFrame*>(v);
  gui->hideStatusMessage();
}

void GUIFrame::showStatusMessage(const std::string& msg) {
  statusMessage = msg;

  int ww, hh;
  fl_measure(statusMessage.c_str(), ww, hh);
  int maxstatuswidth = w() - infoviewwidth - 2 * borderwidth;
  status->size(std::min(ww + 5, maxstatuswidth), statusheight);
  status->label(statusMessage.c_str());

  Fl::remove_timeout(statustimer_cb, this);  // remove overlapping timer
  Fl::add_timeout(2, statustimer_cb, this);

  status->show();
}

void GUIFrame::hideStatusMessage() {
  Fl::remove_timeout(statustimer_cb, this);
  status->hide();
}

int GUIFrame::handle(int event) {
  int localX;
  int localY;
  localX = Fl::event_x() - x();
  localY = Fl::event_y() - y();
  switch (event) {
    case FL_KEYBOARD:
      if (Fl::event_key() == FL_F + 11) {
        if (info->visible()) {
          info->hide();
          infoView->hide();
          colorScaleView->hide();

          visualizationView->size(w() - 3 * borderwidth, h() - titlebarheight - 2 * borderwidth - menuheight);
          return 1;
        } else {
          info->show();
          infoView->show();
          colorScaleView->show();

          visualizationView->size(w() - infoviewwidth - 3 * borderwidth, h() - titlebarheight - 2 * borderwidth - menuheight);
          return 1;
        }
      }
      return 0;
    case FL_PUSH:
    case FL_RELEASE:
    case FL_DRAG:
    case FL_MOVE:
      if (localX < visualizationView->w() && localY < visualizationView->h() && localY > -30) {
        return visualizationView->handle(event);
      }
    case FL_FOCUS:
    case FL_UNFOCUS:
    default:
      if (menuBarView->handle(event)) return 1;
      return Fl_Group::handle(event);
  }
}
