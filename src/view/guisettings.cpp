/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "guisettings.h"
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Preferences.H>
#include <FL/Fl_Return_Button.H>
#include <FL/fl_ask.H>

GUISettings::GUISettings()
    : Fl_Window(225, 160, "Preferences") {
  set_modal();

  Fl_Group* o = new Fl_Group(5, 20, 215, 85, "Modifier Keys");
  o->align(Fl_Align(FL_ALIGN_TOP_LEFT));
  o->box(FL_ENGRAVED_FRAME);

#ifdef __APPLE__
  auto text_shift   = new Fl_Box(7, 27, 25, 16, "\342\207\247");
  auto text_ctrl    = new Fl_Box(32, 27, 25, 16, "^");
  auto text_alt     = new Fl_Box(57, 27, 25, 16, "\342\214\245");
  auto text_command = new Fl_Box(82, 27, 25, 16, "\342\214\230");
  (void)text_shift;
  (void)text_ctrl;
  (void)text_alt;
  (void)text_command;
#else
  auto text_shift   = new Fl_Box(7, 27, 25, 16, "shift");
  auto text_ctrl    = new Fl_Box(32, 27, 25, 16, "meta");
  auto text_alt     = new Fl_Box(57, 27, 25, 16, "alt");
  auto text_command = new Fl_Box(82, 27, 25, 16, "ctrl");
  text_shift->labelsize(text_shift->labelsize() - 4);
  text_ctrl->labelsize(text_ctrl->labelsize() - 4);
  text_alt->labelsize(text_alt->labelsize() - 4);
  text_command->labelsize(text_command->labelsize() - 4);
#endif

  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 4; j++) {
      buttons[i][j] = new Fl_Check_Button(10 + j * 25, 45 + i * 20, 15, 15);
    }
  }

  auto text_jump = new Fl_Box(105, 45, 0, 16, "Jump");
  text_jump->align(FL_ALIGN_RIGHT);

  auto text_max = new Fl_Box(105, 65, 0, 16, "Maximum Jump");
  text_max->align(FL_ALIGN_RIGHT);

  auto text_rotate = new Fl_Box(105, 85, 0, 16, "Rotation");
  text_rotate->align(FL_ALIGN_RIGHT);

  o->end();

  auto button_reset_warning = new Fl_Button(5, 110, 215, 20, "Reset all warning dialogs");
  button_reset_warning->callback(warnings_cb, this);

  auto button_default = new Fl_Button(5, h() - 25, 65, 20, "Default");
  button_default->callback(default_cb, this);

  auto button_cancel = new Fl_Button(w() - 70 - 70, h() - 25, 65, 20, "Cancel");
  button_cancel->callback(cancel_cb, this);

  auto button_save = new Fl_Return_Button(w() - 70, h() - 25, 65, 20, "Save");
  button_save->callback(save_cb, this);
  end();
}

constexpr int modifierkeys[4] = {
    FL_SHIFT,
    FL_CONTROL,
    FL_ALT,
    FL_COMMAND,
};

/**
 * Sets the modifier status to the checkboxes.
 *
 * @param key       The key 0 for jump, 1 for maximum jump, and 2 for rotation.
 * @param modifier  The key modifiers.
 */
void GUISettings::setModifier(ModifyGroup key, int modifier) {
  for (int i = 0; i < 4; i++) {
    buttons[static_cast<int>(key)][i]->value((modifierkeys[i] & modifier) ? 1 : 0);
  }
}

/**
 * Gets the modifier key.
 *
 * @param key       The key 0 for jump, 1 for maximum jump, and 2 for rotation.
 *
 * @return The modifier keys.
 */
int GUISettings::getModifier(ModifyGroup key) const {
  int modifier = 0;
  for (int i = 0; i < 4; i++) {
    if (buttons[static_cast<int>(key)][i]->value()) {
      modifier |= modifierkeys[i];
    }
  }
  return modifier;
}

void GUISettings::default_cb(Fl_Widget* /*w*/, void* v) {
  auto* settings = static_cast<GUISettings*>(v);
  settings->setModifier(ModifyGroup::Jump, FL_ALT);
  settings->setModifier(ModifyGroup::Maximum, FL_COMMAND);
  settings->setModifier(ModifyGroup::Rotation, FL_ALT + FL_COMMAND);
}

void GUISettings::cancel_cb(Fl_Widget* /*w*/, void* v) {
  auto* settings = static_cast<GUISettings*>(v);
  settings->hide();
}

void GUISettings::save_cb(Fl_Widget* /*w*/, void* v) {
  auto* settings = static_cast<GUISettings*>(v);

  settings->keyModifierJump     = settings->getModifier(ModifyGroup::Jump);
  settings->keyModifierMaximum  = settings->getModifier(ModifyGroup::Maximum);
  settings->keyModifierRotation = settings->getModifier(ModifyGroup::Rotation);

  bool nonuniq = (settings->keyModifierJump == settings->keyModifierMaximum ||
                  settings->keyModifierJump == settings->keyModifierRotation ||
                  settings->keyModifierMaximum == settings->keyModifierRotation);

  bool nonset = (settings->keyModifierJump == 0 ||
                 settings->keyModifierMaximum == 0 ||
                 settings->keyModifierRotation == 0);

  if (nonuniq && nonset) {
    fl_alert("Jump, maximum jump and rotation must have at least one modifier key. The modifier keys must be pairwise different.");
  } else if (nonuniq) {
    fl_alert("The modifier keys for jump, maximum jump and rotation must be pairwise different.");
  } else if (nonset) {
    fl_alert("Jump, maximum jump and rotation must have at least one modifier key.");
  } else {
    settings->modified |= NV_ModifiedKeys;
    settings->saveModifierSettings();
    settings->callback();
    settings->hide();
  }
}

void GUISettings::saveModifierSettings() {
  Fl_Preferences prefs(Fl_Preferences::USER, "nastja", "viewer");
  Fl_Preferences prefKeys(prefs, "keys");
  prefKeys.set("ModifierJump", keyModifierJump);
  prefKeys.set("ModifierMaximum", keyModifierMaximum);
  prefKeys.set("ModifierRotation", keyModifierRotation);
}

void GUISettings::warnings_cb(Fl_Widget* /*w*/, void* v) {
  auto* settings = static_cast<GUISettings*>(v);
  Fl_Preferences prefs(Fl_Preferences::USER, "nastja", "viewer");
  prefs.deleteGroup("defaultAnswers");
  settings->modified |= NV_ModifiedWarnings;
}
