#include "guiwarning.h"
#include <FL/Fl_Box.H>
#include <FL/Fl_Preferences.H>
#include <FL/Fl_Return_Button.H>

GUIWarning::GUIWarning()
    : Fl_Window(365, 125, "Rotation warning") {
  set_modal();

  Fl_Box* icon = new Fl_Box(10, 10, 50, 50);
  icon->box(FL_THIN_UP_BOX);
  icon->labelfont(FL_TIMES_BOLD);
  icon->labelsize(34);
  icon->color(FL_WHITE);
  icon->labelcolor(FL_BLUE);
  icon->label("?");

  Fl_Box* o = new Fl_Box(70, 5, 290, 65, "Rotation cannot be performed when the control point is moved inside. How to proceed?");
  o->align(FL_ALIGN_WRAP | FL_ALIGN_LEFT | FL_ALIGN_INSIDE);

  rememberSelection = new Fl_Check_Button(5, 75, 15, 15);
  rememberSelection->label("Remember my &Selection");

  button1 = new Fl_Button(5, h() - 25, 135, 20, "&Zoom");
  button1->callback(button1_cb, this);

  auto button2 = new Fl_Button(5 + 135 + 5, h() - 25, 130, 20, "&Reset Control Point");
  button2->callback(button2_cb, this);

  auto buttonCancel = new Fl_Button(w() - 75, h() - 25, 70, 20, "Cancel");
  buttonCancel->shortcut(FL_Escape);
  buttonCancel->callback(cancel_cb, this);
  end();
}

void GUIWarning::button1_cb(Fl_Widget* /*w*/, void* v) {
  auto* warning   = static_cast<GUIWarning*>(v);
  warning->choice = NV_Button1;
  warning->hide();
}

void GUIWarning::button2_cb(Fl_Widget* /*w*/, void* v) {
  auto* warning   = static_cast<GUIWarning*>(v);
  warning->choice = NV_Button2;
  warning->hide();
}

void GUIWarning::cancel_cb(Fl_Widget* /*w*/, void* v) {
  auto* warning   = static_cast<GUIWarning*>(v);
  warning->choice = 0;
  warning->hide();
}
