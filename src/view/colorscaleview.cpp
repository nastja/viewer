/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "colorscaleview.h"
#include "presenter/presentationmanager.h"
#include <algorithm>
#include <iostream>

ColorScaleView::ColorScaleView(int x, int y, int w, int h, PresentationManager& presentationManager)
    : Fl_Group(x, y, w, h), colorScale(presentationManager.getColorScalePresentation()) {
  colorScale.setView(this);

  Fl_Group::box(FL_UP_BOX);

  mapping = new Fl_Output(x + w / 2 - 26, y + 5, w / 2 - 10, 16, "Mapping:");
  mapping->box(FL_FLAT_BOX);
  mapping->color(FL_BACKGROUND_COLOR);

  auto& currentName = colorScale.getCurrentName();
  mapping->value(currentName.c_str());

  csMax = new Fl_Value_Input(x + w / 2 - 26, y + 25, w / 2 - 10, 16, "Max:");
  csMax->callback(ColorScaleView::callback, this);
  csMin = new Fl_Value_Input(x + w / 2 - 26, y + h - 26, w / 2 - 10, 16, "Min:");
  csMin->callback(ColorScaleView::callback, this);

  box = new FLImageBox(x + w / 2 - 25, y + 45, 50, h - 77, colorScale);
  colorScale.setResolution(box->w(), box->h());

  resizable(box);
  end();

  update();
}

void ColorScaleView::update() {
  box->redraw();

  auto& currentName = colorScale.getCurrentName();
  mapping->value(currentName.c_str());

  auto max = colorScale.getMax();
  auto min = colorScale.getMin();
  csMax->value(max);
  csMin->value(min);
}

void ColorScaleView::callback(Fl_Widget* w, void* v) {
  auto view = static_cast<ColorScaleView*>(v);
  if (w == view->csMax) {
    view->colorScale.setMax(view->csMax->value());
  } else if (w == view->csMin) {
    view->colorScale.setMin(view->csMin->value());
  } else {
    throw std::runtime_error("Callback called from wrong widget.");
  }
}

void ColorScaleView::resize(int x, int y, int w, int h) {
  Fl_Group::resize(x, y, w, h);
  colorScale.setResolution(box->w(), box->h());
}
