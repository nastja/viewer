/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_VIEW_GUISETTINGS_H_
#define SRC_VIEW_GUISETTINGS_H_

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Window.H>
#include <vector>
#include "guiwarning.h"

class MenuBarView;

constexpr unsigned int NV_ModifiedWarnings = 1 << 0;
constexpr unsigned int NV_ModifiedKeys     = 1 << 1;

/**
 * The settings dialoge.
 *
 * @ingroup view
 */
class GUISettings : public Fl_Window {
public:
  explicit GUISettings();

  void setModifierJump(int modifier) { setModifier(ModifyGroup::Jump, modifier); }
  void setModifierMaximum(int modifier) { setModifier(ModifyGroup::Maximum, modifier); }
  void setModifierRotation(int modifier) { setModifier(ModifyGroup::Rotation, modifier); }

  int getModifierJump() const { return getModifier(ModifyGroup::Jump); }
  int getModifierMaximum() const { return getModifier(ModifyGroup::Maximum); }
  int getModifierRotation() const { return getModifier(ModifyGroup::Rotation); }

  void setCallback(Fl_Callback* cb, void* p) {
    callback_  = cb;
    user_data_ = p;
  }

  /**
   * Shows the settings.
   *
   * @return Non 0 when the settings are modified.
   */
  int showSettings() {
    modified = 0;

    show();
    while (shown()) Fl::wait();

    return modified;
  }

private:
  enum class ModifyGroup { Jump     = 0,
                           Maximum  = 1,
                           Rotation = 2 };

  void setModifier(ModifyGroup key, int modifier);
  int getModifier(ModifyGroup key) const;

  void saveModifierSettings();
  void callback() {
    if (callback_ != nullptr) (*callback_)((Fl_Widget*)user_data_, user_data_);
  }

  static void default_cb(Fl_Widget* w, void* v);
  static void cancel_cb(Fl_Widget* w, void* v);
  static void save_cb(Fl_Widget* w, void* v);
  static void warnings_cb(Fl_Widget* w, void* v);

  Fl_Check_Button* buttons[3][4];

  int keyModifierJump{FL_ALT};
  int keyModifierMaximum{FL_COMMAND};
  int keyModifierRotation{FL_ALT + FL_COMMAND};
  Fl_Callback* callback_ = nullptr;
  void* user_data_       = nullptr;

  int modified{0};
};

#endif  // SRC_VIEW_GUISETTINGS_H_
