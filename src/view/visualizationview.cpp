/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "visualizationview.h"
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <iostream>

VisualizationView::VisualizationView(unsigned int x, unsigned int y, unsigned int w, unsigned int h, PresentationManager& presentationManager)
    : Fl_Box(x, y, w, h), dataPresentation(presentationManager.getDataPresentation()) {
  box = new FLImageBox(x, y, w, h, dataPresentation);
  dataPresentation.setResolution(w, h);
  dataPresentation.setView(this);
  take_focus();
}

void VisualizationView::update() {
  box->redraw();
}

int VisualizationView::handle(int event) {
  unsigned int localX;
  unsigned int localY;
  localX = Fl::event_x() - x();
  localY = Fl::event_y() - y();

  switch (event) {
    case FL_PUSH:
      dataPresentation.clickAt(localX, localY, false);

      take_focus();
      return 1;
    case FL_RELEASE:
      dataPresentation.release();
      return 1;
    case FL_DRAG:
      dataPresentation.clickAt(localX, localY, true);
      return 1;
    case FL_MOVE:
      if (dataPresentation.isCursorNESW(localX, localY)) {
        Fl::first_window()->cursor(FL_CURSOR_NESW);
      } else if (dataPresentation.isCursorNWSE(localX, localY)) {
        Fl::first_window()->cursor(FL_CURSOR_NWSE);
      } else if (dataPresentation.isCursorNS(localX, localY)) {
        Fl::first_window()->cursor(FL_CURSOR_NS);
      } else if (dataPresentation.isCursorWE(localX, localY)) {
        Fl::first_window()->cursor(FL_CURSOR_WE);
      } else {
        Fl::first_window()->cursor(FL_CURSOR_ARROW);
      }
      dataPresentation.saveValue(localX, localY);
      return 1;
    case FL_FOCUS:
      return 1;
    case FL_UNFOCUS:
      return 1;
    default:
      return Fl_Box::handle(event);
  }
}

void VisualizationView::screenshot() {
  box->screenshot();
  update();
}

void VisualizationView::resize(int x, int y, int w, int h) {
  Fl_Box::resize(x, y, w, h);

  box->resize(x, y, w, h);

  dataPresentation.setResolution(w, h);
  update();
}
