/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_MODEL_COLORSCALE_H_
#define SRC_MODEL_COLORSCALE_H_

#include "event/modelelement.h"
#include "event/modellistener.h"
#include "model/colorbarset.h"
#include "model/colorscale.h"
#include <FL/Enumerations.H>
#include <initializer_list>
#include <memory>
#include <vector>

/**
 * ModelElement which contains data on the currently used colors.
 * It has a List of Color between which linear interpolation is used to calulate the color.
 *
 * This gradient is fixed between a minimal and a maximal value that the user can change.
 * All colors above the maximum and below the minimum will be set to the maximal and minimal color.
 *
 * @ingroup model
 */
class ContinuousColorscale : public Colorscale {
public:
  /**
   * Create a new Colorscale with default colors.
   */
  ContinuousColorscale(){};

  ContinuousColorscale(const std::string& name) : Colorscale(name) {}

  void switchColorbarSet(Direction dir) override;

  /**
   * calculates a Color using a value according to the used Colorscale.
   *
   * This is done by linear interpolating between the fixedPoints between min and max. Above and below that, the maximal and minimal colors will be used.
   * @param value the value to convert into a color
   * @return the calculated color for the value
   */
  Fl_Color getColor(double value) override;
  Fl_Color getColor(unsigned long value) override;
  Fl_Color getColor(float value) override;
  Fl_Color getColor(unsigned int value) override;

  /**
   * sets the minimal and maximum value for interpolation
   *
   * @param min  The minimum.
   * @param max  The maximum.
   */
  void setMinMax(double min, double max) override;

  void invertColorscale() override;
  bool isDiscrete() const override { return false; }

private:
  template <typename T>
  Fl_Color calculateColor(T value);

  std::array<std::unique_ptr<ColorbarSet>, 6> colorbarsets{
      std::make_unique<Cb_yrb>(),
      std::make_unique<Cb_jet>(),
      std::make_unique<Cb_flag>(),
      std::make_unique<Cb_darkflag>(),
      std::make_unique<Cb_hot>(),
      std::make_unique<Cb_gray>()};

  int colorbarset{0};
  bool invert{false};
};

#endif  // SRC_MODEL_COLORSCALE_H_
