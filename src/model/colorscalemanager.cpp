/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "colorscalemanager.h"
#include "model/continuouscolorscale.h"
#include "model/discretecolorscale.h"
#include "model/modelmanager.h"
#include <algorithm>

ColorscaleManager::ColorscaleManager(bool /*discrete*/)
    : ModelElement("ColorscaleManager"),
      ModelListener("ColorscaleManager") {
  colorscales.emplace_back(std::make_unique<ContinuousColorscale>());
  colorscales.emplace_back(std::make_unique<DiscreteColorscale>());
}

void ColorscaleManager::switchColorbarSet(Direction dir) {
  colorscales[colorscaleIndex]->switchColorbarSet(dir);
  modelManager->getPresentationMode().setRedraw(true);
  notify();
}

void ColorscaleManager::addModelManager(ModelManager* modelManager) {
  auto& dataManager = modelManager->getDataManager();
  dataManager.addListener(this);
  this->modelManager = modelManager;
  for (const auto& colorScale : colorscales) {
    // min and max must be set before ptr is set
    min = dataManager.getDataView().getMin();
    max = dataManager.getDataView().getMax();
    colorScale->setMinMax(min, max);
  }
  reset();
}

Fl_Color ColorscaleManager::getColor(double value) {
  return colorscales[colorscaleIndex]->getColor(value);
}

Fl_Color ColorscaleManager::getColor(unsigned long value) {
  return colorscales[colorscaleIndex]->getColor(value);
}

Fl_Color ColorscaleManager::getColor(float value) {
  return colorscales[colorscaleIndex]->getColor(value);
}

Fl_Color ColorscaleManager::getColor(unsigned int value) {
  return colorscales[colorscaleIndex]->getColor(value);
}

void ColorscaleManager::reset() {
  auto& dataManager = modelManager->getDataManager();

  min = dataManager.getMin();
  max = dataManager.getMax();
  colorscales[colorscaleIndex]->setMinMax(min, max);
  notify();
}

void ColorscaleManager::setMinMax(double min, double max) {
  this->min = min;
  this->max = max;
  colorscales[colorscaleIndex]->setMinMax(min, max);
  notify();
}

void ColorscaleManager::toggleDiscreteColor() {
  colorscaleIndex++;
  if (colorscaleIndex >= colorscales.size()) colorscaleIndex = 0;
  colorscales[colorscaleIndex]->setMinMax(min, max);
  notify();
}

void ColorscaleManager::setDiscrete(bool discrete) {
  for (int i = 0; i < colorscales.size(); i++) {
    if (colorscales[i]->isDiscrete() == discrete) {
      colorscaleIndex = i;
      break;
    }
  }
  colorscales[colorscaleIndex]->setMinMax(min, max);
  notify();
}

bool ColorscaleManager::isDiscrete() const {
  return colorscales[colorscaleIndex]->isDiscrete();
}

void ColorscaleManager::invertColorscale() {
  colorscales[colorscaleIndex]->invertColorscale();
  notify();
}
