/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_MODEL_COORDINATESYSTEM_H_
#define SRC_MODEL_COORDINATESYSTEM_H_

#include "event/modelelement.h"
#include "event/modellistener.h"
#include "util/direction.h"
#include "util/types.h"
#include <array>
#include <memory>

/**
 * This ModelElement contains the current rotational State of the Coordinate system displayed.
 *
 * Each view axis is represented with a base vector in the data coordinate system
 *
 * @ingroup model
 */
class CoordinateSystem : public ModelElement, public ModelListener {
public:
  /**
   * Create a identity coordinate system
   */
  CoordinateSystem() : ModelElement("CoordinateSystem"), ModelListener("CoordinateSystem") {}

  /**
   * Rotate the view by 90° using the direction enum.
   *
   * @param rotation a rotation action
   */
  void rotate(Direction direction);

  void onElementChange() override {}

  /**
   * rotate vector with current rotation from world-space to screen-space
   * @param vector the vector to rotate
   * @return the rotated vector
   */
  vec3 transformVector(vec3 const& vector);

  /**
   * rotate vector with current rotation from screen-space to world-space
   * @param vector the vector to rotate
   * @return the rotated vector
   */
  vec3 transformVectorInverse(vec3 const& vector);

  /** return to identity rotation */
  void reset() {
    transposed    = ivec3(1, 2, 3);
    rotationArray = {1, 2, 3};
    notify();
  }

private:
  ivec3 transposed = ivec3(1, 2, 3);
  //array gives a map from one standard base vector to another and the sign shows the direction
  std::array<int, 3> rotationArray = {1, 2, 3};
};

#endif  // SRC_MODEL_COORDINATESYSTEM_H_
