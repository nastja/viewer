/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_MODEL_ANCHORPOINTS_H_
#define SRC_MODEL_ANCHORPOINTS_H_

#include "util/types.h"
#include <cassert>
#include <util/boundingbox.h>

/**
 * This Class contains two coordinates which mark the corners of the Cuboid that should be drawn.
 *
 * @ingroup model
 */
class AnchorPoints {
public:
  /**
   * Creates a new instance with specified anchors.
   *
   * @param anchorA the first anchor
   * @param anchorA the second anchor
   */
  AnchorPoints(uvec3 anchorA, uvec3 anchorB)
      : anchors(anchorA, anchorB) {}

  /**
  * Creates a new instance with specified anchors.
  *
  * @param anchors the anchors
  */
  AnchorPoints(BoundingBox<unsigned int> anchors)
      : anchors(anchors) {}

  /**
   * returns the first anchor
   * @return the first anchor
   */
  uvec3 getAnchorA() const {
    return anchors[0];
  }

  /**
   * returns the second anchor
   * @return the second anchor
   */
  uvec3 getAnchorB() const {
    return anchors[1];
  }

  /**
   *
   * @return size of the zoomed in area
   */
  uvec3 getSize() const {
    return anchors.size() + uvec3(1, 1, 1);
  }

  /** transform Vector from World space to local space where anchorA is (0,0,0)
   * @return vector - anchorPointA
   */
  ivec3 transformVector(const ivec3& vector) const {
    assert(vector.x() >= (int)getAnchorA().x() && vector.y() >= (int)getAnchorA().y() && vector.z() >= (int)getAnchorA().z());
    assert(vector.x() <= (int)getAnchorB().x() && vector.y() <= (int)getAnchorB().y() && vector.z() <= (int)getAnchorB().z());

    return vector - getAnchorA();
  }

  /** transform Vector from local space (where anchorA is (0,0,0)) to world space
   * @return vector + anchorPointA
   */
  ivec3 transformVectorInverse(const ivec3& vector) const {
    assert(vector.x() >= 0 && vector.y() >= 0 && vector.z() >= 0);
    assert(vector.x() <= (int)getSize().x() && vector.y() <= (int)getSize().y() && vector.z() <= (int)getSize().z());  // <=: some calculation need one value more

    return vector + ivec3(getAnchorA().x(), getAnchorA().y(), getAnchorA().z());
  }

private:
  /**
   * The two coordinates
   */
  BoundingBox<unsigned int> anchors;
};

#endif  // SRC_MODEL_ANCHORPOINTS_H_
