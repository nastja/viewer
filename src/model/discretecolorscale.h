/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef NASTJA_VIEWER_DISCRETECOLORSCALE_H
#define NASTJA_VIEWER_DISCRETECOLORSCALE_H

#include "model/colorscale.h"
#include <cmath>
#include <limits>

class DiscreteColorscale : public Colorscale {
public:
  DiscreteColorscale() : Colorscale() {}

  DiscreteColorscale(const std::string& name) : Colorscale(name) {}

  Fl_Color getColor(double value) override {
    if (value != 0.0 && !std::isnormal(value)) return getSpecialColor(value);

    return calculateColor(static_cast<unsigned long>(value));
  }

  Fl_Color getColor(unsigned long value) override {
    if (value == std::numeric_limits<unsigned long>::max()) return getSpecialColor(value);

    return calculateColor(value);
  }

  Fl_Color getColor(float value) override {
    if (value != 0.0 && !std::isnormal(value)) return getSpecialColor(value);

    return calculateColor(static_cast<unsigned long>(value));
  }

  Fl_Color getColor(unsigned int value) override {
    if (value == std::numeric_limits<unsigned int>::max()) return getSpecialColor(value);

    return calculateColor(value);
  }

  static constexpr unsigned int s = 2654435769;

  /**
   * Fast calculation of a multiplicative hash.
   *
   * Using s as the "randomize" function.
   *
   * @param k  The input value.
   * @param i  Hash size in 2^i.
   *
   * @return The hash value.
   */
  unsigned int mult_hash(unsigned int k, unsigned int i) {
    unsigned int t = k * s;
    return t >> (32 - i);
  }

  Fl_Color calculateColor(unsigned int value) {
    unsigned int hash = mult_hash(value + shift, 15);  // Shift can be used to switch color.

    unsigned int h = (hash & 0x7FC0) >> 6;  // 9 bits for the hue
    unsigned int s = (hash & 0x3E) >> 1;    // 5 bits for saturation or value.
    unsigned int v = 0;
    if (hash & 0x1) {  // 1 bit for choose between saturation and value.
      std::swap(v, s);
    }

    return hsvtorgb(360.0 * h / 512.0, (255 - 4 * s) / 255.0, (255 - 4 * v) / 255.0);
  }

  template <typename T>
  Fl_Color getSpecialColor(T /*value*/) {
    return 0xaaaaaa00;
  }

  void setMinMax(double /*min*/, double /*max*/) override {}

  void switchColorbarSet(Direction dir) override;

  void invertColorscale() override {}

  bool isDiscrete() const override { return true; }

private:
  Fl_Color hsvtorgb(double h, double s, double v);

  unsigned int shift{0};
};

#endif  //NASTJA_VIEWER_DISCRETECOLORSCALE_H
