/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_MODEL_ANCHORPOINTSTACK_H_
#define SRC_MODEL_ANCHORPOINTSTACK_H_

#include "event/modelelement.h"
#include "event/modellistener.h"
#include "model/anchorpoints.h"
#include <memory>
#include <vector>

class ModelManager;

/**
 * ModelElement for the AnchorPoints.
 *
 * This is a Stack of AnchorPoints instances to enable zoom-undoing back to the whole view.
 *
 * @ingroup model
 */
class AnchorPointStack : public ModelElement {
public:
  /**
   * Create a new AnchorPointStack with one AnchorPoints instance which shows all of the data.
   */
  AnchorPointStack();

  /**
   * adds a modelManager and sets first anchorPoints
   * @param modelManager added modelManager
   */
  void addModelManager(ModelManager& modelManager);

  /**
   * push a new AnchorPoints intance to the stack
   * if new instance is not inside current space, do nothung
   *
   * @param anchors the new AnchorPoints
   */
  void push(AnchorPoints anchors);

  /**
   * pop the top AnchorPoints instance.
   * If the stack has only one element, nothing will happen
   */
  void pop();

  /**
   * returns the currently active AnchorPoints instance
   * @return the currently active AnchorPoints instance
   */
  const AnchorPoints& getAnchorPoints() {
    return anchorPoints.back();
  }

  /**
   *
   * @return the number of anchorPoints on the stack
   */
  unsigned long getAmountAnchorPoints() { return anchorPoints.size(); }

  void setSize(uvec3 size) {
    if (!anchorPoints.empty())
      anchorPoints[0] = AnchorPoints(uvec3(0, 0, 0), size - uvec3(1, 1, 1));
  }

private:
  std::vector<AnchorPoints> anchorPoints;
};

#endif  // SRC_MODEL_ANCHORPOINTSTACK_H_
