/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_MODEL_PRESENTATIONMODE_H_
#define SRC_MODEL_PRESENTATIONMODE_H_

#include "event/modelelement.h"
#include "event/modellistener.h"
#include "util/grid.h"
#include "util/linestyle.h"
#include <FL/Enumerations.H>
#include <array>

class ModelManager;

enum class Colors {
  border,    ///< Color of the border.
  blockLine, ///< Color of the block lines.
  grid,      ///< Color of the grid and gradient grid.
  selection,  ///< Color of the mouse selection.
};

/**
 * ModelElement which stores the mode for DataPresentation.
 *
 * This is done in the model so that everyone in the application has the ability to change settings.
 *
 * @ingroup model
 */
class PresentationMode : public ModelElement, ModelListener {
public:
  PresentationMode() : ModelElement("PresentationMode"), ModelListener("PresentationMode") {}

  /**
   * toggles the presentationMode between 1D and 3D
   */
  void selectPresentation(int presentation);

  /**
   * returns the presentation mode
   * @return true if 1D presentation
   */
  bool is1DPresentation() {
    return is1D;
  }

  bool is2DPresentation() {
    return is2D;
  }

  /**
   * toggles labels on or off
   */
  void toggleLabel();

  /**
   * toggles GridLines between two modes:
   *
   * - drawing grid only if gradients exists
   * - drawing grid regardless of gradients
   */
  void toggleGridLine(unsigned int flag = 0);

  void toggleBlockLine() {
    showBlockLine = (oneBlock ? false : !showBlockLine);
    notify();
  }

  bool isShowBlockLine() const { return showBlockLine; }

  /**
   * sets the LineStyle for 1D mode
   * @param lineStyle the new style
   */
  void setGraphLineStyle(LineStyle lineStyle);

  /**
   * Set the thickness of the line in 1D mode
   * @param thickness thickness of line in pixels
   */
  void setGraphLineThickness(int thickness) {
    this->thickness = thickness < 8 ? thickness : 8;
    notify();
  }

  /**
   * return whether labels should be drawn
   * @return true if labels shuld be drawn
   */
  bool hasLabels() const { return labels; }

  /**
   * return the mode of the grid
   * @return true if the grid should always be painted
   */
  Grid getGridState() const { return grid; }

  void setGridState(Grid grid) {
    this->grid = grid;
    redraw = true;
    notify();
  }

  /**
   * returns the line style for 1D mode
   * @return the line style
   */
  LineStyle getLineStyle() const { return lineStyle; }

  /**
   * returns the line thickness of the 1D mode
   * @return the line thickness in pixel
   */
  int getLineThickness() { return thickness; }

  /**
   * returns whether the axis is active
   * @param a the axis
   * @return the state
   */
  bool isActive(int a) const { return (a >= 0 && a < 3) ? activeAxis[a] : false; }

  /**
   * activates next axis
   */
  void activateNextAxis();

  /**
   * activates previous axis
   */
  void activatePreviousAxis();

  /**
   * changes the linestyle to the next linestyle
   */
  void nextLineStyle();

  /**
   * changes the linestyle to the previous linestyle
   */
  void previousLineStyle();

  bool hasSelection() {
    return selection;
  }

  bool getRedraw() { return redraw; }

  void setRedraw(bool redraw) {
    this->redraw = redraw;
    notify();
  }

  bool isOneBlock() {
    return oneBlock;
  }

  void toggleSelection();

  Fl_Color getColor(Colors color) {
    return colors[static_cast<unsigned int>(color)];
  }

  void setCurrentVortexValue(const std::string& value) {
    currentVortexValue = value;
    notify();
  }


  std::string getCurrentVortexValue() {
    return currentVortexValue;
  }

  void onElementChange() override;

  void addModelManager(ModelManager& modelManager);

private:
  std::string currentVortexValue{""};
  bool is1D{false};
  bool is2D{false};
  bool labels{true};
  bool selection{false};
  bool redraw{true};
  bool oneBlock{true};
  Grid grid{Grid::gradient};
  bool showBlockLine{false};  ///< show block lines
  LineStyle lineStyle{LineStyle::NORMAL};
  int thickness{1};
  std::array<bool, 3> activeAxis{true, false, false};

  std::array<Fl_Color, 4> colors {
    FL_BLACK, ///< border
    FL_BLACK,///< blockLine
    FL_DARK3, ///< grid
    fl_rgb_color(210, 105, 30), ///< selection
  }; ///< Lookup colors for the Color enum.
};

#endif  // SRC_MODEL_PRESENTATIONMODE_H_
