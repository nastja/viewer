/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_MODEL_INVERTMODE_H_
#define SRC_MODEL_INVERTMODE_H_

#include "event/modelelement.h"
#include "event/modellistener.h"

/**
 * ModelElement to store whether the view in 3D Mode should be inverted.
 *
 * When the cuboid is inverted, the top right corner will be cut off.
 *
 * This is a seperate ModelElement to enable ModelListeners to listen to changes of this value.
 *
 * @ingroup model
 */
class InvertMode : public ModelElement, public ModelListener {
public:
  InvertMode() : ModelElement("InvertMode"), ModelListener("InvertMode"), inverted(false) {}

  /**
   * toggle the inverted variable
   */
  void toggleInverted() {
    inverted = !inverted;
    notify();
  }

  /**
   * returns whether the cuboid should be inverted.
   * @return true if the cuboid is inverted
   */
  bool isInverted() {
    return inverted;
  }

  void onElementChange() override {}

private:
  bool inverted;
};

#endif  // SRC_MODEL_INVERTMODE_H_
