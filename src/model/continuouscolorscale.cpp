/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include "continuouscolorscale.h"
#include <FL/Fl.H>
#include <algorithm>
#include <array>
#include <assert.h>
#include <cmath>
#include <iostream>
#include <type_traits>

Fl_Color ContinuousColorscale::getColor(double value) {
  return calculateColor<double>(value);
}
Fl_Color ContinuousColorscale::getColor(unsigned long value) {
  return calculateColor<unsigned long>(value);
}
Fl_Color ContinuousColorscale::getColor(float value) {
  return calculateColor<float>(value);
}
Fl_Color ContinuousColorscale::getColor(unsigned int value) {
  return calculateColor<unsigned int>(value);
}

/**
 * Calculates the color. If the value is a particular value like nan or int max the special color is retuned else the value is scaled between 0 and 1.
 *
 * @param value  The value.
 *
 * @tparam T  The type of the value.
 *
 * @return The calculated color or the special color.
 */
template <typename T>
Fl_Color ContinuousColorscale::calculateColor(T value) {
  float v;
  if ((std::is_integral<T>::value && value == std::numeric_limits<T>::max()) ||
      (!std::is_integral<T>::value && value != 0.0 && !std::isnormal(value))) {
    return colorbarsets[colorbarset]->getSpecialColor(value);
  } else if (max == min) {
    v = 0.0;
  } else {
    // scales the value between 0 and 1
    v = (value - min) / (max - min);
    if (v < 0.0) v = 0.0;
    if (v > 1.0) v = 1.0;
  }

  if (invert) v = 1.0 - v;

  return colorbarsets[colorbarset]->getColor(v);
}

void ContinuousColorscale::setMinMax(double min, double max) {
  if (min > max) return;
  this->min = min;
  this->max = max;
  notify();
}

void ContinuousColorscale::switchColorbarSet(Direction dir) {
  switch (dir) {
    case Direction::forward:
      colorbarset++;
      break;
    case Direction::backward:
      colorbarset--;
      break;
    default:
      throw(std::runtime_error("Direction not implementet for switchColorbarSet"));
  }

  if (colorbarset >= colorbarsets.size()) colorbarset = 0;
  if (colorbarset < 0) colorbarset = colorbarsets.size() - 1;
}

void ContinuousColorscale::invertColorscale() {
  invert = !invert;
}
