/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "coordinatesystem.h"
#include "util/math.h"
#include <iostream>

void CoordinateSystem::rotate(Direction direction) {
  switch (direction) {
    case Direction::right:
      rotationArray = {rotationArray[2], rotationArray[1], -rotationArray[0]};
      break;
    case Direction::left:
      rotationArray = {-rotationArray[2], rotationArray[1], rotationArray[0]};
      break;
    case Direction::up:
      rotationArray = {rotationArray[0], rotationArray[2], -rotationArray[1]};
      break;
    case Direction::down:
      rotationArray = {rotationArray[0], -rotationArray[2], rotationArray[1]};
      break;
    case Direction::forward:
      rotationArray = {rotationArray[1], -rotationArray[0], rotationArray[2]};
      break;
    case Direction::backward:
      rotationArray = {-rotationArray[1], rotationArray[0], rotationArray[2]};
      break;
  }

  for (int i = 0; i < 3; i++) {
    transposed[abs(rotationArray[i]) - 1] = sgn(rotationArray[i]) * (i + 1);
  }

  notify();
}

vec3 CoordinateSystem::transformVector(vec3 const& vector) {
  return vec3(sgn(rotationArray[0]) * vector[abs(rotationArray[0]) - 1],
          sgn(rotationArray[1]) * vector[abs(rotationArray[1]) - 1],
          sgn(rotationArray[2]) * vector[abs(rotationArray[2]) - 1]);
}

vec3 CoordinateSystem::transformVectorInverse(vec3 const& vector) {
  return vec3(sgn(transposed[0]) * vector[abs(transposed[0]) - 1],
          sgn(transposed[1]) * vector[abs(transposed[1]) - 1],
          sgn(transposed[2]) * vector[abs(transposed[2]) - 1]);
}
