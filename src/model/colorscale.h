/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef NASTJA_VIEWER_COLORSCALE_H
#define NASTJA_VIEWER_COLORSCALE_H

#include "model/colorscalemanager.h"
#include "util/direction.h"
#include "util/types.h"
#include <FL/Enumerations.H>
#include <map>

class Colorscale : public ModelElement {
public:
  Colorscale() : ModelElement("Colorscale"), name("Color") {}

  Colorscale(const std::string& name) : ModelElement("Colorscale"), name(name) {}

  virtual Fl_Color getColor(double value)        = 0;
  virtual Fl_Color getColor(unsigned long value) = 0;
  virtual Fl_Color getColor(float value)         = 0;
  virtual Fl_Color getColor(unsigned int value)  = 0;

  double getMin() { return min; }

  double getMax() { return max; }

  virtual void setMinMax(double min, double max) = 0;

  virtual std::string getName() { return name; }

  virtual void switchColorbarSet(Direction dir) = 0;

  virtual void invertColorscale() = 0;

  virtual bool isDiscrete() const = 0;

protected:
  std::string name;

  double min;
  double max;
};

#endif  // NASTJA_VIEWER_COLORSCALE_H
