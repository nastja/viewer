/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#include "model/volumetric/volumetricdata.h"
#include "util/types.h"
#include <memory>
#include <string>

/**
 * IFileParser
 *
 * @ingroup model
 */
class IFileParser {
public:
  explicit IFileParser(){};
  virtual ~IFileParser() = default;

  virtual std::unique_ptr<VolumetricBlock<unsigned int>> getBlockUInt32()  = 0;
  virtual std::unique_ptr<VolumetricBlock<unsigned long>> getBlockUInt64() = 0;
  virtual std::unique_ptr<VolumetricBlock<float>> getBlockFloat32()        = 0;
  virtual std::unique_ptr<VolumetricBlock<double>> getBlockFloat64()       = 0;

  virtual vec3 getSpacing()                    = 0;
  virtual uvec3 getInputSize()                 = 0;
  virtual DataType getType()                   = 0;
  virtual int getLevel()                       = 0;
  virtual unsigned int getNumberOfComponents() = 0;
};
