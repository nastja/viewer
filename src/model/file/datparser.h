/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "model/file/ifileparser.h"
#include "model/volumetric/volumetricdata.h"
#include "util/types.h"
#include <fstream>
#include <iostream>
#include <string>

/**
 * Implementation of BlockParser to read a single asci .dat file.
 *
 * @ingroup model
 */
class DATParser : public IFileParser {
public:
  explicit DATParser(const std::string& vtiFile);

  std::unique_ptr<VolumetricBlock<unsigned int>> getBlockUInt32() override { throw std::runtime_error("getBlockUInt32 is not implemented."); }
  std::unique_ptr<VolumetricBlock<unsigned long>> getBlockUInt64() override { throw std::runtime_error("getBlockUInt64 is not implemented."); }
  std::unique_ptr<VolumetricBlock<float>> getBlockFloat32() override { return getBlockImpl<float>(); }
  std::unique_ptr<VolumetricBlock<double>> getBlockFloat64() override { throw std::runtime_error("getBlockFloat64 is not implemented."); }

  vec3 getSpacing() override { return vec3(1.0, 1.0, 1.0); }

  uvec3 getInputSize() override { return size; }

  DataType getType() override { return DataType::float32; }

  unsigned int getNumberOfComponents() override { return 1; }

  int getLevel() override { return 0; }

private:
  template <typename T>
  std::unique_ptr<VolumetricBlock<T>> getBlockImpl() {
    // read data
    std::unique_ptr<VolumetricBlock<T>> block = std::make_unique<VolumetricBlock<T>>(size);
    std::string line;
    size_t index = 0;
    while (std::getline(inFile, line)) {
      const char* pLine = line.c_str();

      char* pEnd = nullptr;
      for (float f = std::strtod(pLine, &pEnd); pLine != pEnd; f = std::strtod(pLine, &pEnd)) {
        block->setDataAt(index, f);
        index++;
        pLine = pEnd;
      }
    }

    inFile.close();

    return block;
  }

  uvec3 parseInputSize();

  std::ifstream inFile;
  uvec3 size;
};
