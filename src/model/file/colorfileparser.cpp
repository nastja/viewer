/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "colorfileparser.h"
#include <fstream>
#include <iostream>
#include <sys/stat.h>

ColorFileParser::ColorFileParser(const std::string& clrFile) {
  std::map<unsigned long, Fl_Color> clrs;
  struct stat results {};
  if (stat(&clrFile[0], &results) == 0) {
    auto size = results.st_size;

    std::string clrStr(size, ' ');
    std::ifstream file(clrFile, std::ios::in | std::ios::binary);
    file.read(&clrStr[0], size);
    file.close();

    int entrySize = 7;
    clrs.empty();
    for (unsigned long i = 0; i < size; i = i + entrySize) {
      unsigned long id = (clrStr[i + 0] << 24) + (clrStr[i + 1] << 16) + (clrStr[i + 2] << 8) + clrStr[i + 3];
      uchar r          = clrStr[i + 4];
      uchar g          = clrStr[i + 5];
      uchar b          = clrStr[i + 6];
      clrs.insert({id, fl_rgb_color(r, g, b)});
    }
    colors = std::make_unique<std::map<unsigned long, Fl_Color>>(clrs);

  } else {
    std::cerr << "Problems with reading ColorFile: " + clrFile << std::endl;
    std::exit(1);
  }
}
