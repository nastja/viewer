/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "model/file/ifileparser.h"
#include "model/volumetric/volumetricdata.h"
#include "util/types.h"
#include <fstream>
#include <rapidxml.hpp>
#include <string>
#include <vector>

/**
 * Implementation of BlockParser to read a single VTK .vti file.
 *
 * @ingroup model
 */
class VTIParser : public IFileParser {
public:
  explicit VTIParser(const std::string& vtiFile);

  std::unique_ptr<VolumetricBlock<unsigned int>> getBlockUInt32() override { return getBlockImpl<unsigned int>(); }
  std::unique_ptr<VolumetricBlock<unsigned long>> getBlockUInt64() override { return getBlockImpl<unsigned long>(); }
  std::unique_ptr<VolumetricBlock<float>> getBlockFloat32() override { return getBlockImpl<float>(); }
  std::unique_ptr<VolumetricBlock<double>> getBlockFloat64() override { return getBlockImpl<double>(); }

  vec3 getSpacing() override { return spacing; }

  uvec3 getInputSize() override { return size; }

  DataType getType() override { return type; }

  int getLevel() override { return level; }

  unsigned int getNumberOfComponents() override { return components; }

private:
  template <typename T>
  std::unique_ptr<VolumetricBlock<T>> getBlockImpl() {
    // skip until '_'
    char c;
    do {
      c = inFile.get();
    } while ( c != '_');

    // check data size
    unsigned long datasize;
    inFile.read(reinterpret_cast<char*>(&datasize), sizeof(unsigned long));
    if (datasize != sizeof(T) * size.x() * size.y() * size.z() * components) {
      throw std::runtime_error("Block size does not match data size.");
    }

    // read data
    std::unique_ptr<VolumetricBlock<T>> block = std::make_unique<VolumetricBlock<T>>(size, level, components);
    inFile.read(reinterpret_cast<char*>(block->data()), datasize);

    inFile.close();

    return block;
  }

  void parseXML();
  vec3 parseSpacing(const rapidxml::xml_document<>& xml);
  uvec3 parseInputSize(const rapidxml::xml_document<>& xml);
  DataType parseType(const rapidxml::xml_document<>& xml);
  int parseLevel(const rapidxml::xml_document<>& xml);
  unsigned int parseComponents(const rapidxml::xml_document<>& xml);
  bool checkFormat(const rapidxml::xml_document<>& xml);

  bool checkAttribute(rapidxml::xml_node<char>* node, const std::string& attribute, const std::string& value);
  bool hasAttribute(rapidxml::xml_node<char>* node, const std::string& attribute);
  rapidxml::xml_node<char>* getNode(const rapidxml::xml_document<>& xml, std::vector<std::string> path);
  bool hasValue(const rapidxml::xml_document<>& xml, std::vector<std::string> path);

  std::ifstream inFile;
  uvec3 size;
  vec3 spacing;
  DataType type;
  int level;
  unsigned int components;

  std::string filePath;
  rapidxml::xml_document<> xml;
};
