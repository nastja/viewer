/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "filemanager.h"
#include "factoryparser.h"
#include "util/math.h"
#include <iostream>
#include <regex>
#include <string>

namespace filesystem = std::filesystem;

/**
 * Read the file system recursively and insert useful files to the file structure. Directories not matching `[0-9]{3}` are skipped.
 */
void FileManager::read() {
  files.clear();
  blockGridSize.x() = 0;
  blockGridSize.y() = 0;
  blockGridSize.z() = 0;
  if(!std::filesystem::is_directory(root)) {
    std::filesystem::path path = root;
    blockGridSize = uvec3(1, 1, 1);

    std::regex reg("([a-z_]+)-([0-9]{5})$");  // field name - frame number
    std::smatch match;

    std::string filename = path.stem();
    std::regex_search(filename, match, reg);

    auto type = getFileTypeByExtension(path.extension());
    if (match.size() == 3) {
      insert(type, match[1], std::stoi(match.str(2)), path);
    }
  } else {
    for (auto iter = filesystem::recursive_directory_iterator(root); iter != filesystem::recursive_directory_iterator(); ++iter) {
      const auto& path = iter->path();

      if (filesystem::is_directory(path)) {
        std::regex reg("([0-9]{3})$");  // 000 - 999
        std::smatch match;

        std::string directoryName = path.string();
        std::regex_search(directoryName, match, reg);

        if (match.size() != 2) {  // skip directories not matching `[0-9]{3}`.
          iter.disable_recursion_pending();
          continue;
        }
      }

      auto type = getFileTypeByExtension(path.extension());
      if (type == FileType::ignore) continue;

      std::regex reg("([a-z_]+)-([0-9]{5})$");  // field name - frame number
      std::smatch match;

      std::string filename = path.stem();
      std::regex_search(filename, match, reg);

      if (match.size() == 3) {
        insert(type, match[1], std::stoi(match.str(2)), path);
      }
    }
  }
  readFieldsFromFiles();
  if(std::filesystem::is_directory(root)) {
    FactoryFileParser factory;
    // iterate over all data fields and find the maximum extension block.
    for (const auto& field : files[FileType::data]) {
      for (const auto& frame : field.second) {
        for (const auto& file : frame.second) {
          auto position   = getBlockPosition(file);
          auto extension  = getExtension(file);
          auto fileParser = factory.create(extension, file);

          auto level        = fileParser->getLevel();
          blockGridSize.x() = std::max(blockGridSize.x(), position.x() + pow2(level) - 1);
          blockGridSize.y() = std::max(blockGridSize.y(), position.y() + pow2(level) - 1);
          blockGridSize.z() = std::max(blockGridSize.z(), position.z() + pow2(level) - 1);
        }
      }
    }
    blockGridSize.x()++;
    blockGridSize.y()++;
    blockGridSize.z()++;
  } else {
    blockGridSize = uvec3(1, 1, 1);
    offset = getBlockPosition(root);
  }
}

/**
 * Gets the file type by the extension.
 *
 * @param extension  The extension.
 *
 * @return The file type.
 */
FileManager::FileType FileManager::getFileTypeByExtension(const std::string& extension) const {
  if (extension == ".vti" || extension == ".dat") {
    return FileType::data;
  } else if (extension == ".csv") {
    return FileType::mapping;
  } else {
    return FileType::ignore;
  }
}

/**
 * Read the field names from the file structure.
 */
void FileManager::readFieldsFromFiles() {
  fields.clear();
  for (const auto& field : files[FileType::data]) {
    fields.push_back(field.first);
  }
}

/**
 * Gets the extension of the first file for the given field name.
 *
 * @param field  The field name.
 *
 * @return The extension.
 */
std::string FileManager::getExtension(filesystem::path path) const {
  return path.extension();
}

/**
 * Gets the block position from a file path.
 *
 * @param file  The file.
 *
 * @return The vector of the block position.
 */
uvec3 FileManager::getBlockPosition(const std::string& file) const {
  uvec3 position;

  std::regex regex("([0-9]{3})/([0-9]{3})/([0-9]{3})/[a-z_]+-[0-9]{5}.vti");
  std::smatch match;

  if (std::regex_search(file, match, regex)) {
    position.x() = std::stoi(match.str(1));
    position.y() = std::stoi(match.str(2));
    position.z() = std::stoi(match.str(3));
  }

  return position;
}
