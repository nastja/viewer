/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "util/types.h"
#include <filesystem>
#include <map>
#include <string>
#include <vector>
#include <regex>

/**
 * Class for file manager.
 */
class FileManager {
public:
  explicit FileManager(const std::string& rootdir) : root(rootdir) {
    if (!std::filesystem::exists(root)) {
      throw(std::runtime_error("'" + std::string(root) + "' does not exist."));
    }
    if (!(std::filesystem::is_directory(root) || getFileTypeByExtension(root) != FileType::data)) {
      throw(std::runtime_error("'" + std::string(root) + "' is not a directory nor a vti file."));
    }
    read();
  };

  ~FileManager() {}

  void read();

  /**
   * Gets the names of the data fields.
   *
   * @return Vector of data field names.
   */
  const std::vector<std::string>& getFields() const { return fields; }

  /**
   * Gets the maximum frame.
   *
   * @param field  The field.
   *
   * @return The maximum frame.
   */
  unsigned int getMaximumFrame(const std::string& field) const {
    return files.at(FileType::data).at(field).rbegin()->first;
  }

  /**
   * Gets the block grid size.
   *
   * @return Vector with the block grid size.
   */
  uvec3 getBlockGridSize() const {
    return blockGridSize;
  }

  /**
   * Gets the data files for the given field and frame.
   *
   * @param field  The field.
   * @param frame  The frame.
   *
   * @return The vector of the data files.
   */
  const std::vector<std::string>& getDataFiles(const std::string& field, unsigned int frame) const {
    return files.at(FileType::data).at(field).at(frame);
  }

  /**
   * Determines if there are mapping files.
   *
   * @return True if it has mapping files, False otherwise.
   */
  bool hasMappingFile() const {
    return (files.count(FileType::mapping) > 0);
  }

  /**
   * Gets the mapping file path. That is the first element in the field map and the first element in the vector of files.
   *
   * @param frame  The frame.
   *
   * @return The mapping file path.
   */
  const std::string& getMappingFile(unsigned int frame) const {
    return files.at(FileType::mapping).begin()->second.at(frame)[0];
  }

  std::string getExtension(std::filesystem::path path) const;

  uvec3 getBlockPosition(const std::string& file) const;
  uvec3 getDataOffset() const {
    return offset;
  }

private:
  enum class FileType { ignore,
                        data,
                        mapping };  ///< File types: data files, csv mapping file or other ignored file types.

  /**
   * Insert a file path to the files data structure.
   *
   * @param type   The type.
   * @param field  The field.
   * @param frame  The frame.
   * @param file   The path to file from the root.
   */
  void insert(FileType type, const std::string& field, unsigned int frame, const std::string& file) {
    files[type][field][frame].push_back(file);
  }

  FileType getFileTypeByExtension(const std::string& extension) const;
  void readFieldsFromFiles();

  std::filesystem::path root;                                                           ///< The data path root.
  std::vector<std::string> fields;                                                                    ///< The field names.
  uvec3 blockGridSize;                                                                                ///< The number of blocks per dimension.
  uvec3 offset{0, 0, 0};                                                                     ///< offset of the data. Needed for single files.
  std::map<FileType, std::map<std::string, std::map<unsigned int, std::vector<std::string>>>> files;  ///< The files data structure: file type, field name, frame number, and vector of file paths.
};
