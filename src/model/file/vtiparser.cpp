/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "vtiparser.h"
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

VTIParser::VTIParser(const std::string& vtiFile) {
  filePath = vtiFile;
  inFile.open(vtiFile, std::ifstream::in | std::ifstream::binary);
  parseXML();
}

void VTIParser::parseXML() {
  std::string header;
  std::string line;
  while (std::getline(inFile, line) && line.find("<AppendedData") == std::string::npos) {
    header += line;
  }
  header += line;  // add <AppendedData line
  // modify file to be valid XML: close open tags.
  header += "</AppendedData></VTKFile>";

  rapidxml::xml_document<> xml;
  xml.parse<0>(&header[0]);

  checkFormat(xml);
  size       = parseInputSize(xml);
  spacing    = parseSpacing(xml);
  type       = parseType(xml);
  level      = parseLevel(xml);
  components = parseComponents(xml);
}

uvec3 VTIParser::parseInputSize(const rapidxml::xml_document<>& xml) {
  std::string text = getNode(xml, {"VTKFile", "ImageData"})->first_attribute("WholeExtent")->value();
  std::stringstream ss(text);

  unsigned int minX, maxX, minY, maxY, minZ, maxZ;
  ss >> minX >> maxX >> minY >> maxY >> minZ >> maxZ;

  return uvec3(maxX - minX, maxY - minY, maxZ - minZ);
}

vec3 VTIParser::parseSpacing(const rapidxml::xml_document<>& xml) {
  std::string text = getNode(xml, {"VTKFile", "ImageData"})->first_attribute("Spacing")->value();
  std::stringstream ss(text);

  float xSpacing, ySpacing, zSpacing;
  ss >> xSpacing >> ySpacing >> zSpacing;

  return vec3(xSpacing, ySpacing, zSpacing);
}

DataType VTIParser::parseType(const rapidxml::xml_document<>& xml) {
  rapidxml::xml_node<char>* node;
  if (hasValue(xml, {"VTKFile", "ImageData", "CellData"})) {
    node = getNode(xml, {"VTKFile", "ImageData", "CellData", "DataArray"});
  } else {
    node = getNode(xml, {"VTKFile", "ImageData", "Piece", "CellData", "DataArray"});
  }

  std::string dataType = node->first_attribute("type")->value();
  if (dataType == "Float32") {
    return DataType::float32;
  } else if (dataType == "Float64") {
    return DataType::float64;
  } else if (dataType == "UInt32") {
    return DataType::uint32;
  } else if (dataType == "UInt64") {
    return DataType::uint64;
  } else {
    throw std::runtime_error("Unknown data type.");
  }
}

int VTIParser::parseLevel(const rapidxml::xml_document<>& xml) {
  std::string level;
  rapidxml::xml_node<char>* node;
  node =  getNode(xml, {"VTKFile", "ImageData"});
  if (hasAttribute(node, {"Level"})) {
    level = node->first_attribute("Level")->value();
  } else {
    level = "0";
  }
  return std::stoi(level);
}

unsigned int VTIParser::parseComponents(const rapidxml::xml_document<>& xml) {
  rapidxml::xml_node<char>* node;
  if (hasValue(xml, {"VTKFile", "ImageData", "CellData"})) {
    node = getNode(xml, {"VTKFile", "ImageData", "CellData", "DataArray"});
  } else {
    node = getNode(xml, {"VTKFile", "ImageData", "Piece", "CellData", "DataArray"});
  }
  if (hasAttribute(node, "NumberOfComponents")) {
    return std::stoul(node->first_attribute("NumberOfComponents")->value());
  } else {
    return 1;
  }
}

/**
 * Checks for the correct supported VTK xml file format.
 *
 * @param xml  The xml.
 *
 * @return True if the we support the format.
 */
bool VTIParser::checkFormat(const rapidxml::xml_document<>& xml) {
  auto* vtkfile_node = xml.first_node("VTKFile");

  if (!checkAttribute(vtkfile_node, "type", "ImageData")) throw std::runtime_error("Only VTKFile type 'ImageData' is supported.");
  if (!checkAttribute(vtkfile_node, "byte_order", "LittleEndian")) throw std::runtime_error("Only byte_order 'LittleEndian' is supported.");
  if (!checkAttribute(vtkfile_node, "header_type", "UInt64")) throw std::runtime_error("Only header_type 'UInt64' is supported.");
  if (hasAttribute(vtkfile_node, "compressor")) throw std::runtime_error("Compressors are not supported.");

  rapidxml::xml_node<char>* dataarray_node;
  if (hasValue(xml, {"VTKFile", "ImageData", "CellData"})) {
    dataarray_node = getNode(xml, {"VTKFile", "ImageData", "CellData", "DataArray"});
  } else {
    dataarray_node = getNode(xml, {"VTKFile", "ImageData", "Piece", "CellData", "DataArray"});
  }

  if (!checkAttribute(dataarray_node, "format", "appended")) throw std::runtime_error("Only DataArray format 'appended' is supported.");

  auto* appended_node = getNode(xml, {"VTKFile", "AppendedData"});

  if (!checkAttribute(appended_node, "encoding", "raw")) throw std::runtime_error("Only AppendedData encoding 'raw' is supported.");

  return true;
}

/**
 * Check if a node has an attribute and if this attribute has the given value.
 *
 * @param node       The node.
 * @param attribute  The attribute.
 * @param value      The value.
 *
 * @return True if there is an attribute with the value, false otherwise.
 */
bool VTIParser::checkAttribute(rapidxml::xml_node<char>* node, const std::string& attribute, const std::string& value) {
  auto* type = node->first_attribute(attribute.c_str());

  if (type == nullptr) return false;

  std::string attrValue = type->value();
  return attrValue == value;
}

/**
 * Determines if a node has an attribute.
 *
 * @param node       The node.
 * @param attribute  The attribute.
 *
 * @return True if it has that attribute, false otherwise.
 */
bool VTIParser::hasAttribute(rapidxml::xml_node<char>* node, const std::string& attribute) {
  auto* type = node->first_attribute(attribute.c_str());

  return !(type == nullptr);
}

/**
 * Gets the node.
 *
 * @param xml   The xml.
 * @param path  The path.
 *
 * @return The node.
 */
rapidxml::xml_node<char>* VTIParser::getNode(const rapidxml::xml_document<>& xml, std::vector<std::string> path) {
  rapidxml::xml_node<char> *node{nullptr};
  for (auto nodeName : path) {
    if(*path.begin() == nodeName) {
      node = xml.first_node(nodeName.c_str());
    } else {
      node = node->first_node(nodeName.c_str());
    }
    if (node == nullptr) {
      throw std::runtime_error("Couldn't find \"" + nodeName + "\" in the header of " + filePath);
    }
  }
  return node;
}

/**
 * Determines if a given path exists.
 *
 * @param xml   The xml.
 * @param path  The path.
 *
 * @return True if it exists, false otherwise.
 */
bool VTIParser::hasValue(const rapidxml::xml_document<>& xml, std::vector<std::string> path) {
  rapidxml::xml_node<char> *node{nullptr};
  for (const auto& nodeName : path) {
    if(*path.begin() == nodeName) {
      node = xml.first_node(nodeName.c_str());
    } else {
      node = node->first_node(nodeName.c_str());
    }
    if (node == nullptr) {
      return false;
    }
  }
  return true;
}
