/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_MODEL_COLORBARTABLE_H_
#define SRC_MODEL_COLORBARTABLE_H_

#include <FL/Enumerations.H>
#include <cmath>
#include <gsl/span>

struct ColorbarSet {
  virtual ~ColorbarSet() = default;

  virtual Fl_Color getColor(float value) const = 0;

  template <typename T>
  Fl_Color getSpecialColor(T /*value*/) {
    return 0xaaaaaa00;
  }
};

static Fl_Color getColorFromTable(gsl::span<const Fl_Color> table, float value) {
  auto N = table.size();
  value *= (N - 1);
  int intervall = floor(value);                            // round up to lower intervall
  if (intervall + 1 >= N) return table[table.size() - 1];  // for special case of t=1 ensure that no values larger than N-1 will be read from table
  value -= intervall;
  return fl_color_average(table[intervall + 1], table[intervall], value);
}

struct Cb_yrb : public ColorbarSet {
  const Fl_Color colormap[3] = {
      0xffff0000,  // yellow
      0xff000000,  // red
      0x0000ff00   // blue
  };

  Fl_Color getColor(float value) const final override {
    return getColorFromTable(colormap, value);
  }
};

struct Cb_jet : public ColorbarSet {
  const Fl_Color colormap[8] = {
      0x00008b00,  // dark-blue
      0x0000ff00,  // blue
      0x00ffff00,  // cyan
      0x00ff0000,  // green
      0xffff0000,  // yellow
      0xff000000,  // red
      0x8b000000,  // dark-red
      0x00000000   // black
  };

  Fl_Color getColor(float value) const final override {
    return getColorFromTable(colormap, value);
  }
};

struct Cb_darkflag : public ColorbarSet {
  const Fl_Color colormap[5] = {
      0x00008b00,  // dark-blue
      0x0000ff00,  // blue
      0xffffff00,  // white
      0xff000000,  // red
      0x8b000000   // dark-red
  };

  Fl_Color getColor(float value) const final override {
    return getColorFromTable(colormap, value);
  }
};

struct Cb_flag : public ColorbarSet {
  const Fl_Color colormap[3] = {
      0x0000ff00,  // blue
      0xffffff00,  // white
      0xff000000   // red
  };

  Fl_Color getColor(float value) const final override {
    return getColorFromTable(colormap, value);
  }
};

struct Cb_hot : public ColorbarSet {
  const Fl_Color colormap[4] = {
      0x0a000000,  // black
      0xff000000,  // red
      0xffff0000,  // yellow
      0xffffff00   // white
  };

  Fl_Color getColor(float value) const final override {
    return getColorFromTable(colormap, value);
  }
};

struct Cb_gray : public ColorbarSet {
  const Fl_Color colormap[2] = {
      0x00000000,  // black
      0xffffff00   // white
  };

  Fl_Color getColor(float value) const final override {
    return getColorFromTable(colormap, value);
  }
};

#endif  // SRC_MODEL_COLORBARTABLE_H_
