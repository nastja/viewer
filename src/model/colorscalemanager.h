/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef NASTJA_VIEWER_COLORSCALEMANAGER_H
#define NASTJA_VIEWER_COLORSCALEMANAGER_H

#include "event/modelelement.h"
#include "event/modellistener.h"
#include "model/colorscale.h"
#include "util/direction.h"
#include <FL/Enumerations.H>
#include <set>

class Colorscale;
class ModelManager;

class ColorscaleManager : public ModelElement, public ModelListener {
public:
  explicit ColorscaleManager(bool discrete);

  double getMin() const { return min; }
  double getMax() const { return max; }

  void setMinMax(double min, double max);

  Fl_Color getColor(double value);
  Fl_Color getColor(float value);
  Fl_Color getColor(unsigned long value);
  Fl_Color getColor(unsigned int value);

  void switchColorbarSet(Direction dir);

  void addModelManager(ModelManager* modelManager);

  void reset();

  void toggleDiscreteColor();
  bool isDiscrete() const;
  void setDiscrete(bool discrete);

  void invertColorscale();

  void onElementChange() override {
    reset();
  }

private:
  unsigned int colorscaleIndex = 0;
  std::vector<std::unique_ptr<Colorscale>> colorscales;
  ModelManager* modelManager;
  double max, min;
};

#endif  //NASTJA_VIEWER_COLORSCALEMANAGER_H
