/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "presentationmode.h"
#include "model/anchorpointstack.h"
#include "model/coordinatesystem.h"
#include "model/modelmanager.h"

/**
 * toggles the presentationMode between 1D and 3D
 */
void PresentationMode::selectPresentation(int presentation) {
  if (presentation == 1) {
    is1D = true;
  } else {
    is1D = false;
    is2D = presentation == 2;
  }
  redraw    = true;
  selection = false;
  notify();
}

/**
 * toggles labels on or off
 */
void PresentationMode::toggleLabel() {
  labels = !labels;
  notify();
}

void PresentationMode::toggleGridLine(unsigned int flag /*= 0*/) {
  if (flag == 0) {
    flag = static_cast<unsigned int>(grid) + 1;
  }

  if (flag > static_cast<unsigned int>(Grid::on)) {
    flag = 1;
  }
  grid = static_cast<Grid>(flag);

  redraw = true;
  notify();
}

void PresentationMode::setGraphLineStyle(LineStyle setlineStyle) {
  lineStyle = setlineStyle;
  notify();
}

void PresentationMode::previousLineStyle() {
  switch (lineStyle) {
    case NORMAL:
      lineStyle = DASHED;
      break;
    case DOTTED:
      lineStyle = NORMAL;
      break;
    case DASHED:
      lineStyle = DOTTED;
      break;
  }
  notify();
}

void PresentationMode::nextLineStyle() {
  switch (lineStyle) {
    case NORMAL:
      lineStyle = DOTTED;
      break;
    case DOTTED:
      lineStyle = DASHED;
      break;
    case DASHED:
      lineStyle = NORMAL;
      break;
  }
  notify();
}

void PresentationMode::activateNextAxis() {
  activeAxis[0] = activeAxis[2];
  activeAxis[2] = activeAxis[1];
  activeAxis[1] = !(activeAxis[0] || activeAxis[2]);
  notify();
}

void PresentationMode::activatePreviousAxis() {
  activeAxis[0] = activeAxis[1];
  activeAxis[1] = activeAxis[2];
  activeAxis[2] = !(activeAxis[0] || activeAxis[1]);
  notify();
}

void PresentationMode::toggleSelection() {
  selection = !selection;
}

void PresentationMode::onElementChange() {
  redraw    = true;
  selection = false;
}

void PresentationMode::addModelManager(ModelManager& modelManager) {
  modelManager.getCoordinateSystem().addListener(this);
  modelManager.getAnchorPointStack().addListener(this);

  auto blockDimensions = modelManager.getDataManager().getRotatedView().getBlockDimensions();
  auto size            = modelManager.getDataManager().getRotatedView().getDataSize();
  auto origin          = modelManager.getDataManager().getRotatedView().transformFromScreenSpace(0, 0, 0);
  for (int i = 0; i < 3; i++) {
    int num = std::ceil((size[i] + origin[i] % blockDimensions[i]) / blockDimensions[i]);
    if (num > 1) oneBlock = false;
  }
}
