/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_MODEL_MODELMANAGER_H_
#define SRC_MODEL_MODELMANAGER_H_

#include "model/anchorpointstack.h"
#include "model/controlpoint.h"
#include "model/coordinatesystem.h"
#include "model/invertmode.h"
#include "model/presentationmode.h"
#include "model/volumetric/datamanager.h"
#include "commandlineparser.h"
#include <memory>
#include <stdexcept>

class CommandLineParser;
class ColorscaleManager;

/**
 * @defgroup model
 *
 * This package contains the current state of the program and the data to display.
 * ModelManager is a public Facade for interfacing with the individual ModelElement objects. Every ModelElement is an Observable and can be observed using ModelListener.
*/

/**
 * This is the Facade to the whole model-package.
 *
 * With Getters the individual ModelElements can be accessed.
 *
 * @ingroup model
 */
class ModelManager {
public:
  /**
   * Creates a ModelManager.
   *
   * Volumetric View should be created outside because of attached files.
   *
   * @param commandLineParser  The command line parser.
   * @param discrete           The discrete.
   */
  ModelManager(CommandLineParser& commandLineParser, bool discrete = false);

  void setupModelElements();

  DataManager& getDataManager() { return dataManager; }
  PresentationMode& getPresentationMode() { return presentationMode; }
  ControlPoint& getControlPoint() { return controlPoint; }
  CoordinateSystem& getCoordinateSystem() { return coordinateSystem; }
  InvertMode& getInvertMode() { return invertMode; }
  AnchorPointStack& getAnchorPointStack() { return anchorPointStack; }
  ColorscaleManager& getColorscaleManager() { return colorscaleManager; }

  void switchDataField();

  void reset();
  void reload();

  void zoomIn(const BoundingBox<unsigned int>& box);
  void zoomOut();

private:
  DataManager dataManager;
  CoordinateSystem coordinateSystem;
  ControlPoint controlPoint;
  InvertMode invertMode;
  AnchorPointStack anchorPointStack;
  ColorscaleManager colorscaleManager;
  PresentationMode presentationMode;
};

#endif  // SRC_MODEL_MODELMANAGER_H_
