/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_MODEL_CONTROLPOINT_H_
#define SRC_MODEL_CONTROLPOINT_H_

#include "event/modelelement.h"
#include "event/modellistener.h"
#include "util/direction.h"
#include "util/types.h"

class ModelManager;

/**
 * ModelElement for the ControlPoint.
 *
 * It contains the coordinate controlPoint and a jumpScale for bigger jumps.
 *
 * @ingroup model
 */
class ControlPoint : public ModelElement, public ModelListener {
public:
  /**
   * Create a ControlPoint with the specified coordinate
   *
   * @param controlPoint the initial controlPoint
   */
  ControlPoint();

  void addModelManager(ModelManager& modelManager);

  /**
   * returns the current controlPoint in world coordinates
   * @return the current controlPoint
   */
  uvec3 getControlPoint() {
    return controlPoint;
  }

  /**
   * sets the controlPoint in world coordinates
   *
   * @param value the new controlPoint coordinate
   */
  void setControlPoint(ivec3 value);

  /**
   * return the jumpScale
   * @return the jumpScale
   */
  int getJumpScale() {
    return jumpScale;
  }

  /**
   * Sets the jumpScale. values below 1 will be clamped to 1
   *
   * @param scale the new jumpScale
   */
  void setJumpScale(int scale) {
    jumpScale = scale >= 1 ? scale : 1;
    notify();
  }

  /** resets the ControlPoint to the top right corner */
  void reset();

  /**
   * moves the controlpoint in one direction
   * @param direction moved to direction
   */
  void move(Direction direction);

  /**
   * jumps the controlpoint in one direction according to the jumpscale
   * @param direction
   */
  void jump(Direction direction);

  void moveMax(Direction dir);

  void onElementChange() override;

private:
  /**
   * The ControlPoint coordinate. In 3D-Mode this is the last Cell to be drawn.
   */
  uvec3 controlPoint;
  /**
   * this value is used for bigger jumps with the controlPoint
   */
  int jumpScale;

  ModelManager* modelManager;
};

#endif  // SRC_MODEL_CONTROLPOINT_H_
