/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "controlpoint.h"
#include "model/anchorpointstack.h"
#include "model/coordinatesystem.h"
#include "model/modelmanager.h"
#include "util/math.h"
#include <iostream>

ControlPoint::ControlPoint()
    : ModelElement("ControlPoint"), ModelListener("ControlPoint"), jumpScale(10) {}

void ControlPoint::addModelManager(ModelManager& modelManager) {
  this->modelManager = &modelManager;

  // register event so that ControlPoint can reset when e.g. rotation changes
  if (this->modelManager) {
    modelManager.getCoordinateSystem().addListener(this);
    modelManager.getAnchorPointStack().addListener(this);
  } else {
    throw std::runtime_error("ModelManager pointer expired!");
  }

  reset();
}

void ControlPoint::setControlPoint(ivec3 value) {
  const AnchorPoints& points = modelManager->getAnchorPointStack().getAnchorPoints();
  ivec3 min                  = points.getAnchorA();
  ivec3 max                  = points.getAnchorB();

  // clamp value to anchor points range
  controlPoint = uvec3(static_cast<unsigned int>(clamp(value.x(), min.x(), max.x())),
                       static_cast<unsigned int>(clamp(value.y(), min.y(), max.y())),
                       static_cast<unsigned int>(clamp(value.z(), min.z(), max.z())));

  if (modelManager->getInvertMode().isInverted()) {
    controlPoint.x() += (controlPoint.x() == min.x());
    controlPoint.y() += (controlPoint.y() == min.y());
    controlPoint.z() += (controlPoint.z() == min.z());
  }

  notify();
}

void ControlPoint::reset() {
  if (modelManager) {
    auto rotatedView    = modelManager->getDataManager().getRotatedView();
    auto screenSpacePos = rotatedView.getDataSize() - ivec3(1, 1, 1);
    setControlPoint(rotatedView.transformFromScreenSpace(screenSpacePos));
  } else {
    throw std::runtime_error("ModelManager pointer expired!");
  }
}

ivec3 getMoveVector(Direction direction) {
  switch (direction) {
    case Direction::up:
      return ivec3{0, 1, 0};
    case Direction::down:
      return ivec3{0, -1, 0};
    case Direction::left:
      return ivec3{-1, 0, 0};
    case Direction::right:
      return ivec3{1, 0, 0};
    case Direction::forward:
      return ivec3{0, 0, 1};
    case Direction::backward:
      return ivec3{0, 0, -1};
  }
  return ivec3{};
}

void ControlPoint::move(Direction direction) {
  ivec3 dir = getMoveVector(direction);
  setControlPoint(controlPoint + modelManager->getCoordinateSystem().transformVectorInverse(dir));
  modelManager->getPresentationMode().setRedraw(true);
}

void ControlPoint::jump(Direction direction) {
  ivec3 dir = getMoveVector(direction) * jumpScale;
  setControlPoint(controlPoint + modelManager->getCoordinateSystem().transformVectorInverse(dir));
}

void ControlPoint::moveMax(Direction direction) {
  auto size = modelManager->getDataManager().getDataView().getDataSize();
  ivec3 dir = getMoveVector(direction);
  dir.x() *= size.x();
  dir.y() *= size.y();
  dir.z() *= size.z();
  setControlPoint(controlPoint + modelManager->getCoordinateSystem().transformVectorInverse(dir));
}

void ControlPoint::onElementChange() {
  reset();
}
