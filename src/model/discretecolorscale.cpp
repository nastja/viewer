/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "discretecolorscale.h"

/**
 * Converts h, s, v to a rgb color.
 *
 * @param h  The hue value [0, 360].
 * @param s  The saturation [0, 1].
 * @param v  The value [0, 1].
 *
 * @return The color.
 */
Fl_Color DiscreteColorscale::hsvtorgb(double h, double s, double v) {
  h /= 60.0;
  int i    = h;
  double f = h - i;
  v *= 255.0;
  double p = v * (1.0 - s);
  double q = v * (1.0 - s * f);
  double t = v * (1.0 - s * (1.0 - f));

  switch (i) {
    case 0:
      return fl_rgb_color(v, t, p);
    case 1:
      return fl_rgb_color(q, v, p);
    case 2:
      return fl_rgb_color(p, v, t);
    case 3:
      return fl_rgb_color(p, q, v);
    case 4:
      return fl_rgb_color(t, p, v);
    case 5:
    default:
      return fl_rgb_color(v, p, q);
  }
}

void DiscreteColorscale::switchColorbarSet(Direction dir) {
  switch (dir) {
    case Direction::forward:
      shift++;
      break;
    case Direction::backward:
      shift--;
      break;
    default:
      throw(std::runtime_error("Direction not implementet for switchColorbarSet."));
  }
}
