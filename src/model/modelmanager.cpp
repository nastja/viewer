/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "modelmanager.h"
#include "model/anchorpointstack.h"
#include "model/continuouscolorscale.h"
#include "model/controlpoint.h"
#include "model/coordinatesystem.h"
#include "util/boundingbox.h"

ModelManager::ModelManager(CommandLineParser& commandLineParser, bool discrete)
    : dataManager(commandLineParser.frame, commandLineParser.path),
      colorscaleManager(discrete) {}

void ModelManager::setupModelElements() {
  // this order is important, a wrong order may lead to wrong states
  dataManager.setup(*this);
  anchorPointStack.addModelManager(*this);
  controlPoint.addModelManager(*this);
  presentationMode.addModelManager(*this);
  presentationMode.selectPresentation(3 - dataManager.is2D());
  colorscaleManager.addModelManager(this);

}

void ModelManager::reset() {
  unsigned long amountAnchorPoints = getAnchorPointStack().getAmountAnchorPoints();
  for (unsigned int i = 0; i < amountAnchorPoints; ++i) {
    getAnchorPointStack().pop();
  }

  getCoordinateSystem().reset();

  getControlPoint().reset();

  bool inverted = getInvertMode().isInverted();
  if (inverted) {
    getInvertMode().toggleInverted();
  }

  presentationMode.setGraphLineThickness(1);
  presentationMode.setGraphLineStyle(LineStyle::NORMAL);
  presentationMode.setGridState(Grid::gradient);
  bool labels = presentationMode.hasLabels();
  if (!labels) {
    presentationMode.toggleLabel();
  }
  bool view = presentationMode.is1DPresentation();
  if (view) {
    presentationMode.selectPresentation(1);
  }
}

void ModelManager::reload() {
  auto oldSize = getDataManager().getDataView().getDataSize();
  getDataManager().reload();
  auto newSize = getDataManager().getDataView().getDataSize();
  getAnchorPointStack().setSize(newSize);
  if (newSize.x() < oldSize.x() || newSize.y() < oldSize.y() || newSize.z() < oldSize.z()) {
    reset();
  }
}

void ModelManager::switchDataField() {
  dataManager.switchDataField();
  reset();
}

void ModelManager::zoomIn(const BoundingBox<unsigned int>& box) {
  auto selA = box[0];
  auto selB = box[1];
  auto view = dataManager.getRotatedView();

  // convert selection to world space
  auto anchorA = uvec3(selA.x(), selA.y(), selA.z() == 0 ? selA.z() : selA.z() - 1);
  auto anchorB = uvec3(selB.x(), selB.y(), selB.z() == 0 ? selB.z() : selB.z() - 1);

  // ensure anchorA is smaller than anchorB
  // the + 1 is needed only for selection because selection is saved differently
  BoundingBox<unsigned int> anchors(anchorA, anchorB);

  if (anchors[0].z() == anchors[1].z() + 1) {
    anchors[0].z() = view.transformToScreenSpace(anchorPointStack.getAnchorPoints().getAnchorA()).z();
    anchors[1].z() = view.transformToScreenSpace(anchorPointStack.getAnchorPoints().getAnchorB()).z();
  }

  anchors[0] = view.transformFromScreenSpace(anchors[0]);
  anchors[1] = view.transformFromScreenSpace(anchors[1]);

  anchorPointStack.push(AnchorPoints(anchors[0], anchors[1]));
}

void ModelManager::zoomOut() {
  anchorPointStack.pop();
}
