/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "model/colorscalemanager.h"
#include "model/volumetric/datamap.h"
#include "model/volumetric/mapview.h"
#include "model/volumetric/volumetricdata.h"
#include <FL/Enumerations.H>
#include <algorithm>
#include <limits>
#include <memory>
#include <string>
#include <vector>

class IDataView {
public:
  virtual ~IDataView() {}
  virtual bool isDiscrete() const                                                             = 0;
  virtual double getDouble(unsigned int x, unsigned int y, unsigned int z) const              = 0;
  virtual unsigned long getULong(unsigned int x, unsigned int y, unsigned int z) const        = 0;
  virtual std::string getString(unsigned int x, unsigned int y, unsigned int z) const         = 0;
  virtual Fl_Color getColor(unsigned int x, unsigned int y, unsigned int z) const             = 0;
  virtual double getDouble(uvec3 v) const                                                     = 0;
  virtual unsigned long getULong(uvec3 v) const                                               = 0;
  virtual std::string getString(uvec3 v) const                                                = 0;
  virtual Fl_Color getColor(uvec3) const                                                      = 0;
  virtual uvec3 getDataSize() const                                                           = 0;
  virtual double getMax() const                                                               = 0;
  virtual double getMin() const                                                               = 0;
  virtual vec3 getSpacing() const                                                            = 0;
  virtual int getLevel(uvec3 v) const                                                         = 0;
  virtual unsigned long getId(uvec3 v) const                                                  = 0;
  virtual std::vector<Fl_Color> getSubarrayColors(const uvec3& start, const uvec3& end) const = 0;
  virtual uvec3 getBlockDimensions() const                                                    = 0;
  virtual void setCurrentComponent(unsigned int component)                                    = 0;
  virtual unsigned int getNumberOfComponents()                                                = 0;
};

template <typename T, typename U>
class DataView : public IDataView {
public:
  DataView(VolumetricData<T>& model, DataMap<unsigned long>& dataMap, ColorscaleManager& colorscaleManager, unsigned int column = 0)
      : dataModel(model), dataMap(dataMap), colorscaleManager(colorscaleManager) {
    if (column == 0) {
      map.reset(new IdentityMapView<T, U>());
    } else {
      map.reset(new MapView<T, U>(dataMap, column - 1));
    }

    if (map->hasMinMax()) {
      min = map->getMin();
      max = map->getMax();
    } else {
      calculateMinMax();
    }
  }

  bool isDiscrete() const override { return std::is_integral<U>::value; }

  double getDouble(unsigned int x, unsigned int y, unsigned int z) const override {
    return map->mapValue(dataModel.getDataAt(x, y, z));
  }

  unsigned long getULong(unsigned int x, unsigned int y, unsigned int z) const override {
    return map->mapValue(dataModel.getDataAt(x, y, z));
  }

  std::string getString(unsigned int x, unsigned int y, unsigned int z) const override {
    auto data_value = map->mapValue(dataModel.getDataAt(x, y, z));
    return std::to_string(data_value);
  }

  Fl_Color getColor(unsigned int x, unsigned int y, unsigned int z) const override {
    return colorscaleManager.getColor(map->mapValue(dataModel.getDataAt(x, y, z)));
  }

  double getDouble(uvec3 v) const override {
    return getDouble(v.x(), v.y(), v.z());
  }

  unsigned long getULong(uvec3 v) const override {
    return getULong(v.x(), v.y(), v.z());
  }

  std::string getString(uvec3 v) const override {
    return getString(v.x(), v.y(), v.z());
  }

  Fl_Color getColor(uvec3 v) const override {
    return getColor(v.x(), v.y(), v.z());
  }

  void calculateMinMax() {
    min = std::numeric_limits<U>::max();
    max = std::numeric_limits<U>::lowest();

    auto size = dataModel.getDataSize();

    for (unsigned int z = 0; z < size.z(); z++) {
      for (unsigned int y = 0; y < size.y(); y++) {
        for (unsigned int x = 0; x < size.x(); x++) {
          auto data_value = dataModel.getDataAt(x, y, z);
          if ((std::is_integral<T>::value && data_value == std::numeric_limits<T>::max()) ||
              (!std::is_integral<T>::value && data_value != 0 && !std::isnormal(data_value))) {
            continue;
          }

          auto value = map->mapValue(data_value);

          min = std::min(min, value);
          max = std::max(max, value);
        }
      }
    }
  }

  double getMin() const override { return min; }
  double getMax() const override { return max; }

  uvec3 getDataSize() const override {
    return dataModel.getDataSize();
  }

  vec3 getSpacing() const override {
    return dataModel.getSpacing();
  }

  int getLevel(uvec3 v) const override {
    return dataModel.getLevel(v);
  }

  unsigned long getId(uvec3 v) const override {
    return dataModel.getId(v);
  }

  std::vector<Fl_Color> getSubarrayColors(const uvec3& start, const uvec3& end) const override {
    uvec3 min(std::min(start.x(), end.x()), std::min(start.y(), end.y()), std::min(start.z(), end.z()));
    uvec3 max(std::max(start.x(), end.x()), std::max(start.y(), end.y()), std::max(start.z(), end.z()));

    auto values = dataModel.getSubarray(min, max);
    std::vector<Fl_Color> clrs;
    for (const auto& value : values) {
      clrs.push_back(colorscaleManager.getColor(map->mapValue(value)));
    }

    return clrs;
  }

  uvec3 getBlockDimensions() const override {
    return dataModel.getBlockDimensions();
  }

  void setCurrentComponent(unsigned int component) override {
    dataModel.setCurrentComponent(component);
    calculateMinMax();
  }

  unsigned int getNumberOfComponents() override {
    return dataModel.getNumberOfComponents();
  }

private:
  VolumetricData<T>& dataModel;
  DataMap<unsigned long>& dataMap;
  ColorscaleManager& colorscaleManager;
  std::unique_ptr<IMapView<T, U>> map;

  U min;
  U max;
};
