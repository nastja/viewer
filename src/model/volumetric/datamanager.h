/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef NASTJA_VIEWER_DATAINFOVIEW_H
#define NASTJA_VIEWER_DATAINFOVIEW_H

#include "event/modelelement.h"
#include "event/modellistener.h"
#include "model/presentationmode.h"
#include "model/file/filemanager.h"
#include "model/volumetric/datamap.h"
#include "model/volumetric/dataview.h"
#include "model/volumetric/mapview.h"
#include "model/volumetric/rotatedview.h"
#include "util/types.h"
#include <fstream>
#include <string>
#include <vector>

class AnchorPointStack;

class DataManager : public ModelElement {
public:
  DataManager(int frame, std::string path)
      : ModelElement("DataManager"),
        frame(frame),
        fileManager(path) {}

  IDataView& getDataView() const { return *(dataViews[viewIndex].get()); }
  RotatedView& getRotatedView() const { return *rotatedView; }

  int getFrame() const { return frame; }
  int getMaxFrame() const { return maxFrame; }

  double getMin() const { return dataViews[viewIndex]->getMin(); }
  double getMax() const { return dataViews[viewIndex]->getMax(); }

  std::string getDataFieldName() const {
    return fileManager.getFields()[dataFieldIndex];
  }

  const std::vector<std::string>& getMappingNames() const {
    return dataMap.getHeaderNames();
  }

  const std::string& getMappingName() const {
    return dataMap.getHeaderNames()[viewIndex];
  }

  int getNumberOfViews() const { return dataViews.size(); }
  int getNumberOfComponents() const { return getDataView().getNumberOfComponents(); }
  int getComponent() const { return component; }
  int getView() const { return viewIndex; }
  int getGridView() const { return gridViewAuto ? -1 : gridViewIndex; }

  void moveFrame(int step);

  void reload();

  bool is2D() const {
    uvec3 depth = dataViews[viewIndex]->getDataSize();
    return depth.x() == 1 || depth.y() == 1 || depth.z() == 1;
  }

  /**
   * Switch the data field.
   */
  void switchDataField() {
    if (fileManager.getFields().size() == 1) return;

    dataFieldIndex++;
    if (dataFieldIndex >= fileManager.getFields().size()) dataFieldIndex = 0;
    reload();
    switchDataView(0);
    notify();
  }

  /**
   * Switchs the data view.
   *
   * @param dir  The direction (forward/backward).
   */
  void switchDataView(Direction dir) {
    if (dataViews.size() <= 1) return;

    int view = viewIndex;

    switch (dir) {
      case Direction::forward:
        view++;
        break;
      case Direction::backward:
        view--;
        break;
      default:
        throw(std::runtime_error("Direction not implemented for switchDataView."));
    }

    if (view >= static_cast<int>(dataViews.size())) view = 0;
    if (view < 0) view = dataViews.size() - 1;

    switchDataView(view);
  }

  void switchDataView(int view) {
    if (dataViews.size() <= 1) return;

    if ((view < 0) || (view >= static_cast<int>(dataViews.size()))) {
      throw(std::runtime_error("View not available."));
    }

    viewIndex = view;
    if (gridViewAuto) {
      gridViewIndex = view;
    }

    rotatedView->resetView(dataViews[viewIndex].get(), dataViews[gridViewIndex].get());
    colorscaleManager->setDiscrete(dataViews[viewIndex]->isDiscrete());
    notify();
  }

  /**
   * Switchs the data view.
   *
   * @param dir  The direction (forward/backward).
   */
  void switchComponent(Direction dir) {
    if (getDataView().getNumberOfComponents() <= 1) return;

    switch (dir) {
      case Direction::forward:
        component++;
        break;
      case Direction::backward:
        component--;
        break;
      default:
        throw(std::runtime_error("Direction not implemented for switchComponent."));
    }

    if (component >= static_cast<int>(getDataView().getNumberOfComponents())) component = 0;
    if (component < 0) component = getDataView().getNumberOfComponents() - 1;

    switchComponent(component);
  }

  void switchComponent(int component) {
    if (getDataView().getNumberOfComponents() <= 1) return;

    if ((component < 0) || (component >= static_cast<int>(getDataView().getNumberOfComponents()))) {
      throw(std::runtime_error("Component not available."));
    }

    this->component = component;
    getDataView().setCurrentComponent(component);

    notify();
  }

  void switchGridDataView(int view) {
    if (dataViews.size() <= 1) return;

    if ((view < 0) || (view >= static_cast<int>(dataViews.size()))) {
      throw(std::runtime_error("View not available."));
    }

    gridViewAuto = false;
    gridViewIndex = view;

    rotatedView->resetView(dataViews[viewIndex].get(), dataViews[gridViewIndex].get());
    notify();
  }

  void setGridDataViewAuto() {
    gridViewAuto = true;
    gridViewIndex = viewIndex;
    rotatedView->resetView(dataViews[viewIndex].get(), dataViews[gridViewIndex].get());
    notify();
  }

  void setup(ModelManager& model);

private:
  template <typename T>
  void createViews(const std::vector<std::string>& filepaths);

  unsigned int dataFieldIndex{0};         ///< The current data field
  std::string previousDataFieldName{""};  ///< Name of the previous data field
  std::string path;                       ///< The root path

  int component{0};          ///< current component
  unsigned int frame{0};     ///< current frame
  unsigned int maxFrame{0};  ///< maximum available frames
  int viewIndex{0};          ///< current view
  bool gridViewAuto{true};   ///< set grid view to current view
  int gridViewIndex{0};      ///< current grid view
  FileManager fileManager;   ///< The file manager holding the file names

  std::vector<std::unique_ptr<IDataView>> dataViews;  ///< The data view for several mappings
  std::unique_ptr<RotatedView> rotatedView{nullptr};  ///< The rotated view

  PresentationMode* presMode;
  ColorscaleManager* colorscaleManager;
  CoordinateSystem* coord;
  AnchorPointStack* anchorStack;

  DataMap<unsigned long> dataMap;                  ///< Mapping of Data
  std::unique_ptr<IVolumetricData> data;           ///< Data in the block grid
  std::map<std::string, DataType> dataTypeBuffer;  ///< lookup buffer for data type by field
};

#endif  //NASTJA_VIEWER_DATAINFOVIEW_H
