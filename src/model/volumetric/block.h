/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef NASTJA_VIEWER_BLOCK_H
#define NASTJA_VIEWER_BLOCK_H

#include "util/types.h"
#include <vector>

template <typename T>
class Block {
public:
  virtual ~Block() = default;

  virtual T getDataAt(unsigned long x, unsigned long y, unsigned long z) const       = 0;
  virtual void setDataAt(unsigned long x, unsigned long y, unsigned long z, T value) = 0;
  virtual void setDataAt(unsigned long index, T value)                               = 0;
  virtual uvec3 getSize() const                                                      = 0;
  virtual T* data()                                                                  = 0;
  virtual std::vector<T> getSubarray(const ulvec3& start, const ulvec3& end) const   = 0;
  virtual void setCurrentComponent(unsigned int value)                               = 0;
};

#endif  //NASTJA_VIEWER_BLOCK_H
