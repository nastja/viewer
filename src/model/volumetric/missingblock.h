/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef NASTJA_VIEWER_MISSINGBLOCK_H
#define NASTJA_VIEWER_MISSINGBLOCK_H

#include "model/volumetric/block.h"
#include "util/types.h"
#include <limits>

template <typename T>
class MissingBlock : public Block<T> {
public:
  T getDataAt(unsigned long /*x*/, unsigned long /*y*/, unsigned long /*z*/) const override {
    constexpr T invalidValue = std::numeric_limits<T>::has_infinity ? -std::numeric_limits<T>::infinity() : std::numeric_limits<T>::max();
    return invalidValue;
  }
  void setDataAt(unsigned long /*x*/, unsigned long /*y*/, unsigned long /*z*/, T /*value*/) override {}
  void setDataAt(unsigned long /*index*/, T /*value*/) override {}
  void setCurrentComponent(unsigned int /*component*/) override {};
  uvec3 getSize() const override { return uvec3(0, 0, 0); }
  T* data() override { return nullptr; }

  std::vector<T> getSubarray(const ulvec3& start, const ulvec3& end) const override {
    auto size = end - start;
    std::vector<T> v((size.x() + 1) * (size.y() + 1) * (size.z() + 1));

    constexpr T invalidValue = std::numeric_limits<T>::has_infinity ? -std::numeric_limits<T>::infinity() : std::numeric_limits<T>::max();

    std::fill(v.begin(), v.end(), invalidValue);

    return v;
  }
};

#endif  //NASTJA_VIEWER_MISSINGBLOCK_H
