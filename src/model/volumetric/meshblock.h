/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#ifndef NASTJA_VIEWER_MESHBLOCK_H
#define NASTJA_VIEWER_MESHBLOCK_H

#include "util/math.h"

template <typename T>
class MeshBlock : public Block<T> {
public:
  explicit MeshBlock(std::shared_ptr<Block<T>> block, ulvec3 bias, int level) : block(block), level(level) {
    auto size = block->getSize();
    bias.x() *= size.x();
    bias.y() *= size.y();
    bias.z() *= size.z();
    this->bias = bias;
  }
  std::vector<T> getSubarray(const ulvec3& start, const ulvec3& end) const override {
    auto size = getSize();
    auto a    = ulvec3(start.x() % size.x(), start.y() % size.y(), start.z() % size.z());
    auto b    = ulvec3(end.x() % size.x(), end.y() % size.y(), end.z() % size.z());
    return block->getSubarray(a + bias, b + bias);
  }
  T getDataAt(unsigned long x, unsigned long y, unsigned long z) const override {
    return block->getDataAt(x + bias.x(), y + bias.y(), z + bias.z());
  }
  void setDataAt(unsigned long x, unsigned long y, unsigned long z, T value) override {
    block->setDataAt(x, y, z, value);
  }
  void setDataAt(unsigned long index, T value) override {
    block->setDataAt(index, value);
  }
  uvec3 getSize() const override {
    return block->getSize() * pow2(level) - bias;
  }
  T* data() override {
    return block->data();
  }

private:
  std::shared_ptr<Block<T>> block;
  int level;
  ulvec3 bias;
};
#endif  //NASTJA_VIEWER_MESHBLOCK_H
