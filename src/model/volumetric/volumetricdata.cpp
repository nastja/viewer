/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "volumetricdata.h"
#include "model/file/factoryparser.h"
#include "model/volumetric/missingblock.h"
#include <iostream>

void addToBlock(std::unique_ptr<Block<unsigned int>>& block, IFileParser* fileParser) {
  block = fileParser->getBlockUInt32();
}

void addToBlock(std::unique_ptr<Block<unsigned long>>& block, IFileParser* fileParser) {
  block = fileParser->getBlockUInt64();
}

void addToBlock(std::unique_ptr<Block<float>>& block, IFileParser* fileParser) {
  block = fileParser->getBlockFloat32();
}

void addToBlock(std::unique_ptr<Block<double>>& block, IFileParser* fileParser) {
  block = fileParser->getBlockFloat64();
}

template <typename T>
VolumetricData<T>::VolumetricData(const std::vector<std::string>& files, const FileManager& fileManager) {
  if (files.empty()) {
    std::cerr << "Could not found any input files. Exiting now.\n";
    std::exit(1);
  }

  blockGridSize = fileManager.getBlockGridSize();

  blocks.clear();
  levels.clear();
  std::vector<bool> isMissing;
  isMissing.resize(blockGridSize.x() * blockGridSize.y() * blockGridSize.z());
  std::fill(isMissing.begin(), isMissing.end(), true);

  FactoryFileParser factory;

  bool first = true;
  for (const auto& file : files) {  // create a VolumetricBlock for every loaded file
    uvec3 position = fileManager.getBlockPosition(file) - fileManager.getDataOffset();

    auto extension  = fileManager.getExtension(file);
    auto fileParser = factory.create(extension, file);

    if (first) {
      blockDimensions = fileParser->getInputSize();
      spacing         = fileParser->getSpacing();
      numOfComponents = fileParser->getNumberOfComponents();
      first           = false;
    }
    auto level = fileParser->getLevel();
    std::unique_ptr<Block<T>> block;
    addToBlock(block, fileParser.get()); // load data of the right data type, from the virtual member of the base class.
    auto blockIndex = position.z() * blockGridSize.y() * blockGridSize.x() + position.y() * blockGridSize.x() + position.x();
    blocks[blockIndex] = std::move(block);
    levels[blockIndex] = level;
    for (int z = 0; z < pow2(level); z++) {
      for (int y = 0; y < pow2(level); y++) {
        for (int x = 0; x < pow2(level); x++) {
          auto blockIndex = (position.z() + z) * blockGridSize.y() * blockGridSize.x() + (position.y() + y) * blockGridSize.x() + position.x() + x;
          isMissing[blockIndex] = false;
        }
      }
    }
  }

  dataSize = uvec3(blockGridSize.x() * blockDimensions.x(),
              blockGridSize.y() * blockDimensions.y(),
              blockGridSize.z() * blockDimensions.z());

  for (unsigned int i = 0; i < blockGridSize.z() * blockGridSize.y() * blockGridSize.x(); ++i) {
    if (isMissing[i]) {
      levels[i] = 0;
      blocks[i] = std::make_unique<MissingBlock<T>>();
    }
  }
}

template <typename T>
T VolumetricData<T>::getDataAt(unsigned int x, unsigned int y, unsigned int z) const {
  assert(x < dataSize.x() && y < dataSize.y() && z < dataSize.z());
  auto level = getLevel(uvec3(x, y, z));
  auto id = getId(uvec3(x, y, z));
  return blocks.at(id)->getDataAt(x % (unsigned int)(pow2(level) * blockDimensions.x()), y %(unsigned int)(pow2(level) * blockDimensions.y()), z % (unsigned int)(pow2(level) * blockDimensions.z()));
}

template <typename T>
std::vector<T> VolumetricData<T>::getSubarray(const uvec3& start, const uvec3& end) const {
  auto level = getLevel(start);
  auto id = getId(start);
  return blocks.at(id)->getSubarray(uvec3(start.x() % (unsigned int)(pow2(level) * blockDimensions.x()), start.y() % (unsigned int)(pow2(level) * blockDimensions.x()), start.z() % (unsigned int)(pow2(level) * blockDimensions.x())), uvec3(end.x() % (unsigned int)(pow2(level) * blockDimensions.x()), end.y() % (unsigned int)(pow2(level) * blockDimensions.x()), end.z() % (unsigned int)(pow2(level) * blockDimensions.x())));
}

template class VolumetricData<unsigned int>;
template class VolumetricData<unsigned long>;
template class VolumetricData<float>;
template class VolumetricData<double>;
