/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "util/stringutils.h"
#include "util/types.h"
#include <cerrno>
#include <cmath>
#include <iostream>
#include <istream>
#include <limits>
#include <map>
#include <vector>

template <typename T>
class DataMap {
public:
  explicit DataMap() {
    headers.push_back("no mapping");
  }

  ~DataMap() {}

  /**
   * Read mapping file from input. The header names are extracted from the first comment line. The data type are
   * recognized by first data line.
   *
   * @param input  The input stream.
   */
  void read(std::istream& input) {
    clean();

    bool processedHeader = false;
    bool firstDataLine   = true;

    std::string line;
    size_t row = 0;
    while (std::getline(input, line)) {
      if (line.front() == '#') {                // comment or header line
        if (processedHeader) continue;          // only first comment can be the header
        headers         = split(line, " ", 1);  // skip the first char '#'
        processedHeader = true;
      } else {
        if (firstDataLine) {
          types         = getDataTypes(line);
          firstDataLine = false;
        }
        readDataLine(line.c_str(), row);
        row++;
      }
    }

    if (headers.size() == 0) {
      setDefaultNames();
    }

    calcMinMax();
  }

  double getValueDouble(T value, size_t column) const {
    return data[map.at(value) * types.size() + column].d;
  }

  unsigned long getValueLong(T value, size_t column) const {
    return data[map.at(value) * types.size() + column].l;
  }

  double getMinDouble(size_t column) const { return minmax[2 * column].d; }
  double getMaxDouble(size_t column) const { return minmax[2 * column + 1].d; }
  unsigned long getMinLong(size_t column) const { return minmax[2 * column].l; }
  unsigned long getMaxLong(size_t column) const { return minmax[2 * column + 1].l; }

  const std::vector<std::string>& getHeaderNames() const { return headers; }
  size_t getColumns() const { return types.size(); }
  DataType getColumnType(size_t column) const { return types[column]; }

private:
  union data_t {
    double d;
    unsigned long l;
  };

  /**
   * Gets the data types of a line.
   *
   * The first column is skipped, here is the map key. If the value has a dot, it is defined to be a floating number,
   * else it is an integer number.
   *
   * @param line  The line.
   *
   * @return The vector of data types.
   */
  std::vector<DataType> getDataTypes(const std::string& line) {
    std::vector<DataType> v;
    auto splitColumns = split(line);
    for (int i = 1; i < splitColumns.size(); i++) {
      auto& field = splitColumns[i];
      if (field.find_first_not_of("-+0123456789") == std::string::npos) {
        v.push_back(DataType::uint64);
      } else {
        v.push_back(DataType::float64);
      }
    }
    return v;
  }

  /**
   * Reads a data line. Add the first column as map key points to the row in the data vector. The other columns are read
   * by the type given in the types vector.
   *
   * @param line  The line.
   * @param row   The row number.
   */
  void readDataLine(const char* line, size_t row) {
    const char* pStart = line;
    char* pEnd;

    T mapkey    = std::strtol(line, &pEnd, 0);
    map[mapkey] = row;

    if (line == pEnd) {
      std::cout << "Element not found in row " << row << ": '" << line << "', assuming zero.\n";
    } else if (errno == ERANGE) {
      std::cout << "Range error in row " << row << ":" << (pEnd - pStart) << ": '" << line << "'\n";
      errno = 0;
    }
    line = pEnd;

    data_t d;
    for (auto type : types) {
      if (type == DataType::uint64) {
        d.l = std::strtoul(line, &pEnd, 0);
      } else {
        d.d = std::strtod(line, &pEnd);
      }
      data.push_back(d);

      if (line == pEnd) {
        std::cout << "Element not found in row " << row << ": '" << line << "', assuming zero.\n";
      } else if (errno == ERANGE) {
        std::cout << "Range error in row " << row << ":" << (pEnd - pStart) << ": '" << line << "'\n";
        errno = 0;
      }
      line = pEnd;
    }
  }

  /**
   * clean all data
   */
  void clean() {
    data.clear();
    headers.clear();
    map.clear();
    types.clear();
  }

  /**
   * Sets default names.
   */
  void setDefaultNames() {
    headers.push_back("no mapping");
    for (int i = 0; i < types.size(); i++) {
      headers.push_back("value " + std::to_string(i));
    }
  }

  void calcMinMax() {
    minmax.resize(2 * (types.size()));

    // initialize min/max for all columns
    for (int column = 0; column < types.size(); column++) {
      if (types[column] == DataType::uint64) {
        minmax[2 * column].l     = std::numeric_limits<unsigned long>::max();
        minmax[2 * column + 1].l = std::numeric_limits<unsigned long>::lowest();
      } else {
        minmax[2 * column].d     = std::numeric_limits<double>::max();
        minmax[2 * column + 1].d = std::numeric_limits<double>::lowest();
      }
    }

    // find min/max columnwise
    auto rows = map.size();
    for (size_t row = 0; row < rows; row++) {
      for (int column = 0; column < types.size(); column++) {
        if (types[column] == DataType::uint64) {
          auto value = data[row * types.size() + column].l;
          if (value == std::numeric_limits<unsigned long>::max()) continue;
          minmax[2 * column].l     = std::min(minmax[2 * column].l, value);
          minmax[2 * column + 1].l = std::max(minmax[2 * column + 1].l, value);
        } else {
          auto value = data[row * types.size() + column].d;
          if (value != 0 && !std::isnormal(value)) continue;
          minmax[2 * column].d     = std::min(minmax[2 * column].d, value);
          minmax[2 * column + 1].d = std::max(minmax[2 * column + 1].d, value);
        }
      }
    }
  }

  std::map<T, size_t> map;           ///< mapping from volume data to row of data
  std::vector<data_t> data;          ///< linearized vector of data
  std::vector<DataType> types;       ///< column type
  std::vector<std::string> headers;  ///< names of the headers
  std::vector<data_t> minmax;        ///< min and max values
};
