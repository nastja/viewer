/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "model/volumetric/datamap.h"
#include <cmath>
#include <limits>
#include <type_traits>

template <typename T, typename U>
class IMapView {
public:
  virtual ~IMapView() {}
  virtual U mapValue(T value) const = 0;
  virtual bool hasMinMax() const    = 0;
  virtual U getMin() const          = 0;
  virtual U getMax() const          = 0;
};

template <typename T, typename U>  // T Block value and U mapping to type
class MapView : public IMapView<T, U> {
public:
  explicit MapView(){};
  explicit MapView(const DataMap<unsigned long>& datamap, size_t column) : datamap(&datamap), column(column) {
    if ((std::is_integral<U>::value && datamap.getColumnType(column) == DataType::float64) ||
        (!std::is_integral<U>::value && datamap.getColumnType(column) == DataType::uint64)) {
      throw std::runtime_error("MapView target type does not match to column type.");
    }
  }
  ~MapView() override = default;

  /**
   * Maps T values via look up to U values.
   *
   * If the ´value´ is a special value this is not lookup. Integral types maps to max and float types maps to -infinity.
   *
   * @param value  The value.
   *
   * @return The mapped value.
   */
  U mapValue(T value) const override final {
    if (std::is_integral<T>::value && value == std::numeric_limits<T>::max()) {
      if (std::is_integral<U>::value) {
        return std::numeric_limits<U>::max();
      } else {
        return -std::numeric_limits<U>::infinity();
      }
    } else if (!std::is_integral<T>::value && value != 0.0 && !std::isnormal(value)) {
      if (!std::is_integral<U>::value) {
        return value;
      } else {
        return std::numeric_limits<U>::max();
      }
    }

    if (std::is_integral<U>::value) {
      return datamap->getValueLong(value, column);
    } else {
      return datamap->getValueDouble(value, column);
    }
  }

  bool hasMinMax() const override final { return true; }

  U getMin() const override final {
    if (std::is_integral<U>::value) {
      return datamap->getMinLong(column);
    } else {
      return datamap->getMinDouble(column);
    }
  }

  U getMax() const override final {
    if (std::is_integral<U>::value) {
      return datamap->getMaxLong(column);
    } else {
      return datamap->getMaxDouble(column);
    }
  }

private:
  const DataMap<unsigned long>* datamap{nullptr};  // TODO: add type T for DataMap
  size_t column;
};

template <typename T, typename U>  // T Block value and U mapping to type
class IdentityMapView : public IMapView<T, U> {
public:
  explicit IdentityMapView() {}
  ~IdentityMapView() override = default;

  bool hasMinMax() const override final { return false; }
  U getMin() const override final { return U(0); }
  U getMax() const override final { return U(0); }

  U mapValue(T value) const override final {
    return value;
  }
};
