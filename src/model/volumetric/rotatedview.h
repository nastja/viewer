/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef NASTJA_VIEWER_ROTATEDVIEW_H
#define NASTJA_VIEWER_ROTATEDVIEW_H

#include "model/anchorpointstack.h"
#include "model/coordinatesystem.h"
#include "model/volumetric/dataview.h"
#include <memory>

class AnchorPointStack;

class RotatedView {
public:
  RotatedView(IDataView* dataView, IDataView* gridDataView, CoordinateSystem& coord, AnchorPointStack& anchorStack)
      : dataView(dataView), gridDataView(gridDataView), coord(coord), anchorStack(anchorStack) {}

  std::vector<Fl_Color> getSubarrayColors(uvec3 const& vector1, uvec3 const& vector2) const;

  bool isScreenSpaceGradientX(const uvec3& pos) const;
  bool isScreenSpaceGradientY(const uvec3& pos) const;
  bool isScreenSpaceGradientZ(const uvec3& pos) const;

  double getDouble(unsigned int x, unsigned int y, unsigned int z) const;
  double getDouble(uvec3 v) const { return getDouble(v.x(), v.y(), v.z()); }

  unsigned long getULong(unsigned int x, unsigned int y, unsigned int z) const;
  unsigned long getULong(uvec3 v) const { return getULong(v.x(), v.y(), v.z()); }

  std::string getString(unsigned int x, unsigned int y, unsigned int z) const;
  std::string getString(uvec3 v) const { return getString(v.x(), v.y(), v.z()); }

  Fl_Color getColor(unsigned int x, unsigned int y, unsigned int z) const;
  Fl_Color getColor(uvec3 v) const { return getColor(v.x(), v.y(), v.z()); }

  double getMax() { return dataView->getMax(); }
  double getMin() { return dataView->getMin(); }

  uvec3 getDataSize() const;
  vec3 getSpacing() const;
  int getLevel(uvec3 v) const { return dataView->getLevel(transformFromScreenSpace(v)); }
  unsigned long getId(uvec3 v) const { return dataView->getId(transformFromScreenSpace(v)); }
  uvec3 getBlockDimensions() const;
  uvec3 transformFromScreenSpace(unsigned int x, unsigned int y, unsigned int z) const;
  uvec3 transformFromScreenSpace(uvec3 v) const;
  uvec3 transformToScreenSpace(unsigned int x, unsigned int y, unsigned int z) const;
  uvec3 transformToScreenSpace(uvec3 v) const;

  void resetView(IDataView* view, IDataView* grid = nullptr) {
    dataView = view;
    gridDataView = (grid == nullptr) ? view : grid;
  }

private:
  /**
   * Calculates the offsets for the rotation of the unrotated rectangle's vector
   *
   * @param globalOffset  The global offset.
   * @param offset1       the axis with the smaller offset
   * @param offset2       the axis with the bigger offset
   * @param base1         base of the axis with offset1
   * @param base2         base of the axis with offset2
   * @param diff1         size of the rectangle in the direction of the axis with the offset1
   * @param diff2         size of the rectangle in the direction of the axis with the offset1
   */
  void calculateOffsets(long& globalOffset, long& offset1, long& offset2, ivec3 base1, ivec3 base2, long diff1, long diff2) const;

  double getGridViewDouble(unsigned int x, unsigned int y, unsigned int z) const;
  double getGridViewDouble(uvec3 v) const { return getGridViewDouble(v.x(), v.y(), v.z()); }

  IDataView* dataView;
  IDataView* gridDataView;
  CoordinateSystem& coord;
  AnchorPointStack& anchorStack;
};

#endif  //NASTJA_VIEWER_ROTATEDVIEW_H
