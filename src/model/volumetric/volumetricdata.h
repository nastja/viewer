/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_MODEL_VOLUMETRIC_VOLUMETRICDATA_H_
#define SRC_MODEL_VOLUMETRIC_VOLUMETRICDATA_H_

#include "model/file/filemanager.h"
#include "model/volumetric/volumetricblock.h"
#include "util/math.h"
#include "util/types.h"
#include <map>
#include <memory>
#include <string>
#include <unordered_map>

/**
 * Back-end part for Volumetric ModelElement.
 * This Class manages the loaded volumetric data in cuboid blocks. Due to size limitations, only the Blocks for the current time are loaded.
 *
 * It also holds an array of strings for every block that is parsed using a BlockFilenameParser in the constructor and is used when blocks are being loaded.
 *
 * @ingroup volumetric
 */

class IVolumetricData {
public:
  virtual ~IVolumetricData() {}
};

template <typename T>
class VolumetricData : public IVolumetricData {
public:
  /**
   * Creates a new Instance of VolumetricData with a BlockFilenameParser pointing to the fileName
   *
   * @param files        The files.
   * @param fileManager  The file manager.
   */
  VolumetricData(const std::vector<std::string>& files, const FileManager& fileManager);
  ~VolumetricData() override = default;

  /** @return data from current time at position x y z */
  T getDataAt(unsigned int x, unsigned int y, unsigned int z) const;

  std::vector<T> getSubarray(const uvec3& start, const uvec3& end) const;

  /**
   *@return the current size of the data
   */
  uvec3 getDataSize() const { return dataSize; }

  vec3 getSpacing() const { return spacing; }

  int getLevel(uvec3 v) const {
    assert(v.x() < dataSize.x() && v.y() < dataSize.y() && v.z() < dataSize.z());
    int level = 0;
    unsigned int index = (v.z() / blockDimensions.z()) * blockGridSize.y() * blockGridSize.x() + (v.y() / blockDimensions.y()) * blockGridSize.x() + (v.x() / blockDimensions.x());
    v.x() /= blockDimensions.x();
    v.y() /= blockDimensions.y();
    v.z() /= blockDimensions.z();
    while (blocks.count(index) == 0) {
      v.x() >>= level;
      v.x() <<= level;
      v.y() >>= level;
      v.y() <<= level;
      v.z() >>= level;
      v.z() <<= level;
      index = v.z() * blockGridSize.y() * blockGridSize.x() + v.y() * blockGridSize.x() + v.x();
      level++;
    }
    return levels.at(index);
  }

  int getId(uvec3 v) const {
    assert(v.x() < dataSize.x() && v.y() < dataSize.y() && v.z() < dataSize.z());
    auto level = getLevel(v);
    v.x() /= blockDimensions.x();
    v.y() /= blockDimensions.y();
    v.z() /= blockDimensions.z();
    v.x() >>= level;
    v.x() <<= level;
    v.y() >>= level;
    v.y() <<= level;
    v.z() >>= level;
    v.z() <<= level;

    return v.z() * blockGridSize.y() * blockGridSize.x() + v.y() * blockGridSize.x() + v.x();
  }

  uvec3 getBlockDimensions() const { return blockDimensions; }

  unsigned int getNumberOfComponents() { return numOfComponents; }

  void setCurrentComponent(unsigned int component) {
    this->component = component;
    for (auto& block : blocks) {
      block.second->setCurrentComponent(component);
    }
  }

private:
  uvec3 dataSize;         ///< number of all cells in direction
  uvec3 blockDimensions;  ///< amount of cells in a block
  uvec3 blockGridSize;    ///< number of blocks in directions
  vec3 spacing{1.0, 1.0, 1.0};
  std::unordered_map<unsigned int, int> levels;
  unsigned int numOfComponents;                                       ///< number of components
  unsigned int component{0};                                          ///< selectet component
  std::unordered_map<unsigned int, std::unique_ptr<Block<T>>> blocks; ///< dynamically allocated array, has size amountBlocks.x()*y*z
};

#endif  // SRC_MODEL_VOLUMETRIC_VOLUMETRICDATA_H_
