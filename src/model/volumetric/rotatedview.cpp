/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "rotatedview.h"
#include <cmath>

std::vector<Fl_Color> RotatedView::getSubarrayColors(uvec3 const& vector1, uvec3 const& vector2) const {
  auto& anchors = anchorStack.getAnchorPoints();

  auto rotatedSize = coord.transformVector(anchors.getSize());

  ivec3 dx{1, 0, 0};
  ivec3 dy{0, 1, 0};
  ivec3 dz{0, 0, 1};
  ivec3 start = vector1;
  ivec3 end   = vector2;

  if (rotatedSize.x() < 0) {
    start.x() += rotatedSize.x() + 1;
    end.x() += rotatedSize.x() + 1;
  }
  if (rotatedSize.y() < 0) {
    start.y() += rotatedSize.y() + 1;
    end.y() += rotatedSize.y() + 1;
  }
  if (rotatedSize.z() < 0) {
    start.z() += rotatedSize.z() + 1;
    end.z() += rotatedSize.z() + 1;
  }
  dx = coord.transformVectorInverse(dx);
  dy = coord.transformVectorInverse(dy);
  dz = coord.transformVectorInverse(dz);

  start = coord.transformVectorInverse(start);
  start = anchors.transformVectorInverse(start);

  end = coord.transformVectorInverse(end);
  end = anchors.transformVectorInverse(end);

  auto rawRect = dataView->getSubarrayColors(start, end);

  auto diff = vector2 - vector1;

  int xBaseIndex = abs(dx.x() + 2 * dx.y() + 3 * dx.z());
  int yBaseIndex = abs(dy.x() + 2 * dy.y() + 3 * dy.z());
  int zBaseIndex = abs(dz.x() + 2 * dz.y() + 3 * dz.z());

  long offset  = 0;
  long xOffset = 0;
  long yOffset = 0;
  long zOffset = 0;

  if (diff.x() != 0) {
    if (diff.y() != 0) {
      if (xBaseIndex < yBaseIndex) {
        calculateOffsets(offset, xOffset, yOffset, dx, dy, diff.x(), diff.y());
      } else {
        calculateOffsets(offset, yOffset, xOffset, dy, dx, diff.y(), diff.x());
      }
    } else {
      if (xBaseIndex < zBaseIndex) {
        calculateOffsets(offset, xOffset, zOffset, dx, dz, diff.x(), diff.z());
      } else {
        calculateOffsets(offset, zOffset, xOffset, dz, dx, diff.z(), diff.x());
      }
    }
  } else {
    if (yBaseIndex < zBaseIndex) {
      calculateOffsets(offset, yOffset, zOffset, dy, dz, diff.y(), diff.z());
    } else {
      calculateOffsets(offset, zOffset, yOffset, dz, dy, diff.z(), diff.y());
    }
  }

  std::vector<Fl_Color> rect;
  rect.reserve((diff.x() + 1) * (diff.y() + 1) * (diff.z() + 1));
  for (unsigned long z = 0; z <= diff.z(); z++) {
    for (unsigned long y = 0; y <= diff.y(); y++) {
      for (unsigned long x = 0; x <= diff.x(); x++) {
        auto index = offset + x * xOffset + y * yOffset + z * zOffset;
        rect.push_back(rawRect[index]);
      }
    }
  }
  return rect;
}

void RotatedView::calculateOffsets(long& globalOffset, long& offset1, long& offset2, ivec3 base1, ivec3 base2, long diff1, long diff2) const {
  offset1      = base1.x() + base1.y();
  offset2      = base2.x() + (diff1 + 1) * (base2.y() + base2.z());
  globalOffset = pow(0, 1 + base1.x() + base1.y()) * diff1 + pow(0, 1 + base2.y() + base2.z()) * (diff1 + 1) * diff2;
  if (base1.x() + base1.y() + base1.z() + base2.x() + base2.y() + base2.z() == -2) {
    globalOffset = (diff1 + 1) * (diff2 + 1) - 1;
  }
}

bool RotatedView::isScreenSpaceGradientX(const uvec3& pos) const {
  auto a = getGridViewDouble(pos);
  auto b = getGridViewDouble(pos + uvec3(1, 0, 0));
  return a != b;
}

bool RotatedView::isScreenSpaceGradientY(const uvec3& pos) const {
  auto a = getGridViewDouble(pos);
  auto b = getGridViewDouble(pos + uvec3(0, 1, 0));
  return a != b;
}

bool RotatedView::isScreenSpaceGradientZ(const uvec3& pos) const {
  auto a = getGridViewDouble(pos);
  auto b = getGridViewDouble(pos + uvec3(0, 0, 1));
  return a != b;
}

double RotatedView::getGridViewDouble(unsigned int x, unsigned int y, unsigned int z) const {
  auto tmp = transformFromScreenSpace(x, y, z);
  return gridDataView->getDouble(tmp.x(), tmp.y(), tmp.z());
}

double RotatedView::getDouble(unsigned int x, unsigned int y, unsigned int z) const {
  auto tmp = transformFromScreenSpace(x, y, z);
  return dataView->getDouble(tmp.x(), tmp.y(), tmp.z());
}

unsigned long RotatedView::getULong(unsigned int x, unsigned int y, unsigned int z) const {
  auto tmp = transformFromScreenSpace(x, y, z);
  return dataView->getULong(tmp.x(), tmp.y(), tmp.z());
}

std::string RotatedView::getString(unsigned int x, unsigned int y, unsigned int z) const {
  auto tmp = transformFromScreenSpace(x, y, z);
  return dataView->getString(tmp.x(), tmp.y(), tmp.z());
}

Fl_Color RotatedView::getColor(unsigned int x, unsigned int y, unsigned int z) const {
  auto tmp = transformFromScreenSpace(x, y, z);
  return dataView->getColor(tmp.x(), tmp.y(), tmp.z());
}

uvec3 RotatedView::getDataSize() const {
  auto size = anchorStack.getAnchorPoints().getSize();
  auto temp = coord.transformVector(size);
  for (int i = 0; i < 3; i++)
    temp[i] = std::abs(temp[i]);
  return temp;
}

vec3 RotatedView::getSpacing() const {
  auto spacing = dataView->getSpacing();
  auto temp    = coord.transformVector(spacing);
  for (int i = 0; i < 3; i++)
    temp[i] = fabs(temp[i]);
  return temp;
}

uvec3 RotatedView::getBlockDimensions() const {
  auto dimensions = dataView->getBlockDimensions();
  auto temp       = coord.transformVector(dimensions);
  for (int i = 0; i < 3; i++)
    temp[i] = fabs(temp[i]);
  return temp;
}

uvec3 RotatedView::transformFromScreenSpace(unsigned int x, unsigned int y, unsigned int z) const {
  ivec3 global(x, y, z);
  auto& anchors    = anchorStack.getAnchorPoints();
  auto rotatedSize = coord.transformVector(anchors.getSize());

  if (rotatedSize.x() < 0) global += ivec3(rotatedSize.x() + 1, 0, 0);
  if (rotatedSize.y() < 0) global += ivec3(0, rotatedSize.y() + 1, 0);
  if (rotatedSize.z() < 0) global += ivec3(0, 0, rotatedSize.z() + 1);

  global = coord.transformVectorInverse(global);
  global = anchors.transformVectorInverse(global);
  return global;
}

uvec3 RotatedView::transformFromScreenSpace(uvec3 v) const {
  return transformFromScreenSpace(v.x(), v.y(), v.z());
}

uvec3 RotatedView::transformToScreenSpace(unsigned int x, unsigned int y, unsigned int z) const {
  auto& anchors    = anchorStack.getAnchorPoints();
  auto rotatedSize = coord.transformVector(anchors.getSize());

  ivec3 local(x, y, z);
  local = anchors.transformVector(local);
  local = coord.transformVector(local);

  if (rotatedSize.x() < 0) local -= ivec3(rotatedSize.x() + 1, 0, 0);
  if (rotatedSize.y() < 0) local -= ivec3(0, rotatedSize.y() + 1, 0);
  if (rotatedSize.z() < 0) local -= ivec3(0, 0, rotatedSize.z() + 1);

  assert(local.x() >= 0 && local.y() >= 0 && local.z() >= 0);
  return local;
}

uvec3 RotatedView::transformToScreenSpace(uvec3 v) const {
  return transformToScreenSpace(v.x(), v.y(), v.z());
}
