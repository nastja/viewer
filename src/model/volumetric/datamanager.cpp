/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "datamanager.h"
#include "model/modelmanager.h"
#include "model/file/factoryparser.h"

/**
 * Set up.
 *
 * @param modelManager  The model manager.
 */
void DataManager::setup(ModelManager& modelManager) {
  presMode          = &(modelManager.getPresentationMode());
  colorscaleManager = &(modelManager.getColorscaleManager());
  coord             = &(modelManager.getCoordinateSystem());
  anchorStack       = &(modelManager.getAnchorPointStack());
  reload();
}

/**
 * Moves step width frames forward or backward. Integer min or max define a jump to the first or last frame, respectively.
 *
 * @param stepWidth  The step width.
 */
void DataManager::moveFrame(int stepWidth) {
  if ((stepWidth == 0) || (stepWidth < 0 && frame == 0) || (stepWidth > 0 && frame == maxFrame)) return;

  if (stepWidth == std::numeric_limits<int>::max()) {
    frame = maxFrame;
  } else if (stepWidth == std::numeric_limits<int>::min()) {
    frame = 0;
  } else {
    int newFrame = static_cast<int>(frame) + stepWidth;
    frame = std::max(0, std::min(newFrame, static_cast<int>(maxFrame)));
  }

  reload();
  presMode->setRedraw(true);
}

/**
 * Create the views for the mapping.
 *
 * @param filepaths  The filepaths.
 */
template <typename T>
void DataManager::createViews(const std::vector<std::string>& filepaths) {
  data.reset(new VolumetricData<T>(filepaths, fileManager));

  dataViews.clear();

  dataViews.emplace_back(std::make_unique<DataView<T, T>>(dynamic_cast<VolumetricData<T>&>(*data), dataMap, *colorscaleManager));
  for (unsigned int i = 0; i < dataMap.getColumns(); i++) {
    if (dataMap.getColumnType(i) == DataType::uint64) {
      dataViews.emplace_back(std::make_unique<DataView<T, unsigned long>>(dynamic_cast<VolumetricData<T>&>(*data), dataMap, *colorscaleManager, i + 1));
    } else {
      dataViews.emplace_back(std::make_unique<DataView<T, double>>(dynamic_cast<VolumetricData<T>&>(*data), dataMap, *colorscaleManager, i + 1));
    }
  }
}

/**
 * Determine the data type and reload the view.
 */
void DataManager::reload() {
  fileManager.read();
  const auto& dataFieldName = fileManager.getFields()[dataFieldIndex];

  maxFrame = fileManager.getMaximumFrame(dataFieldName);

  if (fileManager.hasMappingFile()) {
    auto& mappingFile = fileManager.getMappingFile(frame);
    std::ifstream f(mappingFile);
    dataMap.read(f);
  }

  const auto& filepaths = fileManager.getDataFiles(dataFieldName, frame);

  auto dataType = dataTypeBuffer[dataFieldName];
  if (dataType == DataType::unknown) {
    auto extension = fileManager.getExtension(filepaths[0]);

    dataType = FactoryFileParser().create(extension, filepaths[0])->getType();

    dataTypeBuffer[dataFieldName] = dataType;
  }

  if (dataType == DataType::float32) {
    createViews<float>(filepaths);
  } else if (dataType == DataType::float64) {
    createViews<double>(filepaths);
  } else if (dataType == DataType::uint32) {
    createViews<unsigned int>(filepaths);
  } else if (dataType == DataType::uint64) {
    createViews<unsigned long>(filepaths);
  }

  if (!rotatedView) {
    rotatedView = std::make_unique<RotatedView>(dataViews[viewIndex].get(), dataViews[gridViewIndex].get(), *coord, *anchorStack);
  } else {
    rotatedView->resetView(dataViews[viewIndex].get(), dataViews[gridViewIndex].get());
  }

  if (previousDataFieldName != getDataFieldName()) {
    previousDataFieldName = getDataFieldName();
    colorscaleManager->setDiscrete(dataViews[viewIndex]->isDiscrete());
  }

  getDataView().setCurrentComponent(component);
  notify();
}
