/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_MODEL_VOLUMETRIC_VOLUMETRICBLOCK_H_
#define SRC_MODEL_VOLUMETRIC_VOLUMETRICBLOCK_H_

#include "model/volumetric/block.h"
#include "util/math.h"
#include "util/types.h"
#include <cassert>
#include <vector>

/**
 * Represents single Datablock of one point in time from a larger data set.
 *
 * @ingroup volumetric
 */
template <typename T>
class VolumetricBlock : public Block<T> {
public:
  explicit VolumetricBlock(ulvec3 size, int level = 0, unsigned int components = 1) : blockSize(size), v(size.x() * size.y() * size.z()  * components), level(level), components(components) {}

  std::vector<T> getSubarray(const ulvec3& start, const ulvec3& end) const override {
    auto size = end - start;
    std::vector<T> result;
    result.reserve((size.x() + 1) * (size.y() + 1) * (size.z() + 1));

    for (unsigned long z = start.z(); z <= end.z(); z++) {
      for (unsigned long y = start.y(); y <= end.y(); y++) {
        for (unsigned long x = start.x(); x <= end.x(); x++) {
          result.push_back(getDataAt(x, y, z));
        }
      }
    }

    return result;
  }

  /**
   * Gets the data at (x, y, z) position.
   *
   * @param x  The x-coordinate.
   * @param y  The y-coordinate.
   * @param z  The z-coordinate.
   *
   * @return The value at (x, y, z) postilion.
   */
  T getDataAt(unsigned long x, unsigned long y, unsigned long z) const override {
    assert(x < blockSize.x() * pow2(level) && y < blockSize.y() * pow2(level) && z < blockSize.z() * pow2(level));
    x >>= level;
    y >>= level;
    z >>= level;
    return v[(z * blockSize.x() * blockSize.y() + y * blockSize.x() + x) * components + currentComponent];
  }

  /**
   * Sets the data value at (x, y, z) position.
   *
   * @param x      The x-coordinate.
   * @param y      The y-coordinate.
   * @param z      The z-coordinate.
   * @param value  The value.
   */
  void setDataAt(unsigned long x, unsigned long y, unsigned long z, T value) override {
    assert(x < blockSize.x() && y < blockSize.y() && z < blockSize.z());
    v[z * blockSize.x() * blockSize.y() + y * blockSize.x() + x] = value;
  }

  void setDataAt(unsigned long index, T value) override {
    v[index] = value;
  }

  void setCurrentComponent(unsigned int component) override {
    currentComponent = component;
  }

  /**
   * Gets the block size.
   *
   * @return the size of the block (not in cells)
   */
  uvec3 getSize() const override { return blockSize; }

  T* data() override { return v.data(); }

private:
  uvec3 blockSize;   ///< The size of a block .vti file.
  std::vector<T> v;  ///< Linearized vector of 3D data array.
  int level;
  unsigned int components;
  unsigned int currentComponent = 0;
};

#endif  // SRC_MODEL_VOLUMETRIC_VOLUMETRICBLOCK_H_
