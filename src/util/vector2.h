/*
 * Copyright (c) 2016 - 2019 Marco Berghoff
 * Distributed under the MIT License (license terms are in file LICENSE or at http://opensource.org/licenses/MIT).
 */

#pragma once

#include "mathutils.h"
#include <cassert>
#include <cmath>
#include <iostream>
#include <type_traits>

/**
 * Class for Vector2, a three dimensional vector.
 *
 * @tparam T  Type of the vector elements.
 */
template <typename T>
class Vector2 {
  static_assert(std::is_arithmetic<T>::value, "The Vector2 class needs an arithmetic data type.");

public:
  /// Define the type of the length, this can not be an integral type because of the sqrt.
  using length_type = typename std::conditional<std::is_integral<T>::value, double, T>::type;

  /// @name Constructors
  /// @{

  explicit Vector2() : v{T(), T()} {}
  explicit Vector2(T initial) : v{initial, initial} {}
  explicit Vector2(T x, T y) : v{x, y} {}
  explicit Vector2(const T* initial) : v{initial[0], initial[1]} {}

  template <typename U>
  Vector2(const Vector2<U>& v) : v{T(v[0]), T(v[1])} {}
  /// @}

  /// @name Operators
  /// @{
  //
  Vector2 operator-() const { return Vector2(-v[0], -v[1]); }

  template <typename U>
  Vector2<typename std::common_type<T, U>::type> operator+(const Vector2<U>& other) const {
    return Vector2<typename std::common_type<T, U>::type>(v[0] + other[0], v[1] + other[1]);
  }

  template <typename U>
  Vector2<typename std::common_type<T, U>::type> operator-(const Vector2<U>& other) const {
    return Vector2<typename std::common_type<T, U>::type>(v[0] - other[0], v[1] - other[1]);
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Vector2<typename std::common_type<T, U>::type> operator*(U scalar) const {
    return Vector2<typename std::common_type<T, U>::type>(v[0] * scalar, v[1] * scalar);
  }

  template <typename U>
  typename std::common_type<T, U>::type operator*(const Vector2<U>& other) {
    return dotProduct(other);
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Vector2<typename std::common_type<T, U>::type> operator/(U scalar) const {
    return Vector2<typename std::common_type<T, U>::type>(v[0] / scalar, v[1] / scalar);
  }

  template <typename U>
  typename std::common_type<T, U>::type dotProduct(const Vector2<U>& other) const {
    return v[0] * other[0] + v[1] * other[1];
  }

  template <typename U>
  Vector2& operator+=(const Vector2<U>& other) {
    v[0] += other[0];
    v[1] += other[1];
    return *this;
  }

  template <typename U>
  Vector2& operator-=(const Vector2<U>& other) {
    v[0] -= other[0];
    v[1] -= other[1];
    return *this;
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Vector2& operator*=(U scalar) {
    v[0] *= scalar;
    v[1] *= scalar;
    return *this;
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Vector2& operator/=(U scalar) {
    v[0] /= scalar;
    v[1] /= scalar;
    return *this;
  }

  template <typename U>
  bool operator==(const Vector2<U>& other) const {
    return nearlyEqual(v[0], other[0]) && nearlyEqual(v[1], other[1]);
  }

  template <typename U>
  bool operator!=(const Vector2<U>& other) const {
    return !nearlyEqual(v[0], other[0]) || !nearlyEqual(v[1], other[1]);
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  bool operator==(U scalar) const {
    return nearlyEqual(v[0], scalar) && nearlyEqual(v[1], scalar);
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  bool operator!=(U scalar) const {
    return !nearlyEqual(v[0], scalar) || !nearlyEqual(v[1], scalar);
  }

  /// @}

  /// @name Element access
  /// @{

  T& operator[](unsigned int index) {
    return v[index];
  }

  const T& operator[](unsigned int index) const {
    return v[index];
  }

  const T& x() const {
    return v[0];
  }

  const T& y() const {
    return v[1];
  }

  T& x() {
    return v[0];
  }

  T& y() {
    return v[1];
  }

  /**
   * Data access function.
   *
   * @return A Pointer to the data.
   */
  T* data() noexcept {
    return v;
  }

  /**
   * Data access function.
   *
   * @return A Pointer to the data.
   */
  const T* data() const noexcept {
    return v;
  }

  /**
   * Set the vector.
   *
   * @param x  The x-coordinate.
   * @param y  The y-coordinate.
   */
  void set(T x, T y) {
    v[0] = x;
    v[1] = y;
  }
  /// @}

  /// @name Arithmetic operators
  /// @{

  /**
   * Calculates the length.
   *
   * @return The length of the vector.
   */
  length_type length() const {
    return std::sqrt(static_cast<length_type>(v[0] * v[0] + v[1] * v[1]));
  }

  Vector2<T>& rotate(T angle) {
    T s = sin(angle * M_PI / 180.0);
    T c = cos(angle * M_PI / 180.0);
    T t = v[0] * c - v[1] * s;

    v[1] = v[0] * s + v[1] * c;
    v[0] = t;

    return *this;
  }


  /**
   * Normalize the vector.
   *
   * The normalization of a Vector2 is only possible when its type and its length_type are the same. For Vector2 of
   * integral types use getNormalized() instead.
   *
   * @return The normalized vector.
   */
  Vector2& normalize() {
    static_assert(std::is_same<length_type, T>::value, "Normalization of a Vector2 is not possible when type differs from its length_type.");

    const length_type len(length());
    assert(len != 0.0);
    const length_type len_r(length_type(1) / len);

    return operator*=(len_r);
  }

  /**
   * Gets the normalized vector.
   *
   * @return The normalized vector.
   */
  Vector2<length_type> getNormalized() const {
    const length_type len(length());
    assert(len != 0.0);
    const length_type len_r(length_type(1) / len);

    return Vector2<length_type>(static_cast<length_type>(v[0]) * len_r,
                                static_cast<length_type>(v[1]) * len_r);
  }
  /// @}

  static const Vector2 Zero;  ///< Zero vector

private:
  T v[2];  ///< Array of length two, holding the vector with element type T.
};

template <typename T>
const Vector2<T> Vector2<T>::Zero = Vector2(T(0));

/**
 * Overload of the ostream insertion operator.
 *
 * @param os  The output stream.
 * @param v   The vector.
 *
 * @tparam T  Type of the vector elements.
 *
 * @return Reference to the output stream.
 */
template <typename T>
inline std::ostream& operator<<(std::ostream& os, const Vector2<T>& v) {
  return os << "[" << v[0] << "," << v[1] << "]";
}

template <typename T, typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
bool operator==(U scalar, const Vector2<T>& v) {
  return v == scalar;
}

template <typename T, typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
bool operator!=(U scalar, const Vector2<T>& v) {
  return v != scalar;
}

/**
 * Scalar multiplication
 *
 * @param scalar  The scalar.
 * @param v       The vector.
 *
 * @tparam T  Type of the vector elements.
 * @tparam U  Type of the scalar.
 *
 * @return The vector scalar product of common type.
 */
template <typename T, typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
inline Vector2<typename std::common_type<T, U>::type> operator*(U scalar, const Vector2<T>& v) {
  return v * scalar;
}

/**
 * Calculates the dot product @f$c = a \cdot b@f$.
 *
 * @param a  Vector a.
 * @param b  Vector b.
 *
 * @tparam T  Type of the vector a elements.
 * @tparam U  Type of the vector b elements.
 *
 * @return The dot product of a with b of common type.
 */
template <typename T, typename U>
inline typename std::common_type<T, U>::type dotProduct(const Vector2<T>& a, const Vector2<U>& b) {
  return a[0] * b[0] + a[1] * b[1];
}
