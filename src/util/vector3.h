/*
 * Copyright (c) 2016 - 2019 Marco Berghoff
 * Distributed under the MIT License (license terms are in file LICENSE or at http://opensource.org/licenses/MIT).
 */

#pragma once

#include "mathutils.h"
#include <cassert>
#include <cmath>
#include <iostream>
#include <type_traits>

/**
 * Class for Vector3, a three dimensional vector.
 *
 * @tparam T  Type of the vector elements.
 */
template <typename T>
class Vector3 {
  static_assert(std::is_arithmetic<T>::value, "The Vector3 class needs an arithmetic data type.");

public:
  /// Define the type of the length, this can not be an integral type because of the sqrt.
  using length_type = typename std::conditional<std::is_integral<T>::value, double, T>::type;

  /// @name Constructors
  /// @{

  explicit Vector3() : v{T(), T(), T()} {}
  explicit Vector3(T initial) : v{initial, initial, initial} {}
  explicit Vector3(T x, T y, T z) : v{x, y, z} {}
  explicit Vector3(const T* initial) : v{initial[0], initial[1], initial[2]} {}

  template <typename U>
  Vector3(const Vector3<U>& v) : v{T(v[0]), T(v[1]), T(v[2])} {}
  /// @}

  /// @name Operators
  /// @{
  //
  Vector3 operator-() const { return Vector3(-v[0], -v[1], -v[2]); }

  template <typename U>
  Vector3<typename std::common_type<T, U>::type> operator+(const Vector3<U>& other) const {
    return Vector3<typename std::common_type<T, U>::type>(v[0] + other[0], v[1] + other[1], v[2] + other[2]);
  }

  template <typename U>
  Vector3<typename std::common_type<T, U>::type> operator-(const Vector3<U>& other) const {
    return Vector3<typename std::common_type<T, U>::type>(v[0] - other[0], v[1] - other[1], v[2] - other[2]);
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Vector3<typename std::common_type<T, U>::type> operator*(U scalar) const {
    return Vector3<typename std::common_type<T, U>::type>(v[0] * scalar, v[1] * scalar, v[2] * scalar);
  }

  template <typename U>
  typename std::common_type<T, U>::type operator*(const Vector3<U>& other) {
    return dotProduct(other);
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Vector3<typename std::common_type<T, U>::type> operator/(U scalar) const {
    return Vector3<typename std::common_type<T, U>::type>(v[0] / scalar, v[1] / scalar, v[2] / scalar);
  }

  template <typename U>
  typename std::common_type<T, U>::type dotProduct(const Vector3<U>& other) const {
    return v[0] * other[0] + v[1] * other[1] + v[2] * other[2];
  }

  /**
   * Calculates the cross product @f$c = a \times b@f$.
   *
   * @param other  The other vector.
   *
   * @tparam U  Type of the other vector.
   *
   * @return A vector of the cross product of a with b.
   */
  template <typename U>
  Vector3& crossProduct(const Vector3<U>& other) {
    T tmpv1 = v[2] * other[0] - v[0] * other[2];
    T tmpv2 = v[0] * other[1] - v[1] * other[0];

    v[0] = v[1] * other[2] - v[2] * other[1];
    v[1] = tmpv1;
    v[2] = tmpv2;

    return *this;
  }

  template <typename U>
  Vector3& operator+=(const Vector3<U>& other) {
    v[0] += other[0];
    v[1] += other[1];
    v[2] += other[2];
    return *this;
  }

  template <typename U>
  Vector3& operator-=(const Vector3<U>& other) {
    v[0] -= other[0];
    v[1] -= other[1];
    v[2] -= other[2];
    return *this;
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Vector3& operator*=(U scalar) {
    v[0] *= scalar;
    v[1] *= scalar;
    v[2] *= scalar;
    return *this;
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  Vector3& operator/=(U scalar) {
    v[0] /= scalar;
    v[1] /= scalar;
    v[2] /= scalar;
    return *this;
  }

  template <typename U>
  bool operator==(const Vector3<U>& other) const {
    return nearlyEqual(v[0], other[0]) && nearlyEqual(v[1], other[1]) && nearlyEqual(v[2], other[2]);
  }

  template <typename U>
  bool operator!=(const Vector3<U>& other) const {
    return !nearlyEqual(v[0], other[0]) || !nearlyEqual(v[1], other[1]) || !nearlyEqual(v[2], other[2]);
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  bool operator==(U scalar) const {
    return nearlyEqual(v[0], scalar) && nearlyEqual(v[1], scalar) && nearlyEqual(v[2], scalar);
  }

  template <typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
  bool operator!=(U scalar) const {
    return !nearlyEqual(v[0], scalar) || !nearlyEqual(v[1], scalar) || !nearlyEqual(v[2], scalar);
  }

  /// @}

  /// @name Element access
  /// @{

  T& operator[](unsigned int index) {
    return v[index];
  }

  const T& operator[](unsigned int index) const {
    return v[index];
  }

  const T& x() const {
    return v[0];
  }

  const T& y() const {
    return v[1];
  }

  const T& z() const {
    return v[2];
  }

  T& x() {
    return v[0];
  }

  T& y() {
    return v[1];
  }

  T& z() {
    return v[2];
  }

  /**
   * Data access function.
   *
   * @return A Pointer to the data.
   */
  T* data() noexcept {
    return v;
  }

  /**
   * Data access function.
   *
   * @return A Pointer to the data.
   */
  const T* data() const noexcept {
    return v;
  }

  /**
   * Set the vector.
   *
   * @param x  The x-coordinate.
   * @param y  The y-coordinate.
   * @param z  The z-coordinate.
   */
  void set(T x, T y, T z) {
    v[0] = x;
    v[1] = y;
    v[2] = z;
  }
  /// @}

  /// @name Arithmetic operators
  /// @{

  /**
   * Calculates the length.
   *
   * @return The length of the vector.
   */
  length_type length() const {
    return std::sqrt(static_cast<length_type>(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]));
  }

  /**
   * Normalize the vector.
   *
   * The normalization of a Vector3 is only possible when its type and its length_type are the same. For Vector3 of
   * integral types use getNormalized() instead.
   *
   * @return The normalized vector.
   */
  Vector3& normalize() {
    static_assert(std::is_same<length_type, T>::value, "Normalization of a Vector3 is not possible when type differs from its length_type.");

    const length_type len(length());
    assert(len != 0.0);
    const length_type len_r(length_type(1) / len);

    return operator*=(len_r);
  }

  /**
   * Gets the normalized vector.
   *
   * @return The normalized vector.
   */
  Vector3<length_type> getNormalized() const {
    const length_type len(length());
    assert(len != 0.0);
    const length_type len_r(length_type(1) / len);

    return Vector3<length_type>(static_cast<length_type>(v[0]) * len_r,
                                static_cast<length_type>(v[1]) * len_r,
                                static_cast<length_type>(v[2]) * len_r);
  }
  /// @}

  static const Vector3 Zero;  ///< Zero vector

private:
  T v[3];  ///< Array of length three, holding the vector with element type T.
};

template <typename T>
const Vector3<T> Vector3<T>::Zero = Vector3(T(0));

/**
 * Overload of the ostream insertion operator.
 *
 * @param os  The output stream.
 * @param v   The vector.
 *
 * @tparam T  Type of the vector elements.
 *
 * @return Reference to the output stream.
 */
template <typename T>
inline std::ostream& operator<<(std::ostream& os, const Vector3<T>& v) {
  return os << "[" << v[0] << "," << v[1] << "," << v[2] << "]";
}

template <typename T, typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
bool operator==(U scalar, const Vector3<T>& v) {
  return v == scalar;
}

template <typename T, typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
bool operator!=(U scalar, const Vector3<T>& v) {
  return v != scalar;
}

/**
 * Scalar multiplication
 *
 * @param scalar  The scalar.
 * @param v       The vector.
 *
 * @tparam T  Type of the vector elements.
 * @tparam U  Type of the scalar.
 *
 * @return The vector scalar product of common type.
 */
template <typename T, typename U, typename = typename std::enable_if<std::is_arithmetic<U>::value>::type>
inline Vector3<typename std::common_type<T, U>::type> operator*(U scalar, const Vector3<T>& v) {
  return v * scalar;
}

/**
 * Calculates the cross product @f$c = a \times b@f$.
 *
 * @param a  Vector a.
 * @param b  Vector b.
 *
 * @tparam T  Type of the vector a elements.
 * @tparam U  Type of the vector b elements.
 *
 * @return A vector of the cross product of a with b of common type.
 */
template <typename T, typename U>
inline Vector3<typename std::common_type<T, U>::type> crossProduct(const Vector3<T>& a, const Vector3<U>& b) {
  return Vector3<typename std::common_type<T, U>::type>(a[1] * b[2] - a[2] * b[1],
                                                        a[2] * b[0] - a[0] * b[2],
                                                        a[0] * b[1] - a[1] * b[0]);
}

/**
 * Calculates the dot product @f$c = a \cdot b@f$.
 *
 * @param a  Vector a.
 * @param b  Vector b.
 *
 * @tparam T  Type of the vector a elements.
 * @tparam U  Type of the vector b elements.
 *
 * @return The dot product of a with b of common type.
 */
template <typename T, typename U>
inline typename std::common_type<T, U>::type dotProduct(const Vector3<T>& a, const Vector3<U>& b) {
  return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}
