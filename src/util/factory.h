/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <map>
#include <memory>
#include <string>

/**
 * Factory Interface
 *
 * Use `TBase` for the base class and `ARGs` for arguments of the constructor.
 *
 * Usage:
 * ~~~
 * class MyFactory : public Factory<BaseClass, const std::string&>
 * ~~~
 * where MyFactory handles objects derived from * BaseClass and the constructor accept a const std::string&.
 */
template <typename TBase, typename... ARGs>
class Factory {
public:
  template <typename TDerived>
  void registerType(const std::string& name) {
    static_assert(std::is_base_of<TBase, TDerived>::value,
                  "Factory::registerType does not accept this type because it is not derived from base class");
    createFunctions[name].reset(new Creator<TDerived>);
  }

  std::unique_ptr<TBase> create(const std::string& name, ARGs&&... args) const {
    auto it = createFunctions.find(name);
    if (it == createFunctions.end()) {
      throw std::invalid_argument("Element '" + name + "'' is unknown in factory.");
    }
    return (*it->second).create(std::forward<ARGs>(args)...);
  }

  TBase* create_ptr(const std::string& name, ARGs&&... args) const {
    auto it = createFunctions.find(name);
    if (it == createFunctions.end()) {
      throw std::invalid_argument("Element '" + name + "'' is unknown in factory.");
    }
    return (*it->second).create_ptr(std::forward<ARGs>(args)...);
  }

private:
  class ICreator {
  public:
    ICreator()                    = default;
    virtual ~ICreator()           = default;
    ICreator(const ICreator&)     = delete;
    ICreator(ICreator&&) noexcept = default;
    ICreator& operator=(const ICreator&) = delete;
    ICreator& operator=(ICreator&&) = delete;

    virtual std::unique_ptr<TBase> create(ARGs&&...) const = 0;
    virtual TBase* create_ptr(ARGs&&...) const             = 0;
  };

  template <typename TDerived>
  class Creator : public ICreator {
  public:
    std::unique_ptr<TBase> create(ARGs&&... args) const override {
      return std::unique_ptr<TBase>{new TDerived(std::forward<ARGs>(args)...)};
    }
    TBase* create_ptr(ARGs&&... args) const override {
      return new TDerived(std::forward<ARGs>(args)...);
    }
  };

  std::map<std::string, std::unique_ptr<ICreator>> createFunctions;  ///< The map of create functions.
};
