/*
 * Copyright (c) 2016 - 2019 Marco Berghoff
 * Distributed under the MIT License (license terms are in file LICENSE or at http://opensource.org/licenses/MIT).
 */

#pragma once

#include <cmath>
#include <limits>
#include <type_traits>

/**
 * Check for equal values.
 *
 * Generic function for the comparison of two numeric values. Depending on the types of the two arguments, a floating
 * point aware comparison considers the limited machine accuracy.
 *
 * @param a  First value.
 * @param b  Second value.
 *
 * @tparam T  Type
 *
 * @return true if the two values are equal.
 */
template <typename T>
inline typename std::enable_if<!std::is_floating_point<T>::value, bool>::type equal_impl(T a, T b) {
  return a == b;
}

/**
 * @overload
 * @param a  First value.
 * @param b  Second value.
 * @return true if the two values are equal.
 */
template <typename T>
inline typename std::enable_if<std::is_floating_point<T>::value, bool>::type equal_impl(T a, T b) {
  return std::fabs(a - b) < std::numeric_limits<T>::epsilon();
}

/**
 * Check for nearly equal values.
 *
 * Generic function for the comparison of two numeric values. Depending on the types of the two arguments, a floating
 * point aware comparison considers the limited machine accuracy.
 *
 * @param a  First value.
 * @param b  Second value.
 *
 * @tparam T  Type of first value.
 * @tparam U  Type of second value.
 *
 * @return true if the two values are equal.
 */
template <typename T, typename U>
inline bool nearlyEqual(T a, U b) {
  return equal_impl<typename std::common_type<T, U>::type>(a, b);
}
