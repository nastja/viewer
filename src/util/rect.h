/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_UTIL_RECT_H_
#define SRC_UTIL_RECT_H_

/**
A rectangular area represented by two vectors A and B.

Also vector A and B are in the specified area.

@tparam T the type of the vectors

@ingroup util
*/
template <typename T>
class Rect {
public:
  /**
   * Create a new Rect using A and B vectors
   *
   * @param a A vector
   * @param b B vector
   */
  Rect(T a, T b) {
    pointA = a;
    pointB = b;
  }
  /**
   * return the A vector
   * @return the A vector
   */
  T getA() const {
    return pointA;
  }
  /**
   * return the B vector
   * @return the B vector
   */
  T getB() const {
    return pointB;
  }
  void setA(T a) {
    pointA = a;
  }
  void setB(T b) {
    pointB = b;
  }

private:
  T pointA;
  T pointB;
};

#endif  // SRC_UTIL_RECT_H_
