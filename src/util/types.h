/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef NASTJA_VIEWER_TYPES_H
#define NASTJA_VIEWER_TYPES_H

#include "vector2.h"
#include "vector3.h"
#include <memory>

#ifdef NASTJA_VIEWER_TYPE_LONG
using data_t = unsigned long;
#else
using data_t = double;
#endif

enum class DataType { unknown,  // reserved for not determined data type
                      uint32,
                      uint64,
                      float32,
                      float64,
                      other };

using vec3 = Vector3<double>;
using vec2 = Vector2<double>;
using ivec3 = Vector3<int>;
using ivec2 = Vector2<int>;
using uvec3 = Vector3<unsigned int>;
using ulvec3 = Vector3<unsigned long>;
using uvec2 = Vector2<unsigned int>;
using u8vec3 = Vector3<u_int8_t>;
using u8vec2 = Vector2<u_int8_t>;

#endif //NASTJA_VIEWER_TYPES_H
