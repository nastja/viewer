/*
 * Copyright (c) 2016 - 2019 Marco Berghoff
 * Distributed under the MIT License (license terms are in file LICENSE or at http://opensource.org/licenses/MIT).
 */

#pragma once

#include "util/vector3.h"
#include <algorithm>
#include <cmath>
#include <iostream>

/**
 * Class for bounding box.
 *
 * A lower and upper vector define the bounding box. It is ensured that lower <= upper in each component.
 *
 * @tparam T  Type of the edge elements.
 */
template <typename T>
class BoundingBox {
public:
  /// @name Constructors
  /// @{
  explicit BoundingBox() : lower(0.0), upper(0.0) {}

  explicit BoundingBox(const T x0, const T y0, const T z0, const T x1, const T y1, const T z1) : lower{x0, y0, z0}, upper{x1, y1, z1} {
    ensureInvariant();
  }

  explicit BoundingBox(Vector3<T> a, Vector3<T> b) : lower(a), upper(b) {
    ensureInvariant();
  }

  explicit BoundingBox(Vector3<T> center, T ext) : lower(center), upper(center) {
    extend(ext);
  }

  template <typename U>
  explicit BoundingBox(Vector3<U> a, Vector3<U> b) : lower(a), upper(b) {
    ensureInvariant();
  }
  /// @}

  /// @name Operators
  /// @{
  bool operator==(const BoundingBox& other) const {
    return lower == other.lower && upper == other.upper;
  }

  bool operator!=(const BoundingBox& other) const {
    return lower != other.lower || upper != other.upper;
  }
  /// @}

  /// @name Element access
  /// @{
  Vector3<T>& operator[](const unsigned int index) {
    return index == 0 ? lower : upper;
  }

  const Vector3<T>& operator[](const unsigned int index) const {
    return index == 0 ? lower : upper;
  }

  /**
   * Provides access to the x-component of the lower corner.
   *
   * @return The x-component of the lower corner.
   */
  T xMin() const { return lower[0]; }

  /**
   * Provides access to the y-component of the lower corner.
   *
   * @return The y-component of the lower corner.
   */
  T yMin() const { return lower[1]; }

  /**
   * Provides access to the z-component of the lower corner.
   *
   * @return The z-component of the lower corner.
   */
  T zMin() const { return lower[2]; }

  /**
   * Provides access to the x-component of the upper corner.
   *
   * @return The x-component of the upper corner.
   */
  T xMax() const { return upper[0]; }

  /**
   * Provides access to the y-component of the upper corner.
   *
   * @return The y-component of the upper corner.
   */
  T yMax() const { return upper[1]; }

  /**
   * Provides access to the z-component of the upper corner.
   *
   * @return The z-component of the upper corner.
   */
  T zMax() const { return upper[2]; }
  /// @}

  /**
 * @return size of the bounding box for every axis.
 */
  Vector3<T> size() const {
    return upper - lower;
  }

  /// @name Capacity
  /// @{
  /**
   * Tests for an empty bounding box.
   *
   * @return true if the bounding box is empty, false otherwise.
   */
  bool empty() const {
    return lower[0] == upper[0] || lower[1] == upper[1] || lower[2] == upper[2];
  }
  /// @}

  /// @name Arithmetic Operators
  /// @{

  /**
   * Translates the bounding box.
   *
   * @param x  The translation in x-direction.
   * @param y  The translation in y-direction.
   * @param z  The translation in z-direction.
   *
   * @return The translated bounding box.
   */
  BoundingBox& translate(const T x, const T y, const T z) {
    lower[0] += x;
    lower[1] += y;
    lower[2] += z;

    upper[0] += x;
    upper[1] += y;
    upper[2] += z;

    return *this;
  }

  /**
   * @overload
   *
   * @param v  The translation vector.
   *
   * @return The translated bounding box.
   */
  BoundingBox& translate(const Vector3<T>& v) {
    lower += v;
    upper += v;

    return *this;
  }

  /**
   * Intersects with an other bounding box.
   *
   * @param other  The other bounding box.
   *
   * @return The intersected bounding box.
   */
  BoundingBox& intersect(const BoundingBox& other) {
    lower[0] = std::max(lower[0], other.lower[0]);
    lower[1] = std::max(lower[1], other.lower[1]);
    lower[2] = std::max(lower[2], other.lower[2]);

    upper[0] = std::max(lower[0], std::min(upper[0], other.upper[0]));
    upper[1] = std::max(lower[1], std::min(upper[1], other.upper[1]));
    upper[2] = std::max(lower[2], std::min(upper[2], other.upper[2]));

    return *this;
  }

  /**
   * Extends the bounding box.
   *
   * @param e  The extending vector.
   *
   * @return The extended bounding box.
   */
  BoundingBox& extend(const Vector3<T> e) {
    lower -= e;
    upper += e;

    ensureInvariant();

    return *this;
  }

  /**
   * @overload
   *
   * @param e  The extending scalar.
   *
   * @return The extended bounding box.
   */
  BoundingBox& extend(const T e) {
    return extend(Vector3<T>(e));
  }

  /**
   * @overload
   *
   * @param a  The extending lower vector.
   * @param b  The extending upper vector.
   *
   * @return The extended bounding box.
   */
  BoundingBox& extend(const Vector3<T> a, const Vector3<T> b) {
    lower -= a;
    upper += b;

    ensureInvariant();

    return *this;
  }

  /**
   * @overload
   *
   * @param a  The extending lower scalar.
   * @param b  The extending upper scalar.
   *
   * @return The extended bounding box.
   */
  BoundingBox& extend(const T a, const T b) {
    return extend(Vector3<T>(a), Vector3<T>(b));
  }
  /// @}

private:
  /**
   * Ensures the invariant that lower <= upper in each component.
 * */
  void ensureInvariant() {
    if (lower[0] > upper[0]) std::swap(lower[0], upper[0]);
    if (lower[1] > upper[1]) std::swap(lower[1], upper[1]);
    if (lower[2] > upper[2]) std::swap(lower[2], upper[2]);
  }

  Vector3<T> lower;  ///< The coordinate vector of the lower corner.
  Vector3<T> upper;  ///< The coordinate vector of the upper corner.
};

/**
 * Overload of the ostream insertion operator.
 *
 * @param os  The output stream.
 * @param bb  The bounding box.
 *
 * @tparam T  Type of the corner elements.
 *
 * @return Reference to the output stream.
 */
template <typename T>
std::ostream& operator<<(std::ostream& os, const BoundingBox<T>& bb) {
  return os << "[" << bb[0] << "," << bb[1] << "]";
}

/**
 * Intersects two bounding boxes
 *
 * @param a  The first bounding box.
 * @param b  The first bounding box.
 *
 * @tparam T  Type of the corner elements.
 *
 * @return The intersection of the bounding boxes.
 */
template <typename T>
BoundingBox<T> intersect(const BoundingBox<T>& a, const BoundingBox<T>& b) {
  return BoundingBox<T>(std::max(a.xMin(), b.xMin()),
                        std::max(a.yMin(), b.yMin()),
                        std::max(a.zMin(), b.zMin()),
                        std::max(a.xMin(), std::min(a.xMax(), b.xMax())),
                        std::max(a.yMin(), std::min(a.yMax(), b.yMax())),
                        std::max(a.zMin(), std::min(a.zMax(), b.zMax())));
}
