/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef SRC_COMMANDLINEPARSER_H_
#define SRC_COMMANDLINEPARSER_H_

#include "util/types.h"
#include "model/modelmanager.h"
#include <memory>
#include <string>
#include <vector>

/**
 * Parse the Command Line Arguments into Builder Pattern.
 *
 * The Constructor reads the Arguments and a call to buildModelManager()
 * creates a ModelManager with the selected settings.
 */
class CommandLineParser {
public:
  /**
   * parse arguments in char* array.
   *
   *
   * @param argc the number of command line args
   * @param argv the strings of the command line args
   */
  CommandLineParser(int argc, const char** argv);

  void applySettings(ModelManager& model);

  std::string path;
  int frame{0};

private:
  /**
   * Name of Datafile.
   *
   * e.g. .pvd or .vti folder
   */

  uvec3 controlPoint;
  uvec3 anchorA;
  uvec3 anchorB;
  unsigned int colors;
  std::vector<std::vector<std::string>> typesVector;
  std::vector<std::string> mappingdata;

  bool hasFrame{false};
  bool hasControlPoint{false};
  bool hasAnchorA{false};
  bool hasAnchorB{false};
  bool hasColors{false};
  bool discrete{false};
};

#endif  // SRC_COMMANDLINEPARSER_H_
