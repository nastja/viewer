/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "commandlineparser.h"
#include "model/anchorpointstack.h"
#include "model/continuouscolorscale.h"
#include "model/controlpoint.h"
#include "model/file/colorfileparser.h"
#include "version.h"
#include <args.hxx>
#include <cstring>
#include <iostream>
#include <string>

std::istream& operator>>(std::istream& stream, uvec3& vector);
std::istream& operator>>(std::istream& stream, uvec2& vector);

CommandLineParser::CommandLineParser(int argc, const char** argv) {
  std::string version("The NAStJA Viewer");
  if (std::strlen(GIT_TAG) > 0) {
    version += " (Version " + std::string(GIT_TAG) + ")";
  } else {
    version += "\n" + std::string(GIT_BRANCH) + " " + std::string(GIT_HASH);
  }

  args::ArgumentParser parser(version);
  args::HelpFlag help(parser, "help", "Display this help menu", {'h', "help"});
  args::PositionalList<std::string> filenames(parser, "filenames", "the files to view");
  args::ValueFlag<int> frameFlag(parser, "frame", "set frame", {'f', "frame"});
  args::NargsValueFlag<unsigned int> controlPointFlag(parser, "control-point", "Control Point position used to cut inside the visible area", {'p', "control-point"}, 3);
  args::NargsValueFlag<unsigned int> anchorAFlag(parser, "anchor-a", "", {'a', "anchor-a"}, 3);
  args::NargsValueFlag<unsigned int> anchorBFlag(parser, "anchor-b", "", {'b', "anchor-b"}, 3);
  args::Flag discreteFlag(parser, "discrete", "", {'d', "discrete"});
  args::ValueFlag<unsigned int> colorFlag(parser, "colors", "Changes the Colorscale forward the specified number of times", {'c', "colors"});

  try {
    parser.ParseCLI(argc, (const char* const*)argv);
  } catch (args::Help&) {
    std::cout << parser << std::endl;
    std::exit(0);
  } catch (args::ParseError& e) {
    std::cerr << e.what() << std::endl;
    std::cout << parser << std::endl;
    std::exit(1);
  }
  if (frameFlag) {
    frame    = args::get(frameFlag);
    hasFrame = true;
  }
  if (colorFlag) {
    hasColors = true;
    colors    = args::get(colorFlag);
  }
  if (controlPointFlag) {
    hasControlPoint = true;
    auto v          = args::get(controlPointFlag);
    auto x          = v[0];
    auto y          = v[1];
    auto z          = v[2];
    controlPoint    = uvec3(x, y, z);
  }
  if (anchorAFlag) {
    hasAnchorA = true;
    auto v     = args::get(anchorAFlag);
    auto x     = v[0];
    auto y     = v[1];
    auto z     = v[2];
    anchorA    = uvec3(x, y, z);
  }
  if (anchorBFlag) {
    hasAnchorB = true;
    auto v     = args::get(anchorBFlag);
    auto x     = v[0];
    auto y     = v[1];
    auto z     = v[2];
    anchorB    = uvec3(x, y, z);;
  }

  if (filenames) {
    for (const auto& filename : args::get(filenames)) {
      path = filename;
      break;
      // fileNames.push_back(filename);
    }
  } else {
    std::cerr << "No filename specified." << std::endl
              << parser << std::endl;
    std::exit(1);
  }

  discrete = discreteFlag;
}

std::istream& operator>>(std::istream& stream, uvec3& vector) {
  return stream >> vector.x() >> vector.y() >> vector.z();
}
std::istream& operator>>(std::istream& stream, uvec2& vector) {
  return stream >> vector.x() >> vector.y();
}

void CommandLineParser::applySettings(ModelManager& model) {
  if (hasAnchorA || hasAnchorB) {
    if (!hasAnchorA) anchorA = uvec3{0, 0, 0};
    if (!hasAnchorB) anchorB = model.getDataManager().getDataView().getDataSize() - uvec3{1, 1, 1};
    model.getAnchorPointStack().push({anchorA, anchorB});
  }
  if (hasControlPoint) {
    model.getControlPoint().setControlPoint(controlPoint - uvec3{1, 1, 1});
  }

  if (hasColors) {
    for (int i = 0; i < colors; i++) {
      model.getColorscaleManager().switchColorbarSet(Direction::forward);
    }
  }
}
