/* The MIT License (MIT)
 *
 * Copyright (c) 2018 - 2019 The NAStJA-viewer developers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "model/modelmanager.h"
#include "presenter/presentationmanager.h"
#include "view/guiframe.h"
#include "commandlineparser.h"

/**
 * The main method. Starts the Program.
 *
 * @param argc the number of command line args
 * @param argv the strings of the command line args
 *
 * It first passes its Command Line arguments to a Instance of CommandLineParser and let it create the whole Model.
 *
 * Then an empty PresentationManager will be created.
 *
 * Finally the GUIFrame will be created and started.
 */
int main(int argc, char** argv) {
  CommandLineParser parser(argc, (const char **)argv);
  ModelManager model{parser};
  model.setupModelElements();
  parser.applySettings(model);

  PresentationManager presentation{model};

  auto& gui = presentation.getGui();
  gui.show();

  return Fl::run();
}
