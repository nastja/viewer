\title{User Manual for NAStJA Viewer}
\author{Marcel Riedel, Marco Strohmaier, Alexander Dick, Patrick Jaberg, Thomas Koch}
\date{15. 3. 2018}
\maketitle
\newpage
\tableofcontents
\newpage

# Welcome to NAStJA Viewer

NAStJA Viewer is an open source program, designed to efficiently look at and analyze volumetric data, e.g. results of simulations. Therefor a 3D presentation as well as a 1D presentation is at the users disposal. 

At the same time much value is put on easy and fast utilization with the keyboard, but also short loading times for loading and viewing data.

It can be run on GNU/Linux Clusters, but also on standard computers. According memory is needed to display larger sizes of data.

# Starting the program

The program can be started via command line. The syntax for starting the program is like follows:

- `viewer --help` or `viewer -h`

	Lists all possible command line arguments on the command line and exits the program afterwards.

`viewer [Options] <filename>`

`<filename>` is the file or directory that should be opened, while `[Options]` are the options that can be chosen from the following list:

- `-a, --anchor-a <X> <Y> <Z>`

	Set anchor point A to `<X> <Y> <Z>` in the view.
	If not set, anchor point A is the coordinate origin.

- `-b, --anchor-b <X> <Y> <Z>`

	Set anchor point B to `<X> <Y> <Z>` in the view.
	If not set, anchor point B will be the maximum of the data.

- `-p, --control-position <X> <Y> <Z>`

	Set the control point to `<X> <Y> <Z>` in the view.
	If not set, the control point will be at the same position as anchor point B.

- `-t, --time <T>`

	Start the program at the time `<T>`.
	If not set, the program will start at the time `0`.

- `-c, --color {BlackWhite | RedBlue | KIT}`

	Set the color scale  to the chosen one.
	If not set, the color scale will be `BlackWhite`.

With the use of command line arguments the user should be able to set often recurring options via scripts.

# Keyboard layout

**Help:**

- `[H]` or `[F1]`

	A help window opens, listing all shortcuts and their respective functions.

**Reset:**

- `[R]`

	The presentation of the data is reset. This means that the presentation is set back to the one at the start of the program. The only exception is the time. The current time frame is not set back to the initial one.

**Reload:**

- `[Ctrl] + [R]`

	Reloads the file. If new files have been added or others were deleted and the view has to be changed accordingly.

**Changing of time frames:**

- `[+]` and `[-]`

	Loads the next time frame or the previous time frame accordingly.

- `[Shift] + [+]` and `[Shift] + [-]`

	Loads time frames farther in the future or past. The amount  of steps taken can be adjusted with a jumpscale.

**Changing between 1D and 3D:**

- `[V]`

	Switches from 1D presentation to the 3D presentation and vice versa.

**1D presentation:**

For each axis there will a one dimensional value chart.
The values will be depicted as a two dimensional graph, the x-axis displaying the coordinates and the y-axis the corresponding values.
The graph thus will be shown as a continuous curve.

Operating and switching the axes:

- `[←]` and  `[→]`

	Moves the control point (a slider in this case) along the axis to the right or left respectively.

- `[↑]` and `[↓]`

	Switches from the current axis to the axis above or below.

Adjusting the line:

- `[Ctrl] + [↑]` and  `[Ctrl] + [↓]`

	Making the line either thicker or thinner.

- `[Ctrl] + [←]` and `[Ctrl] + [→]`

	Change the linestyle (normal, dotted, dashed).

**3D presentation:** 

The 3D data is being represented as a cuboid. Only the area left, below and behind the control point is shown. 
The control point is always located between the anchor points. 
The anchor points are responsible for erecting the the cuboid and together with the rotation define the position and size of the cuboid.
Moving the control point in a direction thus makes the cuboid smaller (or larger respectively) in the the according dimension.
Note that if the data should only be two dimensional, that the presentation will no longer be a cuboid but rather just a plane.

Rotating the cuboid:

- `[Ctrl] + [←]`: rotates the cuboid 90° to the left.
- `[Ctrl] + [↑]`: rotates the cuboid 90° upward.
- `[Ctrl] + [→]`: rotates the cuboid 90° to the right.
- `[Ctrl] + [↓]`: rotates the cuboid 90° downward.

- `[Ctrl] + [Pg Up]`: rotate the cuboid 90° counterclockwise.
- `[Ctrl] + [Pg Dn]`: rotate the cuboid 90° clockwise.

Operating the control point:

- `[←]`:       move the control point horizontally to the left.
- `[→]`:       move the control point horizontally to the right.
- `[↑]`:       move the control point vertically upwards.
- `[↓]`:       move the control point vertically downwards.
- `[Pg Up]`: move the control point along the depth axis to the back .
- `[Pg Dn]`: move the control point along the depth axis to the front.

- `[Shift] + [←]`:   move the control point multiple steps horizontally to the left.
- `[Shift] + [→]`:   move the control point multiple steps horizontally to the right.
- `[Shift] + [↑]`:   move the control point multiple steps vertically upwards.
- `[Shift] + [↓]`:   move the control point multiple steps vertically downwards.
- `[Shift] + [Pg Up]`:  move the control point multiple steps along the depth axis to the back.
- `[Shift] + [Pg Dn]`:  move the control point multiple steps along the depth axis to the front.

The amount of steps taken can be adjusted with a jumpscale.

- `[I]`

	Inverts the control point. 
	To ease the analysis of the data, it is possible to cut into the cuboid from the top right.
	Contrary to the usual presentation the cuboid is shown from achor point A to anchor point B except for the area from the top right to the control point.



**Zoom:**

- `[Z]`

	Zooms in; enlarges the selected data to maximum display size.
	Zoom can be used when mouse selection is active or the control point is moved, independent of the invert mode.


- `[Backspace]` and `[Shift]` + `[Z]`

	Undo previous zoom.
	All previous zoom actions are saved and can be undone in reverse order.

**Visuals:**

- `[N]`

	Hide and show numbers. This only applies to the area selected with the mouse (see mouse functions).

- `[G]`

	Toggle between ‘Grid’ and ‘Grid only for gradients’.

	Grid only for gradients: only show grid lines between gradients. This way different colors can be distinguished with the bare eye.



**Screenshot:**

- `[P]`

	Saves a screenshot (.png) of the current presentation of the data. 
	No windows will be depicted on the screenshot. 
	The screenshot is saved in the directory from where the program was started. 
	Screenshots will be name “Screenshot#”, in which “#” is an ascending number.

**Quit Program:**

- `[Esc]`

	Quit the Program.

# Mouse function

- `left mouse click`

	Select a value (depicted as a single block) in the data. Selected block has to be on the frontal face of the cuboid.

- `right mouse click`

	Select a second block in the data. Selected block has to be on the frontal face of the cuboid.

	Together the two blocks span an area, marked in yellow. If only one is set, the area will be spanned from the control point (top right) to the set point. 
	With `[N]` the size of the area will be shown and with `[Z]` the area will be zoomed in.




# Information Window

At the right border of the GUI an information window will be shown.
Multiple, possibly interesting values will be shown and some may be adjusted to the users desire.

**Coordinate system:**

A coordinate system in the top right, indicating which dimension is represented by which axis. 
If the cuboid is rotated the oordinate system will update accordingly.
At the beginning the coordinate system will be as follows:

- x-axis (red) to the right
- y-axis (green) to the top
- z-axis (blue) to the front

**Control point:**

- value: an output field showing the current value of the control point.
- Coordinates: three input fields in which the position of the control point can be adjusted. Each field represents one dimension (in this order: x,y,z) .
- jumpscale: An input field representing how many steps the control should be moved if he should be moved multiple steps.

**Selected points:**

- Value A: An output field with the value of the block selected with `left mouse click`.
- Coordinates A: Two input fields for the coordinate of the selected block.
- Value B: An output field with the value of the block selected with `right mouse click`.
- Coordinates B: Two input fields for the coordinate of the selected block.

**Frame:**

- Jumpscale: Input field showing how many steps should be taken if the user wants to jump to another time frame.
- Time Frame: Input field showing the current time frame followed by an output field showing the maximum amount of time frames.
- Slider: Can be used to slide through the different time frames.

**Color scale:**

- Max: Input field defining which color is assigned to the maximum value.
- Min: Input field defining which color is assigned to the minimum value.

Any value below Min will be treated like Min and any value above Max will be treated as Max.
Changing the Min/Max values of the color scale will color the cuboid accordingly.
Blocks that are non existent or that have no values will be shown in a different color than the colors available in the color scale.

# Menubar

The menubar contains the most important functions and is divided into three setions: File, View and Help.

- `File` → `Reload`: Reload `[Ctrl]` + `[R]`
- `File` → `Exit`: Quit the Program `[Esc]`

- `View` → `Show Number`: Show and hide numbers `[N]`
- `View` → `Invert`: Invert control point `[I]`
- `View` → `Reset`: Reset view `[R]`

- `Help`: Open help window `[H]`/`[F1]`
