The colorfile is a binary file.
It contains the mapping from id to color.
The first 4 bytes generate the id and the next 3 bytes generate the color based on the rgb-format.

colorScript.py shows how to generate colorfile with following mapping

| Id | Color |
| ------------- | ------------- |
| 0  | White  |
| 1  | Black  |
| 2  | Red    |
